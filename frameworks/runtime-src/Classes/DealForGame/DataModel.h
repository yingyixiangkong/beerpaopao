//
//  DataModel.h
//  beerpaopao
//
//  Created by Frank on 14-10-21.
//
//

#ifndef __beerpaopao__DataModel__
#define __beerpaopao__DataModel__

#include <iostream>
#include "../cocos2d.h"
#include "../extensions/cocos-ext.h"
#include <time.h>


#define VisibleSize Director::getInstance()->getVisibleSize()
#define Origin Director::getInstance()->getVisibleOrigin()

#define PI 3.141592654
#define CIRCLER 45
#define CIRCLED 90
#define BASEMAXTILI 30
#define TIMEBOMBROUND 5
#define FANCLEANROW 4

//#define CountJsonName 2

struct LocalizableJsonName
{
    std::string MapGuanUIData;
    std::string LevelData;
    std::string UserData;
    std::string UserLevel;
    std::string UserLevelStar;
    std::string OwlData;
    std::string PropInfo;
    std::string ShoppingMall;
};

//#define CountPlistName 1

struct LocalizablePlistName
{
    std::string GamePlayUI;
    std::string GameSceneUI;
    std::string ToolsUI;
    std::string BagLayerUI;
    std::string FinishUI;
    std::string ShoppingMallUI;
    std::string PreFinishUI;
    std::string OwlLayerUI;
    std::string FriendLayerUI;
    std::string MapBottomUI;
};

//#define CountPngFilePath 7

struct LocalizablePngFilePath
{
    std::string LoginBack;
    std::string ThirdLogin;
    std::string VisitorLogin;
    std::string MapFirstBack;
    
    std::string MapThirdMid;
    std::string MapThirdMid1;
    std::string MapTop;
    std::string GamePlayBack;
};

//#define CountFontString 1

struct LocalizableFontString
{
    std::string Name;
    std::string LevelString;
    std::string WinString1;
    std::string WinString2;
    std::string UserBagList;
};

//#define CountPngName 35

struct LocalizablePngName
{
    std::string BlueNormal;
    std::string BlueGiddy;
    std::string BlueIce;
    std::string BlueMud;
    
    std::string GreenNormal;
    std::string GreenGiddy;
    std::string GreenIce;
    std::string GreenMud;
    
    std::string PinkNormal;
    std::string PinkGiddy;
    std::string PinkIce;
    std::string PinkMud;
    
    std::string RedNormal;
    std::string RedGiddy;
    std::string RedIce;
    std::string RedMud;
    
    std::string YellowNormal;
    std::string YellowGiddy;
    std::string YellowIce;
    std::string YellowMud;
    
    std::string BlueSNormal;
    std::string BlueSGiddy;
    std::string BlueSIce;
    std::string BlueSMud;
    
    std::string GreenSNormal;
    std::string GreenSGiddy;
    std::string GreenSIce;
    std::string GreenSMud;
    
    std::string PinkSNormal;
    std::string PinkSGiddy;
    std::string PinkSIce;
    std::string PinkSMud;
    
    std::string RedSNormal;
    std::string RedSGiddy;
    std::string RedSIce;
    std::string RedSMud;
    
    std::string YellowSNormal;
    std::string YellowSGiddy;
    std::string YellowSIce;
    std::string YellowSMud;
    
    std::string SpiderWeb;
    std::string DemonBall;
    std::string StoneBall;
    std::string TimeBomb;
    std::string TimeBall;
    std::string Flashing;
    std::string ClarityBall;
    std::string Bee;
    std::string BombBall;
    std::string MoreBall;
    
    std::string Bomb;
    std::string Meteorite;
    std::string Fan;
    
    std::string BombCustom;
    std::string MeteoriteCustom;
    std::string FanCustom;
    
    std::string WaterGun;
    std::string Hammer;
    std::string MagicBang;
    std::string IncreaseBall;
    
    std::string RattanLine;
    
//    ToolsUI
    std::string BagIcon;
    std::string MissionIcon;
    std::string SettingIcon;
    std::string ShopIcon;
    std::string SignIcon;
    std::string ToolCloud;
    std::string SpecialIcon;
    std::string OwlSleep;
    std::string OwlWakeUp;
    std::string OwlCloud;
    std::string OwlCloudS;
    std::string SleepPao;
    
    //PrePlayUI
    std::string AddProp;
    std::string Close;
    std::string CloseS;
    std::string Coin;
    std::string PreBackGroup;
    std::string PriceBack;
    std::string PropBack;
    std::string PropInfoButton;
    std::string PropInfoButtonS;
    std::string StartButton;
    std::string StartButtonS;
    
    //FinishUI
    std::string Fail_Back;
    std::string Fail_CloseNormal;
    std::string Fail_CloseSelected;
    std::string Fail_FunnyPNG;
    std::string Fail_Level;
    std::string Fail_TryNormal;
    std::string Fail_TrySelected;
    std::string Fail_Word;
    std::string Win_Back;
    std::string Win_CloseNormal;
    std::string Win_CloseSelected;
    std::string Win_Coin;
    std::string Win_Level;
    std::string Win_NextNormal;
    std::string Win_NextSelected;
    std::string Win_Score;
    std::string Win_ShareNormal;
    std::string Win_ShareSelected;
    std::string Win_StarEmpty;
    std::string Win_StarFull;
    std::string Win_Tili;
    std::string Win_Word;
    
    //BagLayerUI
    std::string BagBack;
    std::string BagText;
    std::string BagPropBack;
    
    //ShoppingMallUI
    std::string AddButton;
    std::string AddbuttonS;
    std::string CoinBack;
    std::string CoinLabel;
    std::string CoinLabelS;
    std::string PackLabel;
    std::string PackLabelS;
    std::string ShopPriceBack;
    std::string ShopPropBack;
    std::string PropLabel;
    std::string PropLabelS;
    std::string ShopLabel;
    std::string ShoppingBack;
    std::string SmallCoin;
    std::string ShoppingGroud;
    
    //PreFinishUI
    std::string CountDownBack;
    std::string GoOnButton;
    std::string GoOnButtonS;
    std::string PaoPaoBag;
    std::string PreFinishBack;
    std::string PriceButton;
    
    //OwlLayerUI
    std::string OwlDialog;
    std::string VerticalLineS;
    std::string VerticalLineL;
    std::string OwlBranch;
    std::string BagBranch;
    std::string OwlPropInfoUI;
    std::string OwlBagUI;
    
    //FriendLayerUI
    std::string One;
    std::string Two;
    std::string Three;
    std::string AddFriend;
    std::string AddFriendS;
    std::string BlankBack;
    std::string DelFriend;
    std::string DelFriendS;
    std::string FriendBack;
    std::string HeadBack;
    std::string HeadImage;
    std::string LineBack;
    std::string SelectBack;
    
    //MapBottomUI
    std::string MapGuanBottom1;
    std::string MapGuanBottom2;
    std::string MapGuanBottom3;
    std::string MapBottomLeaf1;
    std::string MapBottomLeaf2;
    std::string MapBottomLeaf3;
    std::string LeafRight;
    std::string LeafLeft;
};
enum PaoStyle
{
    PaoStyleQiuNormal = 11,
    PaoStyleQiuSpecial = 12,
    PaoStyleDaoJuUp = 13,
    PaoStyleDaoJuDown = 14,
};

enum PaoStatu
{
    PaoStatuNormal = 0,        //正常
    PaoStatuIce = 1,           //冰冻
    PaoStatuMud = 2,           //污泥
    PaoStatuGiddy = 3,         //眩晕
    PaoStatuRattanCover = 4,   //藤蔓覆盖
    PaoStatuRattanSource = 5,  //藤蔓来源
};

//enum PaoName
//{
//    PaoNameBlue = 0,           //蓝色泡泡
//    PaoNameGreen = 1,          //绿色泡泡
//    PaoNamePink = 2,           //粉色泡泡
//    PaoNameRed = 3,            //红色泡泡
//    PaoNameYellow = 4,         //黄色泡泡
//    
//    PaoNameSpiderWeb = 5,      //蜘蛛网
//    PaoNameDemonBall = 6,      //魔鬼球
//    PaoNameStoneBall = 7,      //石头
//    PaoNameTimeBomb = 8,       //定时炸弹
//    PaoNameTimeBall = 9,       //钟表
//    PaoNameFlashing = 10,       //闪电
//    PaoNameClarityBall = 11,    //透明泡泡
//    PaoNameBee = 12,            //蜜蜂
//    PaoNameBombBall = 13,       //炸弹泡泡
//    PaoNameMoreBall = 14,       //加泡泡数的泡泡
//    
//    PaoNameBomb = 15,           //炸弹
//    PaoNameMeteorite = 16,      //陨石
//    PaoNameFan = 17,            //风扇
//};
enum PaoName
{
    PaoNamePao = 110,
    PaoNameSpecial = 130,
    
    PaoNameTimeBomb = 140,       //定时炸弹
    PaoNameSecondsIncrease = 150,       //加时间
    PaoNamePaoIncrease = 160,       //加泡泡
    PaoNameSpiderWeb = 173,      //蜘蛛网
    PaoNameDemonPao = 174,      //魔鬼球
    PaoNameStone = 175,      //石头
    PaoNameFlashing = 176,       //闪电
    PaoNameClarityPao = 177,    //透明泡泡
    PaoNameBee = 178,            //蜜蜂
    PaoNameBombPao = 179,       //炸弹泡泡
    
    PaoNameColor = 200,          //彩球
    PaoNameBomb = 201,           //炸弹
    PaoNameMeteorite = 202,      //陨石
    PaoNameFan = 203,            //风扇
    
    PaoNameCustomBomb = 211,
    PaoNameCustomMeteorite = 221,
    PaoNameCustomFan = 231,
    
    PaoNameWaterGun = 901,
    PaoNameHammer = 902,
    PaoNameMagicWand = 903,
    PaoNamePaoExtra = 904,
    PaoNameSecondsExtra = 905,
};

enum PaoColor
{
    PaoColorNone = 0,
    PaoColorBlue = 2,
    PaoColorGreen = 3,
    PaoColorPink = 4,
    PaoColorRed = 5,
    PaoColorYellow = 6,
    PaoColorColorful = 7,
};

//struct PaoPaoData
//{
//    std::string TargetPaoFrameName;
////    PaoStyle TargetPaoStyle;
//    PaoStatu TargetPaoStatu;
//    PaoName TargetPaoName;
//    PaoColor TargetPaoColor;
//};

struct CustomPaoDestructive
{
    bool operator == ( const CustomPaoDestructive & rDes)const
    {
        return m_maxDestructive == rDes.m_maxDestructive && m_minDestructive == rDes.m_minDestructive;
    }
    
//    CustomPaoDestructive( int nMax, int nMin)
//    {
//        m_maxDestructive = nMax;
//        m_minDestructive = nMin;
//    }
    
    int m_maxDestructive;
    int m_minDestructive;
};




//游戏行列结构体的封装

struct RowCol
{
	bool operator==( const RowCol & rPos ) const
	{
		return m_nRow == rPos.m_nRow && m_nCol == rPos.m_nCol;
	}
    
	RowCol( int nRow, int nCol )
	{
		m_nRow = nRow;
		m_nCol = nCol;
	}
	int m_nRow;
	int m_nCol;
};

struct MapGuanUIData
{
    std::string NormalImage;
    std::string SelectedImage;
    float PositionX;
    float PositionY;
};


struct NetBackNomalData
{
    std::string PNSystemId;
    std::string PNState;
    std::string PNerrorMsg;
};

struct NetBackRankData
{
    std::string PNSystemId;
    std::string PNState;
    std::string PNerrorMsg;
};

struct NetBackFriendData
{
    std::string SystemId;
    std::string UserNick;
    std::string UserSex;
    std::string HeadImage;
    std::string Latitude;
    std::string Longitude;
    std::string LevelID;
    std::string StartNum;
};

struct FinishPlayData
{
    bool isWin;
    long Score;
    int Star;
    int LevelId;
};

struct ArmatureJson
{
    std::string BeerBoyLeftPng;
    std::string BeerBoyLeftplist;
    std::string BeerBoyLeftJson;
    
    std::string BeerBoyCenterPng;
    std::string BeerBoyCenterplist;
    std::string BeerBoyCenterJson;
    
    std::string BeerBoyRightPng;
    std::string BeerBoyRightplist;
    std::string BeerBoyRightJson;
    
    std::string BeerGirlLeftPng;
    std::string BeerGirlLeftplist;
    std::string BeerGirlLeftJson;
    
    std::string BeerGirlRightPng;
    std::string BeerGirlRightplist;
    std::string BeerGirlRightJson;
};

struct TimeStruct
{
    int mYear;
    int mMonth;
    int mDay;
    int mHour;
    int mMinite;
    int mSecond;
};

struct OperationData
{
    std::string OperateDateTime;
    std::string OperateLog;
    std::string ClientAppVersion;
    int LogState;
};

std::string turnIntToString(int targetInt);
int turnStringToInt(std::string targetString);
std::string turnLongToString(long targetLong);
long turnStringToLong(std::string targetLong);
std::string turnDoubleToString(double targetDouble);
double turnStringToDouble(std::string targetString);
std::string Replace_String( const std::string& orignStr, const std::string& oldStr, const std::string& newStr );

std::vector<int> sortAndDelDuplicateVector(std::vector<int> temV);

TimeStruct getLocalTime();


#endif /* defined(__beerpaopao__DataModel__) */
