//
//  DataModel.cpp
//  beerpaopao
//
//  Created by Frank on 14-10-21.
//
//

#include "DataModel.h"

std::string turnIntToString(int targetInt)
{
    char str[20];
    sprintf(str, "%d",targetInt);
    
    return str;
}
int turnStringToInt(std::string targetString)
{
    return atoi(targetString.c_str());
}

std::string turnLongToString(long targetLong)
{
    char str[20];
    sprintf(str, "%ld",targetLong);
    return str;
}

long turnStringToLong(std::string targetLong)
{
    return atol(targetLong.c_str());
}
std::string turnDoubleToString(double targetDouble)
{
    std::string str;
    std::stringstream ss;
    
    ss<<targetDouble;
    ss>>str;
    
    return str;
}
double turnStringToDouble(std::string targetString)
{
    return atof(targetString.c_str());
}

TimeStruct getLocalTime()
{
    struct tm *local;
    time_t t;
    t=time(NULL);
    local=localtime(&t);
//    printf("Local tm_gmtoff is: %ld\n",local->tm_gmtoff);
//    printf("Local tm_hour is: %d\n",local->tm_hour);
//    printf("Local tm_isdst is: %d\n",local->tm_isdst);
//    printf("Local tm_mday is: %d\n",local->tm_mday);
//    printf("Local tm_min is: %d\n",local->tm_min);
//    printf("Local tm_mon is: %d\n",local->tm_mon);
//    printf("Local tm_sec is: %d\n",local->tm_sec);
//    printf("Local tm_wday is: %d\n",local->tm_wday);
//    printf("Local tm_yday is: %d\n",local->tm_yday);
//    printf("Local tm_year is: %d\n",local->tm_year);
//    printf("Local tm_zone is: %s\n",local->tm_zone);
    
    TimeStruct temTimeSt;
    temTimeSt.mYear = local->tm_year + 1900;	   // years since 1900 ,得到的数字加上1900就是现在的年
    temTimeSt.mMonth = local->tm_mon + 1;    // months since January [0-11],得到的数字加1就是现在的月
    temTimeSt.mDay = local->tm_mday;	   // day of the month [1-31],得到的数字就是现在的日
    
    temTimeSt.mHour = local->tm_hour;	   // hours since midnight [0-23],得到的数字就是时
	temTimeSt.mMinite = local->tm_min;   // minutes after the hour [0-59],得到的数字就是分
    temTimeSt.mSecond = local->tm_sec;   // seconds after the minute [0-60],得到的数字就是秒
	
    printf("LocalTime is %d-%d-%d %d:%d:%d\n",temTimeSt.mYear,temTimeSt.mMonth,temTimeSt.mDay,temTimeSt.mHour,temTimeSt.mMinite,temTimeSt.mSecond);
    
    return temTimeSt;
}

std::string Replace_String( const std::string& orignStr, const std::string& oldStr, const std::string& newStr )
{
    size_t pos = 0;
    std::string tempStr = orignStr;
    std::string::size_type newStrLen = newStr.length();
    std::string::size_type oldStrLen = oldStr.length();
    while(true)
    {
        pos = tempStr.find(oldStr, pos);
        if (pos == std::string::npos) break;
        
        tempStr.replace(pos, oldStrLen, newStr);
        pos += newStrLen;
        
    }
    
    return tempStr;
}

std::vector<int> sortAndDelDuplicateVector(std::vector<int> temV)
{
    std::sort(temV.begin(), temV.end());
    
    std::vector<int>::iterator pos = std::unique(temV.begin(), temV.end());
    
    temV.erase(pos, temV.end());
    
    return temV;
}