//
//  PropInfo.h
//  beerpaopao
//
//  Created by liupeng on 14/11/18.
//
//

#ifndef __beerpaopao__PropInfo__
#define __beerpaopao__PropInfo__

#include <iostream>
#include "../cocos2d.h"
#include "../extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class PropInfo : Ref{
public:
    virtual bool init();
    
    CC_SYNTHESIZE(std::string, _propName, PropName);
    CC_SYNTHESIZE(std::string, _info, Info);
    CC_SYNTHESIZE(std::string, _pngName, PngName);
    
    
    CREATE_FUNC(PropInfo);
private:
    
    std::string _pName;
};

#endif /* defined(__beerpaopao__PropInfo__) */
