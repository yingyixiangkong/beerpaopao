//
//  UserLevel.h
//  beerpaopao
//
//  Created by liupeng on 14-10-24.
//
//

#ifndef __beerpaopao__UserLevel__
#define __beerpaopao__UserLevel__

#include <iostream>
#include "../cocos2d.h"
#include "../extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class UserLevel : Ref{
public:
    virtual bool init();
    
    void setLevel(int level);
    
    CC_SYNTHESIZE(std::string, _winScore, WinScore);
    CC_SYNTHESIZE(int, _levelStart, LevelStart);
    CC_SYNTHESIZE(int, _playCount, PlayCount);
    CC_SYNTHESIZE(int, _failCount, FailCount);
    
    
    CREATE_FUNC(UserLevel);
private:
    
    int _levelNum;
};

#endif /* defined(__beerpaopao__UserLevel__) */
