//
//  OwlObjectData.cpp
//  beerpaopao
//
//  Created by liupeng on 14/11/29.
//
//

#include "OwlObjectData.h"

bool OwlObjectData::init()
{
    return true;
}

void OwlObjectData::setOwlObject(std::string OwlOb)
{
    _owlObject = OwlOb;

}
std::string OwlObjectData::getOwlObject()
{
    return _owlObject;
}

std::vector<std::map<std::string,std::string>> OwlObjectData::getMakeNeed()
{
    return _makeNeed;
}

void OwlObjectData::setMakeNeed(std::vector<std::map<std::string,std::string>> var)
{
    _makeNeed=var;
}

