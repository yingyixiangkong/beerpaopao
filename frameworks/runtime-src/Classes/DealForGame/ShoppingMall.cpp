//
//  ShoppingMall.cpp
//  beerpaopao
//
//  Created by liupeng on 14/11/18.
//
//

#include "ShoppingMall.h"

bool ShoppingMall::init()
{
    
    return true;
}


void ShoppingMall::setPropPack(std::vector<std::map<std::string,std::string>> var)
{
    _propPack = var;
}

std::vector<std::map<std::string,std::string>> ShoppingMall::getPropPack()
{
    return _propPack;
}


void ShoppingMall::setPropShop(std::vector<std::map<std::string,std::string>> var)
{
    _propShop = var;
}

std::vector<std::map<std::string,std::string>> ShoppingMall::getPropShop()
{
    return _propShop;
}


void ShoppingMall::setCoinShop(std::vector<std::map<std::string,std::string>> var)
{
    _coinShop = var;
}

std::vector<std::map<std::string,std::string>> ShoppingMall::getCoinShop()
{
    return _coinShop;
}