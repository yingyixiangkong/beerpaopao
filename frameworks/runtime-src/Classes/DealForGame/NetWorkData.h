//
//  NetWorkData.h
//  beerpaopao
//
//  Created by liupeng on 14-10-24.
//
//

#ifndef __beerpaopao__NetWorkData__
#define __beerpaopao__NetWorkData__

#include "../cocos2d.h"
#include "CCLuaEngine.h"
#include "../ScutSDK/ScutExt.h"
#include "../ScutSDK/ScutDataLogic/LuaHost.h"
#include "DataCenter.h"
//#include "../SceneForGame/FriendsListLayer.h"
//#include "../SceneForGame/MapScene.h"

USING_NS_CC;

class NetWorkData;
class NetWorkDataDelegate
{
public:
    
    virtual void updataFriends(std::vector<NetBackFriendData> friendsList,int FDType);
};

class NetWorkData : Ref{
public:
    
    static NetWorkData* newNetWorkData();
    
    virtual bool init();
    
    void setDelegate(NetWorkDataDelegate *delegate);
    
    //新用户注册
    void sendNewUser(int newUser,std::string userDevice,std::string deviceNo,std::string clientVersion,std::string opDateTime);
    
    //接收常规信息返回值
    void receiveNBNormal(std::string systemId,std::string state,std::string errorMsg);
    
    void clearNBNormal();
    
    //发送操作记录
    void sendUserLog(std::string systemId,std::string clientVersion,std::string opDateTime);
    
    //请求好友列表（非好友列表）
    void requestFriendsList(std::string systemId,std::string longitude,std::string latitude,int FDType);
    
    //接收好友列表（非好友列表）
    void receiveFriendsList(std::string result,int FDType);
    
    //FDType=1、2、3：添加好友、确认好友、好友邀请
    void addFriend(std::string systemId,std::string friendId,int FDType);
    
    CREATE_FUNC(NetWorkData);
    
protected:
    
    NetBackNomalData getNBData();
private:
    NetWorkDataDelegate *_delegate;
    NetBackNomalData _NBData;
};

#endif /* defined(__beerpaopao__NetWorkData__) */
