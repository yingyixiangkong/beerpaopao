//
//  UserData.h
//  beerpaopao
//
//  Created by liupeng on 14-10-23.
//
//

#ifndef __beerpaopao__UserData__
#define __beerpaopao__UserData__

#include <iostream>
#include "../cocos2d.h"
#include "../extensions/cocos-ext.h"
#include "DataModel.h"

USING_NS_CC;
USING_NS_CC_EXT;

class UserData : Ref{
public:
    virtual bool init();
    
    CC_SYNTHESIZE(std::string, _systemId, SystemId);
    CC_SYNTHESIZE(std::string, _userId, UserId);
    CC_SYNTHESIZE(std::string, _userNick, UserNick);
    CC_SYNTHESIZE(std::string, _userComeFrom, UserComeFrom);
    CC_SYNTHESIZE(std::string, _headImg, HeadImg);
//    CC_SYNTHESIZE(std::string, _userDevice, UserDevice);
//    CC_SYNTHESIZE(std::string, _deviceNo, DeviceNo);
//    CC_SYNTHESIZE(std::vector<std::vector<long>>, _userBagProp, UserBagProp)
//    CC_SYNTHESIZE(std::vector<long>, _userBagSpecialBall, UserBagSpecialBall);
    CC_SYNTHESIZE(int, _userCoin, UserCoin);
    CC_SYNTHESIZE(int, _userStart, UserStar);
    CC_SYNTHESIZE(int, _userLevel, UserLevel);
    CC_SYNTHESIZE(int, _owlLevel, OwlLevel);
    CC_SYNTHESIZE(int, _userVip, UserVip);
    CC_SYNTHESIZE(int, _userMonth, UserMonth);
    
    //上次储存时，拥有的体力时间的值。包括两部分，一部分是拥有的体力*600秒；另一部分是为了下一点体力已经经过了多少秒。此数值在每一个需要倒计时的场景都进行实时改变
    CC_PROPERTY(long, _ownSeconds, OwnSeconds);
    //上次离开游戏的时间，在每次进入游戏时读取，配合_ownSeconds可以修正当前拥有的时间；在每次离开游戏时存，以便下一次进来时的使用
    CC_SYNTHESIZE_READONLY(TimeStruct, _lastLeaveGameTime, LastLeaveGameTime);
    
    void reLoadLastLeaveGameTime();
    
    void setUserBagProp(std::vector<std::map<std::string,std::string>> userBagProp);
    std::vector<std::map<std::string,std::string>> getUserBagProp();
    
    void setUserBagSpecialBall(std::vector<std::map<std::string,std::string>> userBagSpecialBall);
    std::vector<std::map<std::string,std::string>> getUserBagSpecialBall();
    
    CREATE_FUNC(UserData);
    
private:
    std::vector<std::map<std::string,std::string>> _userBagProp;
    std::vector<std::map<std::string,std::string>> _userBagSpecialBall;
    
    void fixOwnSeconds();
};

#endif /* defined(__beerpaopao__UserData__) */
