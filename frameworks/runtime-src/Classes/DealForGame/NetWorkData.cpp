//
//  NetWorkData.cpp
//  beerpaopao
//
//  Created by liupeng on 14-10-24.
//
//

#include "NetWorkData.h"
#include "../SceneForGame/MapScene.h"

bool NetWorkData::init()
{
    
    return true;
}

NetWorkData* NetWorkData::newNetWorkData(){
    NetWorkData *newNetWork = NetWorkData::create();
    
    return newNetWork;
}

void NetWorkData::sendNewUser(int newUser,std::string userDevice,std::string deviceNo,std::string clientVersion,std::string opDateTime){
    lua_State* pState = ScutDataLogic::LuaHost::Instance()->GetLuaState();

    ScutExt::getInstance()->pushfunc("GameNetWork.callAction1001");

    lua_pushnumber(pState, newUser);
    lua_pushstring(pState, userDevice.c_str());
    lua_pushstring(pState, deviceNo.c_str());
    lua_pushstring(pState, clientVersion.c_str());
    lua_pushstring(pState, opDateTime.c_str());

    int error=lua_pcall(pState, 5, 0, NULL);

    if (error) {
        log("bool:lua_tostring--%s",lua_tostring(pState, -1));
    }
    
    
}

void NetWorkData::receiveNBNormal(std::string systemId,std::string state,std::string errorMsg){
    log("receiveNewUser--%s----%s---%s",systemId.c_str(),state.c_str(),errorMsg.c_str());
    
    _NBData.PNSystemId=systemId;
    _NBData.PNState=state;
    _NBData.PNerrorMsg=errorMsg;
    
}

void NetWorkData::sendUserLog(std::string systemId,std::string clientVersion,std::string opDateTime){
    
    lua_State* pState = ScutDataLogic::LuaHost::Instance()->GetLuaState();
    
    ScutExt::getInstance()->pushfunc("GameNetWork.callAction1002");
    lua_pushstring(pState, systemId.c_str());
    lua_pushstring(pState, clientVersion.c_str());
    lua_pushstring(pState, opDateTime.c_str());
    int error=lua_pcall(pState, 3, 0, NULL);
    
    if (error) {
        log("bool:lua_tostring--%s",lua_tostring(pState, -1));
    }
    
}

NetBackNomalData NetWorkData::getNBData(){
    return _NBData;
}

void NetWorkData::clearNBNormal(){
    _NBData.PNSystemId="";
    _NBData.PNState="";
    _NBData.PNerrorMsg="";
}

void NetWorkData::requestFriendsList(std::string systemId,std::string longitude,std::string latitude,int FDType){
    lua_State* pState = ScutDataLogic::LuaHost::Instance()->GetLuaState();
    
    ScutExt::getInstance()->pushfunc("GameNetWork.callAction1006");
    
    lua_pushstring(pState, systemId.c_str());
    lua_pushstring(pState, longitude.c_str());
    lua_pushstring(pState, latitude.c_str());
    lua_pushnumber(pState, FDType);
    
    
    int error=lua_pcall(pState, 4, 0, NULL);
    
    if (error) {
        log("bool:lua_tostring--%s",lua_tostring(pState, -1));
    }
    
}

void NetWorkData::receiveFriendsList(std::string result,int FDType){
    log("receiveFriendsList--%s",result.c_str());
    std::vector<NetBackFriendData> friendsList=DataCenter::getInstance()->getFriendsList(result);
    
    MapScene *ll=(MapScene*)Director::getInstance()->getRunningScene()->getChildByTag(888);
    ll->setFriendsData(friendsList, FDType);
    
}

void NetWorkData::addFriend(std::string systemId,std::string friendId,int FDType){
    lua_State* pState = ScutDataLogic::LuaHost::Instance()->GetLuaState();
    
    ScutExt::getInstance()->pushfunc("GameNetWork.callAction1007");
    
    lua_pushstring(pState, systemId.c_str());
    lua_pushstring(pState, friendId.c_str());
    lua_pushnumber(pState, FDType);
    
    int error=lua_pcall(pState, 3, 0, NULL);
    
    if (error) {
        log("bool:lua_tostring--%s",lua_tostring(pState, -1));
    }
}

void NetWorkData::setDelegate(NetWorkDataDelegate *delegate){
    _delegate = delegate;
    
    if (_delegate != NULL)
    {
        log("setDelegate--YES!");
    }else{
        log("setDelegate--NO!");
    }
}

void NetWorkDataDelegate::updataFriends(std::vector<NetBackFriendData> friendsList,int FDType){
    
}