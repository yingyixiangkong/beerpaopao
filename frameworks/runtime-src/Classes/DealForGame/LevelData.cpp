//
//  LevelData.cpp
//  beerpaopao
//
//  Created by Frank on 14-9-30.
//
//

#include "LevelData.h"


bool LevelData::init()
{
//    randomNewPaoNameV();
    return true;
}


void LevelData::setLevel(int level)
{
    _level = level;
    log("LevelData::setLevel------%d",_level);
//    clearData();
}
int LevelData::getLevel()
{
//    log("LevelData::getLevel------%d",_level);
    return _level;
}

void LevelData::clearData()
{
    _row = 0;
    _col = 0;
    if (_ballUpVector.size() != 0)
    {
        _ballUpVector.clear();
    }
    
    _ballDownCount = 0;
    if (_ballDownVector.size() != 0)
    {
        _ballDownVector.clear();
    }
//    if (_newPaoIndexV.size() != 0)
//    {
//        _newPaoIndexV.clear();
//    }
}

void LevelData::setSpecailBallDataV(std::vector<std::map<std::string,long>> var)
{
    _specailBallDataV = var;
}

std::vector<std::map<std::string,long>> LevelData::getSpecailBallDataV()
{
    return _specailBallDataV;
}

//long LevelData::transformWithLevelRandomNewPaoIndexV(long var)
//{
//    long firstL = (var/10000)%100;
//    long secondL = (var/100)%100;
//    long thirdL = var%100;
//    
//    if (secondL == 0)
//    {
//        struct timeval tpTimeA;
//        gettimeofday(&tpTimeA,NULL);
//        srand(tpTimeA.tv_usec);
//        
//        secondL = rand()%5+1;
//    }
//    
//    log("_newPaoIndexV ------- %ld",_newPaoIndexV.size());
//    
//    if ((firstL == 11 || firstL == 12))
//    {
//        log("_newPaoIndexV ------- %ld",_newPaoIndexV.size());
//        return firstL*10000+(_newPaoIndexV[secondL-1])*100+thirdL;
//    }
//    else
//    {
//        return var;
//    }
//}
//void LevelData::randomNewPaoNameV()
//{
//    std::vector<int> temLongV;
//    
//    for (int index = 1; index < 6; index++)
//    {
//        temLongV.push_back(index);
//    }
//    
//    for (int index = 5; index > 0; index--)
//    {
//        struct timeval tpTimeA;
//        gettimeofday(&tpTimeA,NULL);
//        srand(tpTimeA.tv_usec);
//        
//        int target = rand()%index;
//        int it = temLongV[target];
//        _newPaoIndexV.push_back(it);
//        
//        temLongV.erase(temLongV.begin()+target);
//    }
//    
//    log("temLongV size : %ld,_newPaoIndexV : %ld",temLongV.size(),_newPaoIndexV.size());
//    
//    for (int index = 0; index < _newPaoIndexV.size(); index++)
//    {
//        log("_newPaoIndexV[index] = %d",_newPaoIndexV[index]);
//    }
//}

//std::vector<int> LevelData::getNewPaoIndexV()
//{
//    return _newPaoIndexV;
//}

//void LevelData::cloneFromOtherLD(LevelData* tLvlData)
//{
//    _level = tLvlData->getLevel();
//    _row = tLvlData->getRow();
//    _col = tLvlData->getCol();
//    _ballDownCount = tLvlData->getBallDownCount();
//    _winType = tLvlData->getWinType();
//    _gameSeconds = tLvlData->getGameSeconds();
//    _gameWinScore = tLvlData->getGameWinScore();
//    _specialBallCount = tLvlData->getSpecialBallCount();
//    _promptBall = tLvlData->getPromptBall();
//    _oneStarScore = tLvlData->getOneStarScore();
//    _twoStarScore = tLvlData->getTwoStarScore();
//    _threeStarScore = tLvlData->getThreeStarScore();
//    _ballUpVector = tLvlData->getBallUpVector();
//    _ballDownVector = tLvlData->getBallDwonVector();
//    _specailBallDataV = tLvlData->getSpecailBallDataV();
//    _newPaoIndexV = tLvlData->getNewPaoIndexV();
//}


