//
//  ShoppingMall.h
//  beerpaopao
//
//  Created by liupeng on 14/11/18.
//
//

#ifndef __beerpaopao__ShoppingMall__
#define __beerpaopao__ShoppingMall__
#include <iostream>
#include "../cocos2d.h"
#include "../extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class ShoppingMall : Ref{
public:
    virtual bool init();
    
//    CC_SYNTHESIZE(std::string, _propName, PropPack);
//    CC_SYNTHESIZE(std::string, _info, PropShop);
//    CC_SYNTHESIZE(std::string, _pngName, CoinShop);
    
    void setPropPack(std::vector<std::map<std::string,std::string>> var);
    std::vector<std::map<std::string,std::string>> getPropPack();
    
    void setPropShop(std::vector<std::map<std::string,std::string>> var);
    std::vector<std::map<std::string,std::string>> getPropShop();
    
    void setCoinShop(std::vector<std::map<std::string,std::string>> var);
    std::vector<std::map<std::string,std::string>> getCoinShop();
    
    CREATE_FUNC(ShoppingMall);
private:
    
    std::vector<std::map<std::string,std::string>> _propPack;
    std::vector<std::map<std::string,std::string>> _propShop;
    std::vector<std::map<std::string,std::string>> _coinShop;
};


#endif /* defined(__beerpaopao__ShoppingMall__) */
