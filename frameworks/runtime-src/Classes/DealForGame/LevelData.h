//
//  LevelData.h
//  beerpaopao
//
//  Created by Frank on 14-9-30.
//
//

#ifndef __beerpaopao__LevelData__
#define __beerpaopao__LevelData__

#include <iostream>
#include "../cocos2d.h"
#include "../extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class LevelData : Ref
{
public:
    
    virtual bool init();
    
    
    void setLevel(int level);
    int getLevel();
//    CC_PROPERTY(int, _level, Level);//先调用set方法后才能设置其他属性
    
    CC_SYNTHESIZE(int, _row, Row);
    CC_SYNTHESIZE(int, _col, Col);
    CC_SYNTHESIZE(std::vector<std::vector<long>>, _ballUpVector, BallUpVector)
    CC_SYNTHESIZE(int, _ballDownCount, BallDownCount);
    CC_SYNTHESIZE(std::vector<long>, _ballDownVector, BallDwonVector);
    CC_SYNTHESIZE(int, _winType, WinType);
    CC_SYNTHESIZE(int, _gameSeconds, GameSeconds);
    CC_SYNTHESIZE(long, _gameWinScore, GameWinScore);
    CC_SYNTHESIZE(int, _specialBallCount, SpecialBallCount);
    
    
    CC_SYNTHESIZE(long, _promptBall, PromptBall);
    CC_SYNTHESIZE(long, _oneStarScore, OneStarScore);
    CC_SYNTHESIZE(long, _twoStarScore, TwoStarScore);
    CC_SYNTHESIZE(long, _threeStarScore, ThreeStarScore);
    
    void setSpecailBallDataV(std::vector<std::map<std::string,long>> var);
    
    std::vector<std::map<std::string,long>> getSpecailBallDataV();
    
//    std::vector<int> getNewPaoIndexV();
    
//    long transformWithLevelRandomNewPaoIndexV(long var);//所有泡泡都从此方法转化，得到本关统一规则转换后的泡泡
    
//    void cloneFromOtherLD(LevelData* tLvlData);
    
    CREATE_FUNC(LevelData);
    
private:
    
    int _level;
    
    std::vector<std::map<std::string,long>> _specailBallDataV;
    
//    std::vector<int> _newPaoIndexV;//新的泡泡顺序，将原来的1-5随机换位，得到新的1-5的排列，在得到数据的时候，按这个新排列重新整理数据
//    
//    void randomNewPaoNameV();
    
    
    void clearData();
};







#endif /* defined(__beerpaopao__LevelData__) */
