//
//  OwlObjectData.h
//  beerpaopao
//
//  Created by liupeng on 14/11/29.
//
//

#ifndef __beerpaopao__OwlObjectData__
#define __beerpaopao__OwlObjectData__

#include <iostream>
#include "../cocos2d.h"
#include "../extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class OwlObjectData : public Ref
{
public:
    
    virtual bool init();
    
    void setOwlObject(std::string OwlOb);
    std::string getOwlObject();
    
    std::vector<std::map<std::string,std::string>> getMakeNeed();
    void setMakeNeed(std::vector<std::map<std::string,std::string>> var);
    
    
    CREATE_FUNC(OwlObjectData);
    
    
private:
    
    std::vector<std::map<std::string,std::string>> _makeNeed;
    
    std::string _owlObject;
};


#endif /* defined(__beerpaopao__OwlObjectData__) */
