//
//  DataCenter.cpp
//  beerpaopao
//
//  Created by Frank on 14-9-4.
//
//

#include "DataCenter.h"
#include <fstream>

using namespace std;



static DataCenter *s_dataCenter = NULL;

DataCenter* DataCenter::getInstance()
{
    if (s_dataCenter == NULL)
    {
        s_dataCenter = new DataCenter();
    }
    
    return s_dataCenter;
}

void DataCenter::loadLocalData()
{
    getLocalTime();
    //进入游戏调用
    loadLocalizable();
    
    loadMapGuanUIData();
    
    loadOwlData();
}

void DataCenter::saveLocalData()
{
    //出游戏调用
}

void DataCenter::loadLocalizable()
{
    LanguageType language = Application::getInstance()->getCurrentLanguage();
    
    std::string directoryName;
    
    if (language == LanguageType::CHINESE)
    {
        directoryName = "res/CH";
    }
    else
    {
        directoryName = "res/EN";
    }
    
    std::string localizableFilePath = FileUtils::getInstance()->fullPathForFilename(directoryName+"/Localizable.json");
    
//    std::string newPath = FileUtils::getInstance()->getWritablePath()+"test.json";
    
    std::string newPath = localizableFilePath;
    
//    log("newPath = %s",newPath.c_str());

//    log("localizableFilePath = %s",localizableFilePath.c_str());
    
//    log("*********\n%s",FileUtils::getInstance()->getStringFromFile(localizableFilePath).c_str());
    
    rapidjson::Document docum;
    
    
    std::string fileString = FileUtils::getInstance()->getStringFromFile(newPath);

    docum.Parse<0>(fileString.c_str());
    
    rapidjson::Value jsonNameValue,plistNameValue,pngFilePathValue,fontStringValue,pngNameValue,armatureValue;
    
    jsonNameValue = docum["JsonName"];
    plistNameValue = docum["PlistName"];
    pngFilePathValue = docum["PngFilePath"];
    fontStringValue = docum["FontString"];
    pngNameValue = docum["PngName"];
    armatureValue = docum["ArmatureJson"];
    
    _armature.BeerBoyLeftPng = armatureValue["BeerBoyLeftPng"].GetString();
    _armature.BeerBoyLeftplist = armatureValue["BeerBoyLeftplist"].GetString();
    _armature.BeerBoyLeftJson = armatureValue["BeerBoyLeftJson"].GetString();
    _armature.BeerBoyCenterPng = armatureValue["BeerBoyCenterPng"].GetString();
    _armature.BeerBoyCenterplist = armatureValue["BeerBoyCenterplist"].GetString();
    _armature.BeerBoyCenterJson = armatureValue["BeerBoyCenterJson"].GetString();
    _armature.BeerBoyRightPng = armatureValue["BeerBoyRightPng"].GetString();
    _armature.BeerBoyRightplist = armatureValue["BeerBoyRightplist"].GetString();
    _armature.BeerBoyRightJson = armatureValue["BeerBoyRightJson"].GetString();
    _armature.BeerGirlLeftPng = armatureValue["BeerGirlLeftPng"].GetString();
    _armature.BeerGirlLeftplist = armatureValue["BeerGirlLeftplist"].GetString();
    _armature.BeerGirlLeftJson = armatureValue["BeerGirlLeftJson"].GetString();
    _armature.BeerGirlRightPng = armatureValue["BeerGirlRightPng"].GetString();
    _armature.BeerGirlRightplist = armatureValue["BeerGirlRightplist"].GetString();
    _armature.BeerGirlRightJson = armatureValue["BeerGirlRightJson"].GetString();
    
    
    _jsonNames.MapGuanUIData = jsonNameValue["MapGuanUIData"].GetString();
    _jsonNames.LevelData = jsonNameValue["LevelData"].GetString();
    _jsonNames.UserData = jsonNameValue["UserData"].GetString();
    _jsonNames.UserLevel = jsonNameValue["UserLevel"].GetString();
    _jsonNames.UserLevelStar = jsonNameValue["UserLevelStar"].GetString();
    _jsonNames.OwlData = jsonNameValue["OwlData"].GetString();
    _jsonNames.PropInfo = jsonNameValue["PropInfo"].GetString();
    _jsonNames.ShoppingMall = jsonNameValue["ShoppingMall"].GetString();
    
    _plistNames.GamePlayUI = plistNameValue["GamePlayUI"].GetString();
    _plistNames.GameSceneUI = plistNameValue["GameSceneUI"].GetString();
    _plistNames.ToolsUI = plistNameValue["ToolsUI"].GetString();
    _plistNames.BagLayerUI = plistNameValue["BagLayerUI"].GetString();
    _plistNames.PreFinishUI = plistNameValue["PreFinishUI"].GetString();
    _plistNames.FinishUI = plistNameValue["FinishUI"].GetString();
    _plistNames.ShoppingMallUI=plistNameValue["ShoppingMallUI"].GetString();
    _plistNames.OwlLayerUI=plistNameValue["OwlLayerUI"].GetString();
    _plistNames.FriendLayerUI=plistNameValue["FriendLayerUI"].GetString();
    _plistNames.MapBottomUI=plistNameValue["MapBottomUI"].GetString();
    
    _pngFilePaths.LoginBack = pngFilePathValue["LoginBack"].GetString();
    _pngFilePaths.MapFirstBack = pngFilePathValue["MapFirstBack"].GetString();
    _pngFilePaths.GamePlayBack = pngFilePathValue["GamePlayBack"].GetString();
//    _pngFilePaths.MapThirdBottom1 = pngFilePathValue["MapThirdBottom1"].GetString();
//    _pngFilePaths.MapThirdBottom2 = pngFilePathValue["MapThirdBottom2"].GetString();
    _pngFilePaths.MapThirdMid = pngFilePathValue["MapThirdMid"].GetString();
    _pngFilePaths.MapThirdMid1 = pngFilePathValue["MapThirdMid1"].GetString();
    _pngFilePaths.MapTop = pngFilePathValue["MapTop"].GetString();
    _pngFilePaths.ThirdLogin = pngFilePathValue["ThirdLogin"].GetString();
    _pngFilePaths.VisitorLogin = pngFilePathValue["VisitorLogin"].GetString();
    
    _fontStrings.Name = fontStringValue["Name"].GetString();
    _fontStrings.LevelString=fontStringValue["LevelString"].GetString();
    _fontStrings.WinString1=fontStringValue["WinString1"].GetString();
    _fontStrings.WinString2=fontStringValue["WinString2"].GetString();
    _fontStrings.UserBagList=fontStringValue["UserBagList"].GetString();
    
    
    _pngNames.BlueNormal = pngNameValue["BlueNormal"].GetString();
    _pngNames.BlueGiddy = pngNameValue["BlueGiddy"].GetString();
    _pngNames.BlueIce = pngNameValue["BlueIce"].GetString();
    _pngNames.BlueMud = pngNameValue["BlueMud"].GetString();
    
    _pngNames.GreenNormal = pngNameValue["GreenNormal"].GetString();
    _pngNames.GreenGiddy = pngNameValue["GreenGiddy"].GetString();
    _pngNames.GreenIce = pngNameValue["GreenIce"].GetString();
    _pngNames.GreenMud = pngNameValue["GreenMud"].GetString();
    
    _pngNames.PinkNormal = pngNameValue["PinkNormal"].GetString();
    _pngNames.PinkGiddy = pngNameValue["PinkGiddy"].GetString();
    _pngNames.PinkIce = pngNameValue["PinkIce"].GetString();
    _pngNames.PinkMud = pngNameValue["PinkMud"].GetString();
    
    _pngNames.RedNormal = pngNameValue["RedNormal"].GetString();
    _pngNames.RedGiddy = pngNameValue["RedGiddy"].GetString();
    _pngNames.RedIce = pngNameValue["RedIce"].GetString();
    _pngNames.RedMud = pngNameValue["RedMud"].GetString();
    
    _pngNames.YellowNormal = pngNameValue["YellowNormal"].GetString();
    _pngNames.YellowGiddy = pngNameValue["YellowGiddy"].GetString();
    _pngNames.YellowIce = pngNameValue["YellowIce"].GetString();
    _pngNames.YellowMud = pngNameValue["YellowMud"].GetString();
    
    _pngNames.BlueSNormal = pngNameValue["BlueSNormal"].GetString();
    _pngNames.BlueSGiddy = pngNameValue["BlueSGiddy"].GetString();
    _pngNames.BlueSIce = pngNameValue["BlueSIce"].GetString();
    _pngNames.BlueSMud = pngNameValue["BlueSMud"].GetString();
    
    _pngNames.GreenSNormal = pngNameValue["GreenSNormal"].GetString();
    _pngNames.GreenSGiddy = pngNameValue["GreenSGiddy"].GetString();
    _pngNames.GreenSIce = pngNameValue["GreenSIce"].GetString();
    _pngNames.GreenSMud = pngNameValue["GreenSMud"].GetString();
    
    _pngNames.PinkSNormal = pngNameValue["PinkSNormal"].GetString();
    _pngNames.PinkSGiddy = pngNameValue["PinkSGiddy"].GetString();
    _pngNames.PinkSIce = pngNameValue["PinkSIce"].GetString();
    _pngNames.PinkSMud = pngNameValue["PinkSMud"].GetString();
    
    _pngNames.RedSNormal = pngNameValue["RedSNormal"].GetString();
    _pngNames.RedSGiddy = pngNameValue["RedSGiddy"].GetString();
    _pngNames.RedSIce = pngNameValue["RedSIce"].GetString();
    _pngNames.RedSMud = pngNameValue["RedSMud"].GetString();
    
    _pngNames.YellowSNormal = pngNameValue["YellowSNormal"].GetString();
    _pngNames.YellowSGiddy = pngNameValue["YellowSGiddy"].GetString();
    _pngNames.YellowSIce = pngNameValue["YellowSIce"].GetString();
    _pngNames.YellowSMud = pngNameValue["YellowSMud"].GetString();
    
    _pngNames.SpiderWeb = pngNameValue["SpiderWeb"].GetString();
    _pngNames.DemonBall = pngNameValue["DemonBall"].GetString();
    _pngNames.StoneBall = pngNameValue["StoneBall"].GetString();
    _pngNames.TimeBomb = pngNameValue["TimeBomb"].GetString();
    _pngNames.TimeBall = pngNameValue["TimeBall"].GetString();
    _pngNames.Flashing = pngNameValue["Flashing"].GetString();
    _pngNames.ClarityBall = pngNameValue["ClarityBall"].GetString();
    _pngNames.Bee = pngNameValue["Bee"].GetString();
    _pngNames.BombBall = pngNameValue["BombBall"].GetString();
    _pngNames.MoreBall = pngNameValue["MoreBall"].GetString();
    
    _pngNames.Bomb = pngNameValue["Bomb"].GetString();
    _pngNames.Meteorite = pngNameValue["Meteorite"].GetString();
    _pngNames.Fan = pngNameValue["Fan"].GetString();
    
    _pngNames.WaterGun = pngNameValue["WaterGun"].GetString();
    _pngNames.Hammer = pngNameValue["Hammer"].GetString();
    _pngNames.MagicBang = pngNameValue["MagicBang"].GetString();
    _pngNames.IncreaseBall = pngNameValue["IncreaseBall"].GetString();
    
    _pngNames.RattanLine = pngNameValue["RattanLine"].GetString();
    
    //ToolsUI
    _pngNames.BagIcon = pngNameValue["BagIcon"].GetString();
    _pngNames.MissionIcon = pngNameValue["MissionIcon"].GetString();
    _pngNames.SettingIcon = pngNameValue["SettingIcon"].GetString();
    _pngNames.ShopIcon = pngNameValue["ShopIcon"].GetString();
    _pngNames.SignIcon = pngNameValue["SignIcon"].GetString();
    _pngNames.ToolCloud = pngNameValue["ToolCloud"].GetString();
    _pngNames.SpecialIcon = pngNameValue["SpecialIcon"].GetString();
    _pngNames.OwlSleep = pngNameValue["OwlSleep"].GetString();
    _pngNames.OwlWakeUp = pngNameValue["OwlWakeUp"].GetString();
    _pngNames.OwlCloud = pngNameValue["OwlCloud"].GetString();
    _pngNames.OwlCloudS = pngNameValue["OwlCloudS"].GetString();
    _pngNames.SleepPao = pngNameValue["SleepPao"].GetString();
    
    //PrePlayUI
    _pngNames.AddProp = pngNameValue["AddProp"].GetString();
    _pngNames.Close = pngNameValue["Close"].GetString();
    _pngNames.CloseS = pngNameValue["CloseS"].GetString();
    _pngNames.Coin = pngNameValue["Coin"].GetString();
    _pngNames.PreBackGroup = pngNameValue["PreBackGroup"].GetString();
    _pngNames.PriceBack = pngNameValue["PriceBack"].GetString();
    _pngNames.PropBack = pngNameValue["PropBack"].GetString();
    _pngNames.PropInfoButton = pngNameValue["PropInfoButton"].GetString();
    _pngNames.PropInfoButtonS = pngNameValue["PropInfoButtonS"].GetString();
    _pngNames.StartButton = pngNameValue["StartButton"].GetString();
    _pngNames.StartButtonS = pngNameValue["StartButtonS"].GetString();

    //FinishUI
    _pngNames.Fail_Back = pngNameValue["Fail_Back"].GetString();
    _pngNames.Fail_CloseNormal = pngNameValue["Fail_CloseNormal"].GetString();
    _pngNames.Fail_CloseSelected = pngNameValue["Fail_CloseSelected"].GetString();
    _pngNames.Fail_FunnyPNG = pngNameValue["Fail_FunnyPNG"].GetString();
    _pngNames.Fail_Level = pngNameValue["Fail_Level"].GetString();
    _pngNames.Fail_TryNormal = pngNameValue["Fail_TryNormal"].GetString();
    _pngNames.Fail_TrySelected = pngNameValue["Fail_TrySelected"].GetString();
    _pngNames.Fail_Word = pngNameValue["Fail_Word"].GetString();
    _pngNames.Win_Back = pngNameValue["Win_Back"].GetString();
    _pngNames.Win_CloseNormal = pngNameValue["Win_CloseNormal"].GetString();
    _pngNames.Win_CloseSelected = pngNameValue["Win_CloseSelected"].GetString();
    _pngNames.Win_Coin = pngNameValue["Win_Coin"].GetString();
    _pngNames.Win_Level = pngNameValue["Win_Level"].GetString();
    _pngNames.Win_NextNormal = pngNameValue["Win_NextNormal"].GetString();
    _pngNames.Win_NextSelected = pngNameValue["Win_NextSelected"].GetString();
    _pngNames.Win_Score = pngNameValue["Win_Score"].GetString();
    _pngNames.Win_ShareNormal = pngNameValue["Win_ShareNormal"].GetString();
    _pngNames.Win_ShareSelected = pngNameValue["Win_ShareSelected"].GetString();
    _pngNames.Win_StarEmpty = pngNameValue["Win_StarEmpty"].GetString();
    _pngNames.Win_StarFull = pngNameValue["Win_StarFull"].GetString();
    _pngNames.Win_Tili = pngNameValue["Win_Tili"].GetString();
    _pngNames.Win_Word = pngNameValue["Win_Word"].GetString();
    
    //BagLayerUI
    _pngNames.BagBack = pngNameValue["BagBack"].GetString();
    _pngNames.BagText = pngNameValue["BagText"].GetString();
    _pngNames.BagPropBack = pngNameValue["BagPropBack"].GetString();
    
    
    //ShoppingMallUI
    _pngNames.AddButton = pngNameValue["AddButton"].GetString();
    _pngNames.AddbuttonS = pngNameValue["AddbuttonS"].GetString();
    _pngNames.CoinBack = pngNameValue["CoinBack"].GetString();
    _pngNames.CoinLabel = pngNameValue["CoinLabel"].GetString();
    _pngNames.CoinLabelS = pngNameValue["CoinLabelS"].GetString();
    _pngNames.PackLabel = pngNameValue["PackLabel"].GetString();
    _pngNames.PackLabelS = pngNameValue["PackLabelS"].GetString();
    _pngNames.ShopPriceBack = pngNameValue["ShopPriceBack"].GetString();
    _pngNames.ShopPropBack = pngNameValue["ShopPropBack"].GetString();
    _pngNames.PropLabel = pngNameValue["PropLabel"].GetString();
    _pngNames.PropLabelS = pngNameValue["PropLabelS"].GetString();
    _pngNames.ShopLabel = pngNameValue["ShopLabel"].GetString();
    _pngNames.ShoppingBack = pngNameValue["ShoppingBack"].GetString();
    _pngNames.SmallCoin = pngNameValue["SmallCoin"].GetString();
    _pngNames.ShoppingGroud = pngNameValue["ShoppingGroud"].GetString();
    
    
    //PreFinishUI
    _pngNames.CountDownBack = pngNameValue["CountDownBack"].GetString();
    _pngNames.GoOnButton = pngNameValue["GoOnButton"].GetString();
    _pngNames.GoOnButtonS = pngNameValue["GoOnButtonS"].GetString();
    _pngNames.PaoPaoBag = pngNameValue["PaoPaoBag"].GetString();
    _pngNames.PreFinishBack = pngNameValue["PreFinishBack"].GetString();
    _pngNames.PriceButton = pngNameValue["PriceButton"].GetString();
    
    //OwlLayerUI
    _pngNames.OwlDialog = pngNameValue["OwlDialog"].GetString();
    _pngNames.VerticalLineS = pngNameValue["VerticalLineS"].GetString();
    _pngNames.VerticalLineL = pngNameValue["VerticalLineL"].GetString();
    _pngNames.OwlBranch = pngNameValue["OwlBranch"].GetString();
    _pngNames.BagBranch = pngNameValue["BagBranch"].GetString();
    _pngNames.OwlPropInfoUI = pngNameValue["OwlPropInfoUI"].GetString();
    _pngNames.OwlBagUI = pngNameValue["OwlBagUI"].GetString();
    
    //FriendLayerUI
    _pngNames.One = pngNameValue["One"].GetString();
    _pngNames.Two = pngNameValue["Two"].GetString();
    _pngNames.Three = pngNameValue["Three"].GetString();
    _pngNames.AddFriend = pngNameValue["AddFriend"].GetString();
    _pngNames.AddFriendS = pngNameValue["AddFriendS"].GetString();
    _pngNames.BlankBack = pngNameValue["BlankBack"].GetString();
    _pngNames.DelFriend = pngNameValue["DelFriend"].GetString();
    _pngNames.DelFriendS = pngNameValue["DelFriendS"].GetString();
    _pngNames.FriendBack = pngNameValue["FriendBack"].GetString();
    _pngNames.HeadBack = pngNameValue["HeadBack"].GetString();
    _pngNames.HeadImage = pngNameValue["HeadImage"].GetString();
    _pngNames.LineBack = pngNameValue["LineBack"].GetString();
    _pngNames.SelectBack = pngNameValue["SelectBack"].GetString();
    
    //MapBottomUI
    _pngNames.MapGuanBottom1 = pngNameValue["MapGuanBottom1"].GetString();
    _pngNames.MapGuanBottom2 = pngNameValue["MapGuanBottom2"].GetString();
    _pngNames.MapGuanBottom3 = pngNameValue["MapGuanBottom3"].GetString();
    _pngNames.MapBottomLeaf1 = pngNameValue["MapBottomLeaf1"].GetString();
    _pngNames.MapBottomLeaf2 = pngNameValue["MapBottomLeaf2"].GetString();
    _pngNames.MapBottomLeaf3 = pngNameValue["MapBottomLeaf3"].GetString();
    _pngNames.LeafRight = pngNameValue["LeafRight"].GetString();
    _pngNames.LeafLeft = pngNameValue["LeafLeft"].GetString();

//    rapidjson::Value temV;
//    
////    temV.SetString("111");
//    temV.SetObject();
//    
////    temV.AddMember("1", "a", docum.GetAllocator());
//    docum["FontString"]["Name"].SetString("aaa");
//    rapidjson::StringBuffer buf;
//    rapidjson::Writer<rapidjson::StringBuffer> w(buf);
//    docum.Accept(w);
//    
//    log("*****************************");
//    log("%s",buf.GetString());
//    log("*****************************");
//    
////    rapidjson::FileStream *files;
////    
////    files->Put('c');
    
}

void DataCenter::loadLevelPlayData(int bigLevel)
{
    //读取游戏json文件
    
    std::string localizableFilePath = FileUtils::getInstance()->fullPathForFilename(_jsonNames.LevelData + turnIntToString(bigLevel) + ".json");
    
//    log("loadLevelPlayData localizableFilePath = %s",localizableFilePath.c_str());
    
//    log("loadLevelPlayData %s",FileUtils::getInstance()->getStringFromFile(localizableFilePath).c_str());
    
    std::string gameLevelDataStr = FileUtils::getInstance()->getStringFromFile(localizableFilePath);
    
    _gameLevelDataStrMap[bigLevel] = gameLevelDataStr;
}

void DataCenter::loadMapGuanUIData()
{
    std::string mapGuanUIDataFilePath = FileUtils::getInstance()->fullPathForFilename(_jsonNames.MapGuanUIData + ".json");
    
    std::string mapGuanUIDataStr = FileUtils::getInstance()->getStringFromFile(mapGuanUIDataFilePath);
    
//    log("mapGuanUIDataStr :\n%s",mapGuanUIDataStr.c_str());
    
    rapidjson::Document docum;
    
    docum.Parse<0>(mapGuanUIDataStr.c_str());
    
    int dataCount = docum["Count"].GetInt();
    
    log("dataCount = %d",dataCount);
    
    rapidjson::Value dataValue;
    dataValue = docum["Data"];
    
    for (int index = 0; index < dataCount; index++)
    {
        MapGuanUIData temMapGuanUIData;
        
        rapidjson::Value value;
        value = dataValue[index];
        
        temMapGuanUIData.NormalImage = value["NormalImage"].GetString();
        temMapGuanUIData.SelectedImage = value["SelectedImage"].GetString();
        temMapGuanUIData.PositionX = value["PositionX"].GetDouble();
        temMapGuanUIData.PositionY = value["PositionY"].GetDouble();

        _mapGuanUIDataVector.push_back(temMapGuanUIData);
        
//        log("temMapGuanUIData  n:%s,s:%s,x:%g,y:%g",temMapGuanUIData.NormalImage.c_str(),temMapGuanUIData.SelectedImage.c_str(),temMapGuanUIData.PositionX,temMapGuanUIData.PositionY);
    }
    
//    for (int index = 0; index < dataCount; index++)
//    {
//        std::string nI = _mapGuanUIDataVector[index].NormalImage;
//        std::string sI = _mapGuanUIDataVector[index].SelectedImage;
//        float x = _mapGuanUIDataVector[index].PositionX;
//        float y = _mapGuanUIDataVector[index].PositionY;
//        
//        log("VECTOR  n:%s,s:%s,x:%g,y:%g",nI.c_str(),sI.c_str(),x,y);
//    }
}

LocalizablePngFilePath DataCenter::getPngFilePaths()
{
    return _pngFilePaths;
}
LocalizableFontString DataCenter::getFontStrings()
{
    return _fontStrings;
}
LocalizablePlistName DataCenter::getPlistNames()
{
    return _plistNames;
}
LocalizableJsonName DataCenter::getJsonNames()
{
    return _jsonNames;
}
LocalizablePngName DataCenter::getPngNames()
{
    return _pngNames;
}

std::vector<MapGuanUIData> DataCenter::getMapGuanUIDataVector()
{
    return _mapGuanUIDataVector;
}
ArmatureJson DataCenter::getArmature()
{
    return _armature;
}

LocalizablePngFilePath DataCenter::pngFilePaths()
{
    return s_dataCenter->getPngFilePaths();
}
LocalizableFontString DataCenter::fontStrings()
{
    return s_dataCenter->getFontStrings();
}
LocalizablePlistName DataCenter::plistNames()
{
    return s_dataCenter->getPlistNames();
}
LocalizableJsonName DataCenter::jsonNames()
{
    return s_dataCenter->getJsonNames();
}
LocalizablePngName DataCenter::pngNames()
{
    return s_dataCenter->getPngNames();
}
std::vector<MapGuanUIData> DataCenter::mapGuanUIDataVector()
{
    return s_dataCenter->getMapGuanUIDataVector();
}
ArmatureJson DataCenter::armatureJson()
{
    return s_dataCenter->getArmature();
}
LevelData* DataCenter::getLevelData(int levelNum)
{
    int bigLevel = (levelNum-1)/10;
    
    if (_gameLevelDataStrMap[bigLevel].length() == 0)
    {
        loadLevelPlayData(bigLevel);
    }
    
//    log("_gameLevelDataStrMap[bigLevel] \n%s",_gameLevelDataStrMap[bigLevel].c_str());
    
    LevelData *targetLevelData = LevelData::create();
    
    targetLevelData->setLevel(levelNum);
    
    std::string levelStr = turnIntToString(levelNum);
//    log("levelStr = \n%s",levelStr.c_str());
    rapidjson::Document docum;
    
    docum.Parse<0>(_gameLevelDataStrMap[bigLevel].c_str());
    
    int row = docum[levelStr.c_str()]["LevelDataBall"]["LevelDataUp"]["Row"].GetInt();
    int col = docum[levelStr.c_str()]["LevelDataBall"]["LevelDataUp"]["Col"].GetInt();
    
    std::vector<std::vector<long>> upDataV;
    
    for (int indexR = 0; indexR < row; indexR++)
    {
        std::vector<long> rDataV;
        
        for (int indexC = 0; indexC < col; indexC++)
        {
            rDataV.push_back(turnStringToLong(docum[levelStr.c_str()]["LevelDataBall"]["LevelDataUp"]["Data"][indexR][indexC].GetString()));
        }
        
        upDataV.push_back(rDataV);
    }
    
    int count = docum[levelStr.c_str()]["LevelDataBall"]["LevelDataDown"]["Count"].GetInt();
    
    std::vector<long> downDataV;
    
    for (int indexCo = 0; indexCo < count; indexCo++)
    {
        long temN = turnStringToLong(docum[levelStr.c_str()]["LevelDataBall"]["LevelDataDown"]["Data"][indexCo].GetString());
        if (temN == 0)
        {
            break;
        }
        downDataV.push_back(temN);
    }
    
    int winType = docum[levelStr.c_str()]["StyleToWin"]["Type"].GetInt();
    
    int gameSeconds = docum[levelStr.c_str()]["StyleToWin"]["TimeData"]["Seconds"].GetInt();
    long gameWinScore = turnStringToLong(docum[levelStr.c_str()]["StyleToWin"]["TimeData"]["Score"].GetString());
    
    int specialBallCount = docum[levelStr.c_str()]["StyleToWin"]["SpecialBallCount"].GetInt();
    
    std::vector<std::map<std::string,long>> specailBallDataV;
    
    rapidjson::Value specailBallValue;
    specailBallValue = docum[levelStr.c_str()]["StyleToWin"]["SpecialBallData"];
    
    for (int indexSBDV = 0; indexSBDV < specialBallCount; indexSBDV++)
    {
        std::map<std::string,long> spbMap;
        
        spbMap["Data"] = turnStringToLong(specailBallValue[indexSBDV]["Data"].GetString());
        spbMap["Count"] = turnStringToLong(specailBallValue[indexSBDV]["Count"].GetString());
        
        specailBallDataV.push_back(spbMap);
    }
    
    
    long prompt = turnStringToLong(docum[levelStr.c_str()]["PromptData"].GetString());
    long oneStarScore = turnStringToLong(docum[levelStr.c_str()]["OneStarScore"].GetString());
    long twoStarScore = turnStringToLong(docum[levelStr.c_str()]["TwoStarScore"].GetString());
    long threeStarScore = turnStringToLong(docum[levelStr.c_str()]["ThreeStarScore"].GetString());
    
    targetLevelData->setLevel(levelNum);
    targetLevelData->setRow(row);
    targetLevelData->setCol(col);
    targetLevelData->setBallUpVector(upDataV);
    targetLevelData->setBallDownCount(count);
    targetLevelData->setBallDwonVector(downDataV);
    targetLevelData->setWinType(winType);
    targetLevelData->setGameSeconds(gameSeconds);
    targetLevelData->setGameWinScore(gameWinScore);
    targetLevelData->setSpecailBallDataV(specailBallDataV);
    targetLevelData->setPromptBall(prompt);
    targetLevelData->setOneStarScore(oneStarScore);
    targetLevelData->setTwoStarScore(twoStarScore);
    targetLevelData->setThreeStarScore(threeStarScore);
    targetLevelData->setSpecialBallCount(specialBallCount);
    
    return targetLevelData;
}

/*
PaoPaoData DataCenter::getPaoPaoDataWithNum(long paoNum)
{
 
    long statuNum = paoNum%100;
    long nameNum = (paoNum/100)%100;
    long styleNum = (paoNum/10000)%100;
    
    std::string spFrameName;
    PaoStyle pStyle = PaoStyleQiuNormal;
    PaoStatu pStatu = PaoStatuNormal;
    PaoName pName = PaoNameBlue;
    
    if (styleNum == 14)
    {
        pStyle = PaoStyleDaoJuDown;
        
        if (nameNum == 3)
        {
            pName = PaoNameFan;
            spFrameName = DataCenter::pngNames().Fan;
        }
        else if (nameNum == 2)
        {
            pName = PaoNameMeteorite;
            spFrameName = DataCenter::pngNames().Meteorite;
        }
        else
        {
            pName = PaoNameBomb;
            spFrameName = DataCenter::pngNames().Bomb;
        }
    }
    else if (styleNum == 13)
    {
        pStyle = PaoStyleDaoJuUp;
        
        if (nameNum == 10)
        {
            pName = PaoNameMoreBall;
            spFrameName = DataCenter::pngNames().MoreBall;
        }
        else if (nameNum == 9)
        {
            pName = PaoNameBombBall;
            spFrameName = DataCenter::pngNames().BombBall;
        }
        else if (nameNum == 8)
        {
            pName = PaoNameBee;
            spFrameName = DataCenter::pngNames().Bee;
        }
        else if (nameNum == 7)
        {
            pName = PaoNameClarityBall;
            spFrameName = DataCenter::pngNames().ClarityBall;
        }
        else if (nameNum == 6)
        {
            pName = PaoNameFlashing;
            spFrameName = DataCenter::pngNames().Flashing;
        }
        else if (nameNum == 5)
        {
            pName = PaoNameTimeBall;
            spFrameName = DataCenter::pngNames().TimeBall;
        }
        else if (nameNum == 4)
        {
            pName = PaoNameTimeBomb;
            spFrameName = DataCenter::pngNames().TimeBomb;
        }
        else if (nameNum == 3)
        {
            pName = PaoNameStoneBall;
            spFrameName = DataCenter::pngNames().StoneBall;
        }
        else if (nameNum == 2)
        {
            pName = PaoNameDemonBall;
            spFrameName = DataCenter::pngNames().DemonBall;
        }
        else
        {
            pName = PaoNameSpiderWeb;
            spFrameName = DataCenter::pngNames().SpiderWeb;
        }
    }
    else if (styleNum == 12)
    {
        pStyle = PaoStyleQiuSpecial;
        
        if (nameNum == 0)
        {
            struct timeval tpTimeA;
            gettimeofday(&tpTimeA,NULL);
            srand(tpTimeA.tv_usec);
            
            nameNum = rand()%5+1;
        }
        
        pStatu = getPaoStatuWithNum(statuNum);
        pName = getPaoNameWithNum(nameNum);
        
        if (nameNum == 1)
        {
            switch (pStatu)
            {
                case 3:
                    spFrameName = DataCenter::pngNames().BlueSGiddy;
                    break;
                case 2:
                    spFrameName = DataCenter::pngNames().BlueSMud;
                    break;
                case 1:
                    spFrameName = DataCenter::pngNames().BlueSIce;
                    break;
                    
                default:
                    spFrameName = DataCenter::pngNames().BlueSNormal;
                    break;
            }
        }
        else if (nameNum == 2)
        {
            switch (pStatu)
            {
                case 3:
                    spFrameName = DataCenter::pngNames().GreenSGiddy;
                    break;
                case 2:
                    spFrameName = DataCenter::pngNames().GreenSMud;
                    break;
                case 1:
                    spFrameName = DataCenter::pngNames().GreenSIce;
                    break;
                    
                default:
                    spFrameName = DataCenter::pngNames().GreenSNormal;
                    break;
            }
        }
        else if (nameNum == 3)
        {
            switch (pStatu)
            {
                case 3:
                    spFrameName = DataCenter::pngNames().PinkSGiddy;
                    break;
                case 2:
                    spFrameName = DataCenter::pngNames().PinkSMud;
                    break;
                case 1:
                    spFrameName = DataCenter::pngNames().PinkSIce;
                    break;
                    
                default:
                    spFrameName = DataCenter::pngNames().PinkSNormal;
                    break;
            }
        }
        else if (nameNum == 4)
        {
            switch (pStatu)
            {
                case 3:
                    spFrameName = DataCenter::pngNames().RedSGiddy;
                    break;
                case 2:
                    spFrameName = DataCenter::pngNames().RedSMud;
                    break;
                case 1:
                    spFrameName = DataCenter::pngNames().RedSIce;
                    break;
                    
                default:
                    spFrameName = DataCenter::pngNames().RedSNormal;
                    break;
            }
        }
        else
        {
            switch (pStatu)
            {
                case 3:
                    spFrameName = DataCenter::pngNames().YellowSGiddy;
                    break;
                case 2:
                    spFrameName = DataCenter::pngNames().YellowSMud;
                    break;
                case 1:
                    spFrameName = DataCenter::pngNames().YellowSIce;
                    break;
                    
                default:
                    spFrameName = DataCenter::pngNames().YellowSNormal;
                    break;
            }
        }
    }
    else
    {
        pStyle = PaoStyleQiuNormal;
        
        pStatu = getPaoStatuWithNum(statuNum);
        
        if (nameNum == 0)
        {
            struct timeval tpTimeA;
            gettimeofday(&tpTimeA,NULL);
            srand(tpTimeA.tv_usec);
            
            nameNum = rand()%5+1;
        }
        
        pName = getPaoNameWithNum(nameNum);
        
        if (nameNum == 1)
        {
            switch (pStatu)
            {
                case 3:
                    spFrameName = DataCenter::pngNames().BlueGiddy;
                    break;
                case 2:
                    spFrameName = DataCenter::pngNames().BlueMud;
                    break;
                case 1:
                    spFrameName = DataCenter::pngNames().BlueIce;
                    break;
                    
                default:
                    spFrameName = DataCenter::pngNames().BlueNormal;
                    break;
            }
        }
        else if (nameNum == 2)
        {
            switch (pStatu)
            {
                case 3:
                    spFrameName = DataCenter::pngNames().GreenGiddy;
                    break;
                case 2:
                    spFrameName = DataCenter::pngNames().GreenMud;
                    break;
                case 1:
                    spFrameName = DataCenter::pngNames().GreenIce;
                    break;
                    
                default:
                    spFrameName = DataCenter::pngNames().GreenNormal;
                    break;
            }
        }
        else if (nameNum == 3)
        {
            switch (pStatu)
            {
                case 3:
                    spFrameName = DataCenter::pngNames().PinkGiddy;
                    break;
                case 2:
                    spFrameName = DataCenter::pngNames().PinkMud;
                    break;
                case 1:
                    spFrameName = DataCenter::pngNames().PinkIce;
                    break;
                    
                default:
                    spFrameName = DataCenter::pngNames().PinkNormal;
                    break;
            }
        }
        else if (nameNum == 4)
        {
            switch (pStatu)
            {
                case 3:
                    spFrameName = DataCenter::pngNames().RedGiddy;
                    break;
                case 2:
                    spFrameName = DataCenter::pngNames().RedMud;
                    break;
                case 1:
                    spFrameName = DataCenter::pngNames().RedIce;
                    break;
                    
                default:
                    spFrameName = DataCenter::pngNames().RedNormal;
                    break;
            }
        }
        else
        {
            switch (pStatu)
            {
                case 3:
                    spFrameName = DataCenter::pngNames().YellowGiddy;
                    break;
                case 2:
                    spFrameName = DataCenter::pngNames().YellowMud;
                    break;
                case 1:
                    spFrameName = DataCenter::pngNames().YellowIce;
                    break;
                    
                default:
                    spFrameName = DataCenter::pngNames().YellowNormal;
                    break;
            }
        }
    }
    
    
    temPaoPaoData.TargetPaoFrameName = spFrameName;
    temPaoPaoData.TargetPaoStyle = pStyle;
    temPaoPaoData.TargetPaoStatu = pStatu;
    temPaoPaoData.TargetPaoName = pName;
    
    
    return temPaoPaoData;
}
*/
    
//PaoStatu DataCenter::getPaoStatuWithNum(long paoStatuN)
//{
//    if (paoStatuN == 5)
//    {
//        return PaoStatuRattanSource;
//    }
//    else if (paoStatuN == 4)
//    {
//        return PaoStatuRattanCover;
//    }
//    else if (paoStatuN == 3)
//    {
//        return PaoStatuGiddy;
//    }
//    else if (paoStatuN == 2)
//    {
//        return PaoStatuMud;
//    }
//    else if (paoStatuN == 1)
//    {
//        return PaoStatuIce;
//    }
//    else
//    {
//        return PaoStatuNormal;
//    }
//}

//PaoName DataCenter::getPaoNameWithNum(long PaoNameN)
//{
//    if (PaoNameN == 5)
//    {
//        return PaoNameYellow;
//    }
//    else if (PaoNameN == 4)
//    {
//        return PaoNameRed;
//    }
//    else if (PaoNameN == 3)
//    {
//        return PaoNamePink;
//    }
//    else if (PaoNameN == 2)
//    {
//        return PaoNameGreen;
//    }
//    else
//    {
//        return PaoNameBlue;
//    }
//}

UserData* DataCenter::getUserData()
{
    _userData = UserData::create();

    std::string userDataFilePath = FileUtils::getInstance()->getWritablePath() + _jsonNames.UserData + ".json";
    std::string userDataStr = FileUtils::getInstance()->getStringFromFile(userDataFilePath);

//    log("userDataFilePath :\n%s",userDataFilePath.c_str());
    
    _userDataDocum.Parse<0>(userDataStr.c_str());
    
    std::vector<std::map<std::string,std::string>> userBagPropV;
    std::vector<std::map<std::string,std::string>> userBagSpecialBallV;
    
    for (int i = 0; i < _userDataDocum["UserBagProp"].Size(); i++)
    {
        std::map<std::string,std::string> porpMap;
        
        porpMap["Data"] = _userDataDocum["UserBagProp"][i]["Data"].GetString();
        porpMap["Count"] = _userDataDocum["UserBagProp"][i]["Count"].GetString();
        
        userBagPropV.push_back(porpMap);
    }
    
    for (int i = 0; i < _userDataDocum["UserBagSpecialBall"].Size(); i++)
    {
        std::map<std::string,std::string> spbMap;
        
        spbMap["Data"] = _userDataDocum["UserBagSpecialBall"][i]["Data"].GetString();
        spbMap["Count"] = _userDataDocum["UserBagSpecialBall"][i]["Count"].GetString();
        
        userBagSpecialBallV.push_back(spbMap);
    }
    

    std::string systemId=_userDataDocum["SystemId"].GetString();
    std::string userId=_userDataDocum["UserId"].GetString();
    std::string userNick=_userDataDocum["UserNick"].GetString();
    std::string userComeFrom=_userDataDocum["UserComeFrom"].GetString();
    std::string headImg=_userDataDocum["HeadImg"].GetString();
    
    int userCoin=_userDataDocum["UserCoin"].GetInt();
    int userStar=_userDataDocum["UserStar"].GetInt();
    int userLevel=_userDataDocum["UserLevel"].GetInt();
    int owlLevel=_userDataDocum["OwlLevel"].GetInt();
    int userVip=_userDataDocum["UserVip"].GetInt();
    int userMonth=_userDataDocum["UserMonth"].GetInt();
    
    _userData->setSystemId(systemId);
    _userData->setUserId(userId);
    _userData->setUserNick(userNick);
    _userData->setUserComeFrom(userComeFrom);
    _userData->setHeadImg(headImg);
    _userData->setUserBagProp(userBagPropV);
    _userData->setUserBagSpecialBall(userBagSpecialBallV);
    _userData->setUserCoin(userCoin);
    _userData->setUserStar(userStar);
    _userData->setUserLevel(userLevel);
    _userData->setOwlLevel(owlLevel);
    _userData->setUserVip(userVip);
    _userData->setUserMonth(userMonth);
    
    return _userData;
}

std::map<int,int> DataCenter::getUserLevelStart(int userLevel)
{
    std::map<int,int> userLevelStart;
    
    if (_userLevelStarDocum.IsNull())
    {
        std::string userLevelStartFilePath = FileUtils::getInstance()->fullPathForFilename(_jsonNames.UserLevelStar + ".json");
        
        std::string userLevelStartStr = FileUtils::getInstance()->getStringFromFile(userLevelStartFilePath);
        
//        log("userLevelStartStr :\n%s",userLevelStartStr.c_str());
        
        _userLevelStarDocum.Parse<0>(userLevelStartStr.c_str());
    }
    
//    log("loadUserLevelStart :\n%d",docum.Size());
    
    for (int i=1; i<=userLevel; i++)
    {
        
        userLevelStart[i]=_userLevelStarDocum[turnIntToString(i).c_str()].GetInt();
    }
    return userLevelStart;
}

UserLevel* DataCenter::getUserLevel(int levelNum)
{
    _currentBigLevel = (levelNum-1)/10;
    
    
//    std::string userLevelFilePath = FileUtils::getInstance()->fullPathForFilename(_jsonNames.UserLevel + turnIntToString(_currentBigLevel) + ".json");
    std::string userLevelFilePath = FileUtils::getInstance()->getWritablePath() + _jsonNames.UserLevel + turnIntToString(_currentBigLevel) + ".json";
    
    std::string userLevelStr = FileUtils::getInstance()->getStringFromFile(userLevelFilePath);
    
    UserLevel* userLevel=UserLevel::create();
    
    userLevel->setLevel(levelNum);
    
    std::string levelStr = turnIntToString(levelNum);
    
//    rapidjson::Document docum;
    _userLevelDocum.Parse<0>(userLevelStr.c_str());
//    log("userLevelFilePath:---%s",userLevelFilePath.c_str());
    std::string winScore=_userLevelDocum[turnIntToString(levelNum).c_str()]["WinScore"].GetString();
    int playCount=_userLevelDocum[turnIntToString(levelNum).c_str()]["PlayCount"].GetInt();
    int failCount=_userLevelDocum[turnIntToString(levelNum).c_str()]["FailCount"].GetInt();
    
    userLevel->setWinScore(winScore);
    userLevel->setPlayCount(playCount);
    userLevel->setFailCount(failCount);
    
    return userLevel;
}

void DataCenter::saveUserData(UserData* userData)
{
    rapidjson::StringBuffer buf;
    rapidjson::Writer<rapidjson::StringBuffer> w(buf);
    
//    rapidjson::Document docum;
//    std::string userDataFilePath = FileUtils::getInstance()->fullPathForFilename(_jsonNames.UserData + ".json");
//    
//    std::string userDataStr = FileUtils::getInstance()->getStringFromFile(userDataFilePath);
//    
//    log("userData :\n%s",userDataStr.c_str());
//    
//    docum.Parse<0>(userDataStr.c_str());
    
    _userDataDocum["SystemId"].SetString(userData->getSystemId().c_str());
    _userDataDocum["UserId"].SetString(userData->getUserId().c_str());
    _userDataDocum["UserNick"].SetString(userData->getUserNick().c_str());
    _userDataDocum["UserComeFrom"].SetString(userData->getUserComeFrom().c_str());
    _userDataDocum["HeadImg"].SetString(userData->getHeadImg().c_str());
    
    _userDataDocum["UserCoin"].SetInt(userData->getUserCoin());
    _userDataDocum["UserStar"].SetInt(userData->getUserStar());
    _userDataDocum["UserLevel"].SetInt(userData->getUserLevel());
    _userDataDocum["OwlLevel"].SetInt(userData->getOwlLevel());
    _userDataDocum["UserVip"].SetInt(userData->getUserVip());
    _userDataDocum["UserMonth"].SetInt(userData->getUserMonth());
    
//    log("SystemId:---%s",_userDataDocum["SystemId"].GetString());
    
    std::vector<std::map<std::string,std::string>> userBagPropV=userData->getUserBagProp();
    std::vector<std::map<std::string,std::string>> userBagSpecialBallV=userData->getUserBagSpecialBall();
    
    for (int i=0; i<userBagPropV.size(); i++) {
        
        _userDataDocum["UserBagProp"][i]["Data"].SetString(userBagPropV[i]["Data"].c_str());
        _userDataDocum["UserBagProp"][i]["Count"].SetString(userBagPropV[i]["Count"].c_str());
    }
    
    for (int i=0; i<userBagSpecialBallV.size(); i++) {
        
        _userDataDocum["UserBagSpecialBall"][i]["Data"].SetString(userBagSpecialBallV[i]["Data"].c_str());
        _userDataDocum["UserBagSpecialBall"][i]["Count"].SetString(userBagSpecialBallV[i]["Count"].c_str());
    }
    
    std::string newPath = FileUtils::getInstance()->getWritablePath()+DataCenter::getJsonNames().UserData + ".json";
    _userDataDocum.Accept(w);
    
    
    log("newPath:---%s",newPath.c_str());
//    log("buf:---%s",buf.GetString());
    FILE* file = fopen(newPath.c_str(), "wb");
    if (file) {
        fputs(buf.GetString(), file);
        fclose(file);
    }
}

void DataCenter::saveLevel(int levelID,int levelStar,bool isFail,std::string WinScore)
{
    saveLevelStar(levelID,levelStar);
    saveLevelData(levelID,isFail,WinScore);
}

void DataCenter::saveLevelStar(int levelID,int levelStar)
{
    rapidjson::StringBuffer buf;
    rapidjson::Writer<rapidjson::StringBuffer> w(buf);
    
    std::string userLevelStartFilePath = FileUtils::getInstance()->getWritablePath() + _jsonNames.UserLevelStar +".json";
    if (_userLevelStarDocum.IsNull())
    {
        std::string userLevelStartStr = FileUtils::getInstance()->getStringFromFile(userLevelStartFilePath);
        
        _userLevelStarDocum.Parse<0>(userLevelStartStr.c_str());
    }
    
    if (!_userLevelStarDocum.HasMember(turnIntToString(levelID).c_str()))
    {
        _userLevelStarDocum.AddMember(turnIntToString(levelID).c_str(), levelStar, _userLevelStarDocum.GetAllocator());
    }
    else
    {
        _userLevelStarDocum[turnIntToString(levelID).c_str()].SetInt(levelStar);
    }
    
    std::string newPath = FileUtils::getInstance()->getWritablePath()+"test.json";
    _userLevelStarDocum.Accept(w);
    
//    log("_userLevelStarDocum---%s",userLevelStartFilePath.c_str());
    FILE* file = fopen(userLevelStartFilePath.c_str(), "wb");
    if (file)
    {
        fputs(buf.GetString(), file);
        fclose(file);
    }
}

void DataCenter::saveLevelData(int levelID,bool isFail,std::string WinScore)
{
    rapidjson::StringBuffer buf;
    rapidjson::Writer<rapidjson::StringBuffer> w(buf);
    
    std::string userLevelFilePath = FileUtils::getInstance()->getWritablePath() + _jsonNames.UserLevel +turnIntToString(_currentBigLevel) +".json";
    
    if (_userLevelDocum.IsNull())
    {
        _currentBigLevel = (levelID-1)/10;
        
        std::string userLevelStr = FileUtils::getInstance()->getStringFromFile(userLevelFilePath);
        
        UserLevel* userLevel=UserLevel::create();
        
        userLevel->setLevel(levelID);
        
        std::string levelStr = turnIntToString(levelID);
        
        //    rapidjson::Document docum;
        _userLevelDocum.Parse<0>(userLevelStr.c_str());
    }
    
    if (!_userLevelDocum.HasMember(turnIntToString(levelID).c_str()))
    {
        UserLevel *userLevel=UserLevel::create();
        userLevel=getUserLevel(levelID);
        
        int playCount=_userLevelDocum[turnIntToString(levelID).c_str()]["PlayCount"].GetInt()+1;
        int playFail=_userLevelDocum[turnIntToString(levelID).c_str()]["PlayCount"].GetInt();
        if (isFail)
        {
            playFail=playFail+1;
        }
        _userLevelDocum[turnIntToString(levelID).c_str()]["WinScore"].SetString(WinScore.c_str());
        _userLevelDocum[turnIntToString(levelID).c_str()]["PlayCount"].SetInt(playCount);
        _userLevelDocum[turnIntToString(levelID).c_str()]["FailCount"].SetInt(playFail);
        
    }
    else
    {
        int playCount=_userLevelDocum[turnIntToString(levelID).c_str()]["PlayCount"].GetInt()+1;
        int playFail=_userLevelDocum[turnIntToString(levelID).c_str()]["PlayCount"].GetInt();
        if (isFail)
        {
            playFail=playFail+1;
        }
        _userLevelDocum[turnIntToString(levelID).c_str()]["WinScore"].SetString(WinScore.c_str());
        _userLevelDocum[turnIntToString(levelID).c_str()]["PlayCount"].SetInt(playCount);
        _userLevelDocum[turnIntToString(levelID).c_str()]["FailCount"].SetInt(playFail);
    }
    
    std::string newPath = FileUtils::getInstance()->getWritablePath()+"UserLevel" + turnIntToString(_currentBigLevel) +"test.json";
    _userLevelDocum.Accept(w);
    
    log("_userLevelDocum---%s",userLevelFilePath.c_str());
    
    FILE* file = fopen(userLevelFilePath.c_str(), "wb");
    if (file)
    {
        fputs(buf.GetString(), file);
        fclose(file);
    }
}

void DataCenter::loadOwlData()
{
    /*
    std::string filePath = FileUtils::getInstance()->fullPathForFilename(_jsonNames.OwlData+".json");
    
    std::string fileString = FileUtils::getInstance()->getStringFromFile(filePath);
    
//    log("OwlData : \n%s",fileString.c_str());
    
    rapidjson::Document docum;
    docum.Parse<0>(fileString.c_str());
    
    rapidjson::Value owlLevelUpDataValue,owlObjectValue;
    
    owlLevelUpDataValue = docum["OwlLevelUpData"];
    owlObjectValue = docum["OwlObject"];
    
    _owlMaxLevel = owlLevelUpDataValue["Max"].GetInt();
    
    for (int index = _owlMaxLevel; index <= 0; index--)
    {
        rapidjson::Value temOwlLevelUpDataValue;
        
        temOwlLevelUpDataValue = owlLevelUpDataValue[turnIntToString(index).c_str()];
        
        OwlData *temOwlData = OwlData::create();
        
        temOwlData->setOwlLevel(index);
        
        temOwlData->setBombCustomLevel(temOwlLevelUpDataValue["Property"]["BombCustomLevel"].GetInt());
        temOwlData->setMeteoriteCustomLevel(temOwlLevelUpDataValue["Property"]["MeteoriteCustom"].GetInt());
        temOwlData->setFanCustomLevel(temOwlLevelUpDataValue["Property"]["FanCustom"].GetInt());
        temOwlData->setTiLi(temOwlLevelUpDataValue["Property"]["TiLi"].GetBool());
        
        
        
        if (index == _owlMaxLevel)
        {
            _owlDataM[index] = temOwlData;
            continue;
        }
        
        std::map<std::string, int> temMap;
        
        temMap["Coins"] = temOwlLevelUpDataValue["Need"]["Coins"].GetInt();
        temMap["120100"] = temOwlLevelUpDataValue["Need"]["120100"].GetInt();
        temMap["120200"] = temOwlLevelUpDataValue["Need"]["120200"].GetInt();
        temMap["120300"] = temOwlLevelUpDataValue["Need"]["120300"].GetInt();
        temMap["120400"] = temOwlLevelUpDataValue["Need"]["120400"].GetInt();
        temMap["120500"] = temOwlLevelUpDataValue["Need"]["120500"].GetInt();
        temMap["Special"] = temOwlLevelUpDataValue["Need"]["Special"].GetInt();
        
        temOwlData->setUnlockNeedMap(temMap);
        
        _owlDataM[index] = temOwlData;
    }
    
    _bombCustomLevelMax = owlObjectValue["BombCustomLevel"]["Max"].GetInt();
    
    for (int bIndex = _bombCustomLevelMax; bIndex > 0; bIndex--)
    {
        rapidjson::Value temBombCustomLevelValue;
        temBombCustomLevelValue = owlObjectValue["BombCustomLevel"][turnIntToString(bIndex).c_str()];
        
        CustomPaoDestructive temDes;
        
        temDes.m_maxDestructive = temBombCustomLevelValue["Max"].GetInt();
        temDes.m_minDestructive = temBombCustomLevelValue["Min"].GetInt();
        
        _bombCustomLevelMap[bIndex] = temDes;
    }
    
    _meteoriteCustomLevelMax = owlObjectValue["MeteoriteCustomLevel"]["Max"].GetInt();
    
    for (int bIndex = _meteoriteCustomLevelMax; bIndex > 0; bIndex--)
    {
        rapidjson::Value temBombCustomLevelValue;
        temBombCustomLevelValue = owlObjectValue["MeteoriteCustomLevel"][turnIntToString(bIndex).c_str()];
        
        CustomPaoDestructive temDes;
        
        temDes.m_maxDestructive = temBombCustomLevelValue["Max"].GetInt();
        temDes.m_minDestructive = temBombCustomLevelValue["Min"].GetInt();
        
        _meteoriteCustomLevelMap[bIndex] = temDes;
    }
    
    _fanCustomLevelMax = owlObjectValue["FanCustomLevel"]["Max"].GetInt();
    
    for (int bIndex = _fanCustomLevelMax; bIndex > 0; bIndex--)
    {
        rapidjson::Value temBombCustomLevelValue;
        temBombCustomLevelValue = owlObjectValue["FanCustomLevel"][turnIntToString(bIndex).c_str()];
        
        CustomPaoDestructive temDes;
        
        temDes.m_maxDestructive = temBombCustomLevelValue["Max"].GetInt();
        temDes.m_minDestructive = temBombCustomLevelValue["Min"].GetInt();
        
        _fanCustomLevelMap[bIndex] = temDes;
    }
    */
}


OwlData* DataCenter::getOwlDataWithOwlLevel(int owlLevelNum)
{
    OwlData *temOwlData = OwlData::create();
    
//    if (_owlDataDocum.IsNull()) {
        std::string filePath = FileUtils::getInstance()->fullPathForFilename(_jsonNames.OwlData+".json");
        
        std::string fileString = FileUtils::getInstance()->getStringFromFile(filePath);
        
        rapidjson::Document docum;
        docum.Parse<0>(fileString.c_str());
        
//        log("fileString--%s",fileString.c_str());
        //        _tiLiIncrease=docum["tiLiIncrease"].GetInt();
        _owlDataDocum = docum["OwlLevelUpData"];
        _owlObjectDataDocum = docum["OwlObject"];
        temOwlData->setTiLi(docum["tiLiIncrease"].GetInt());
//    }
    
    temOwlData->setOwlLevel(owlLevelNum);
    
    _owlMaxLevel = _owlDataDocum["Max"].GetInt();
    
    std::vector<std::map<std::string,std::string>> owlUpNeedV;
    std::vector<std::map<std::string,std::string>> owlPropertyV;
    
    rapidjson::Value temOwlLevelNeed,temOwlLevelProperty;
    
    temOwlLevelNeed = _owlDataDocum[turnIntToString(owlLevelNum).c_str()]["Need"];
    
    temOwlLevelProperty = _owlDataDocum[turnIntToString(owlLevelNum).c_str()]["Property"];
    
    for (int i=0; i < temOwlLevelNeed.Size(); i++)
    {
        std::map<std::string,std::string> owlMap;
        
        owlMap["Data"]=temOwlLevelNeed[i]["Data"].GetString();
        owlMap["Count"]=temOwlLevelNeed[i]["Count"].GetString();
        
        owlUpNeedV.push_back(owlMap);
        
    }
    
    temOwlData->setUnlockNeedMap(owlUpNeedV);
    
    for (int i=0; i < temOwlLevelProperty.Size(); i++)
    {
        std::map<std::string,std::string> owlMap;
        
        owlMap["Data"]=temOwlLevelProperty[i]["Data"].GetString();
        owlMap["LevelMax"]=temOwlLevelProperty[i]["LevelMax"].GetString();
        owlMap["LevelMin"]=temOwlLevelProperty[i]["LevelMin"].GetString();
        
        owlPropertyV.push_back(owlMap);
    }
    
    temOwlData->setOwlPropertyMap(owlPropertyV);
    
    return temOwlData;
}

OwlObjectData* DataCenter::getOwlObjectData(std::string owlObject)
{
//    if (_owlObjectDataDocum.IsNull()) {
        std::string filePath = FileUtils::getInstance()->fullPathForFilename(_jsonNames.OwlData+".json");
        
        std::string fileString = FileUtils::getInstance()->getStringFromFile(filePath);
        
        rapidjson::Document docum;
        docum.Parse<0>(fileString.c_str());
        
        _owlDataDocum = docum["OwlLevelUpData"];
        _owlObjectDataDocum = docum["OwlObject"];
        
//    }
    
//    log("_owlObjectDataDocum--%s",_owlObjectDataDocum.GetString());
    
    _owlObject=owlObject;
    OwlObjectData *temOwlObject = OwlObjectData::create();
    temOwlObject->setOwlObject(owlObject);
    
    rapidjson::Value temOwlObjectNeed;
    temOwlObjectNeed=_owlObjectDataDocum[owlObject.c_str()];
    
    std::vector<std::map<std::string,std::string>> owlObjectNeedV;
    
    for (int i=0; i<temOwlObjectNeed.Size(); i++) {
        std::map<std::string,std::string> owlMap;
        
        owlMap["Data"]=temOwlObjectNeed[i]["Data"].GetString();
        owlMap["Count"]=temOwlObjectNeed[i]["Count"].GetString();
        
        owlObjectNeedV.push_back(owlMap);
    }
    
    temOwlObject->setMakeNeed(owlObjectNeedV);
    
    return temOwlObject;
}

int DataCenter::getOwlMaxLevel()
{
    return _owlMaxLevel;
}
//int DataCenter::getBombCustomLevelMax()
//{
//    return _bombCustomLevelMax;
//}
//int DataCenter::getMeteoriteCustomLevelMax()
//{
//    return _meteoriteCustomLevelMax;
//}
//int DataCenter::getFanCustomLevelMax()
//{
//    return _fanCustomLevelMax;
//}
int DataCenter::getTiLiIncrease()
{
    return _tiLiIncrease;
}

std::string DataCenter::getOwlObject()
{
    return _owlObject;
}

//CustomPaoDestructive DataCenter::getCurrentBombCustomDes(int var)
//{
//    return _bombCustomLevelMap[var];
//}
//CustomPaoDestructive DataCenter::getCurrentMeteoriteCustomDes(int var)
//{
//    return _meteoriteCustomLevelMap[var];
//}
//CustomPaoDestructive DataCenter::getCurrentFanCustomDes(int var)
//{
//    return _fanCustomLevelMap[var];
//}

PropInfo* DataCenter::getPropInfo(std::string pName)
{
    
    if (_propInfoDocum.IsNull()) {
        std::string propInfoFilePath = FileUtils::getInstance()->fullPathForFilename(_jsonNames.PropInfo  + ".json");
        
        std::string propInfoStr = FileUtils::getInstance()->getStringFromFile(propInfoFilePath);
        
        _propInfoDocum.Parse<0>(propInfoStr.c_str());
    }
    
    PropInfo* propInfo=PropInfo::create();
    
    //    rapidjson::Document docum;
    //    log("userLevelFilePath:---%s",userLevelFilePath.c_str());
    std::string propName=_propInfoDocum[pName.c_str()]["name"].GetString();
    std::string pInfo=_propInfoDocum[pName.c_str()]["info"].GetString();
    std::string pngName=_propInfoDocum[pName.c_str()]["pngName"].GetString();
    
    propInfo->setPropName(propName);
    propInfo->setInfo(pInfo);
    propInfo->setPngName(pngName);
    
    return propInfo;
}


ShoppingMall* DataCenter::getShopping(){
    if (_shoppingDocum.IsNull()) {
        std::string shopingFilePath = FileUtils::getInstance()->fullPathForFilename(_jsonNames.ShoppingMall  + ".json");
        
        std::string shopingStr = FileUtils::getInstance()->getStringFromFile(shopingFilePath);
        
        _shoppingDocum.Parse<0>(shopingStr.c_str());
        //        log("_shoppingDocum--PropPack---%s",shopingStr.c_str());
    }
    
    std::vector<std::map<std::string,std::string>> propPack;
    
    for (int i=0; i<_shoppingDocum["PropPack"].Size(); i++) {
        std::map<std::string,std::string> spbMap;
        
        spbMap["nameString"] = _shoppingDocum["PropPack"][i]["nameString"].GetString();
        spbMap["originalPrice"] = _shoppingDocum["PropPack"][i]["originalPrice"].GetString();
        spbMap["paoCoin"] = _shoppingDocum["PropPack"][i]["paoCoin"].GetString();
        spbMap["pngName"] = _shoppingDocum["PropPack"][i]["pngName"].GetString();
        
        propPack.push_back(spbMap);
    }
    
    std::vector<std::map<std::string,std::string>> propShop;
    for (int i=0; i<_shoppingDocum["PropShop"].Size(); i++) {
        std::map<std::string,std::string> spbMap;
        
        spbMap["nameString"] = _shoppingDocum["PropPack"][i]["nameString"].GetString();
        spbMap["originalPrice"] = _shoppingDocum["PropPack"][i]["originalPrice"].GetString();
        spbMap["paoCoin"] = _shoppingDocum["PropPack"][i]["paoCoin"].GetString();
        spbMap["pngName"] = _shoppingDocum["PropPack"][i]["pngName"].GetString();
        
        propShop.push_back(spbMap);
    }
    
    std::vector<std::map<std::string,std::string>> coinShop;
    for (int i=0; i<_shoppingDocum["CoinShop"].Size(); i++) {
        std::map<std::string,std::string> spbMap;
        
        spbMap["nameString"] = _shoppingDocum["CoinShop"][i]["nameString"].GetString();
        spbMap["originalPrice"] = _shoppingDocum["CoinShop"][i]["originalCoin"].GetString();
        spbMap["extraCoin"] = _shoppingDocum["CoinShop"][i]["extraCoin"].GetString();
        spbMap["paoCoin"] = _shoppingDocum["CoinShop"][i]["paoCoin"].GetString();
        spbMap["pngName"] = _shoppingDocum["CoinShop"][i]["pngName"].GetString();
        
        coinShop.push_back(spbMap);
    }
    
    ShoppingMall* shoppingMall=ShoppingMall::create();
    
    shoppingMall->setPropPack(propPack);
    shoppingMall->setPropShop(propShop);
    shoppingMall->setCoinShop(coinShop);
    
    
    return shoppingMall;
}


std::vector<NetBackFriendData> DataCenter::getFriendsList(std::string FDStr){
    std::vector<NetBackFriendData> friendsList;
    
    rapidjson::Document docum;
    docum.Parse<0>(FDStr.c_str());
    
    rapidjson::Value friendDataDocum;
    friendDataDocum = docum["userFirendList"];
    
    for (int i=0; i<friendDataDocum.Size(); i++) {
        NetBackFriendData friends;
        
        friends.SystemId=friendDataDocum[i]["SystemId"].GetString();
//        friends.UserNick=friendDataDocum[i]["UserNick"].GetString();
//        friends.UserSex=friendDataDocum[i]["UserSex"].GetString();
//        friends.HeadImage=friendDataDocum[i]["HeadImage"].GetString();
        friends.HeadImage="HeadImage.png";
//        friends.Latitude=friendDataDocum[i]["Latitude"].GetString();
//        friends.Longitude=friendDataDocum[i]["Longitude"].GetString();
//        friends.LevelID=friendDataDocum[i]["LevelID"].GetString();
//        friends.StartNum=friendDataDocum[i]["UserStar"].GetString();
        friends.LevelID="1";
        friends.StartNum="3";
        
        friendsList.push_back(friends);
    }
    
    return friendsList;
}





















