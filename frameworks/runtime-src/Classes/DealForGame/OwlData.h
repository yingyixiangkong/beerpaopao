//
//  OwlData.h
//  beerpaopao
//
//  Created by Frank on 14-10-27.
//
//

#ifndef __beerpaopao__OwlData__
#define __beerpaopao__OwlData__

#include <iostream>
#include "../cocos2d.h"
#include "../extensions/cocos-ext.h"
#include "DataModel.h"

USING_NS_CC;
USING_NS_CC_EXT;

//class DataModel;

class OwlData : public Ref
{
public:
    
    virtual bool init();
    
    CC_SYNTHESIZE(int, _owlLevel, OwlLevel);
    
    CC_SYNTHESIZE(int, _tiLi, TiLi);
    
    
    std::vector<std::map<std::string,std::string>> getUnlockNeedMap();
    void setUnlockNeedMap(std::vector<std::map<std::string,std::string>> var);
    
    std::vector<std::map<std::string,std::string>> getOwlPropertyMap();
    void setOwlPropertyMap(std::vector<std::map<std::string,std::string>> var);
    
    CustomPaoDestructive getOwlProperty(std::string owlObiect);
    
    CREATE_FUNC(OwlData);
    
    
private:
    
    std::vector<std::map<std::string,std::string>> _unlockNeedMap;
    std::vector<std::map<std::string,std::string>> _proPertMap;
};

#endif /* defined(__beerpaopao__OwlData__) */
