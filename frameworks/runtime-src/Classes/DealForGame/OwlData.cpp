//
//  OwlData.cpp
//  beerpaopao
//
//  Created by Frank on 14-10-27.
//
//

#include "OwlData.h"


USING_NS_CC;
USING_NS_CC_EXT;



bool OwlData::init()
{
    return true;
}



std::vector<std::map<std::string,std::string>> OwlData::getUnlockNeedMap()
{
    return _unlockNeedMap;
}

void OwlData::setUnlockNeedMap(std::vector<std::map<std::string,std::string>> var)
{
    _unlockNeedMap = var;
}

void OwlData::setOwlPropertyMap(std::vector<std::map<std::string,std::string>> var)
{
    _proPertMap = var;
}

std::vector<std::map<std::string,std::string>> OwlData::getOwlPropertyMap()
{
    return _proPertMap;
}

CustomPaoDestructive OwlData::getOwlProperty(std::string owlObject)
{
    CustomPaoDestructive aa;
    
    size_t len=_proPertMap.size();
//    log("len-----%zu",len);
    for (int  i=0; i<len; i++) {
        std::map<std::string,std::string> owlMap=_proPertMap[i];
//        log("LevelMax-----%s",owlMap["LevelMax"].c_str());
        if (std::strcmp(owlObject.c_str(), owlMap["Data"].c_str())==0) {
            aa.m_maxDestructive=turnStringToInt(owlMap["LevelMax"]);
            aa.m_minDestructive=turnStringToInt(owlMap["LevelMin"]);
        }
    }
    
    return aa;
}