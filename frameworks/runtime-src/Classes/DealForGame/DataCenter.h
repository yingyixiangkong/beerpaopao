//
//  DataCenter.h
//  beerpaopao
//
//  Created by Frank on 14-9-4.
//
//

#ifndef __beerpaopao__DataCenter__
#define __beerpaopao__DataCenter__

#include <iostream>
#include "../cocos2d.h"
#include "../extensions/cocos-ext.h"
#include "../external/json/rapidjson.h"
#include "../external/json/document.h"
#include "../external/json/writer.h"
#include "../external/json/stringbuffer.h"
#include "../external/json/filestream.h"
#include "LevelData.h"
#include "DataModel.h"
#include "UserData.h"
#include "OwlData.h"
#include "OwlObjectData.h"
#include "UserLevel.h"
#include "PropInfo.h"
#include "ShoppingMall.h"

USING_NS_CC;
USING_NS_CC_EXT;


class DataCenter
{
public:
    static DataCenter* getInstance();
    
    void loadLocalData();
    void saveLocalData();
    void loadMapGuanUIData();
    
    static LocalizablePngFilePath pngFilePaths();
    static LocalizableFontString fontStrings();
    static LocalizablePlistName plistNames();
    static LocalizableJsonName jsonNames();
    static LocalizablePngName pngNames();
    
    static ArmatureJson armatureJson();
    
    static std::vector<MapGuanUIData> mapGuanUIDataVector();
    
//    PaoPaoData getPaoPaoDataWithNum(long paoNum);
    
    LevelData* getLevelData(int levelNum);
    
    std::map<int,int> getUserLevelStart(int userLevel);
    UserData* getUserData();
    UserLevel* getUserLevel(int levelNum);
    PropInfo* getPropInfo(std::string pName);
    ShoppingMall* getShopping();
    
    OwlData* getOwlDataWithOwlLevel(int owlLevelNum);
    OwlObjectData* getOwlObjectData(std::string owlObject);
    
    std::vector<NetBackFriendData> getFriendsList(std::string FDStr);
    
    int getOwlMaxLevel();
//    int getBombCustomLevelMax();
//    int getMeteoriteCustomLevelMax();
//    int getFanCustomLevelMax();
    int getTiLiIncrease();
    std::string getOwlObject();
    
//    CustomPaoDestructive getCurrentBombCustomDes(int var);
//    CustomPaoDestructive getCurrentMeteoriteCustomDes(int var);
//    CustomPaoDestructive getCurrentFanCustomDes(int var);
    
    
    void saveLevel(int levelID,int levelStar,bool isFail,std::string WinScore);
    void saveUserData(UserData* userData);
    
protected:
    
    LocalizablePngFilePath getPngFilePaths();
    LocalizableFontString getFontStrings();
    LocalizablePlistName getPlistNames();
    LocalizableJsonName getJsonNames();
    LocalizablePngName getPngNames();
    std::vector<MapGuanUIData> getMapGuanUIDataVector();
    
    ArmatureJson getArmature();
    
private:
    
    LocalizablePngFilePath _pngFilePaths;
    LocalizableFontString _fontStrings;
    LocalizablePlistName _plistNames;
    LocalizableJsonName _jsonNames;
    LocalizablePngName _pngNames;
    
    ArmatureJson _armature;
    
    UserData *_userData;
    
    std::vector<MapGuanUIData> _mapGuanUIDataVector;
    
    std::map<int, std::string> _gameLevelDataStrMap;
    
    int _owlMaxLevel;
    
//    std::map<int, OwlData*> _owlDataM;
//    
//    int _bombCustomLevelMax;
//    int _meteoriteCustomLevelMax;
//    int _fanCustomLevelMax;
    int _tiLiIncrease;
    std::string _owlObject;
    
    std::map<int, CustomPaoDestructive> _bombCustomLevelMap;
    std::map<int, CustomPaoDestructive> _meteoriteCustomLevelMap;
    std::map<int, CustomPaoDestructive> _fanCustomLevelMap;
    
    
    void loadLocalizable();
    void loadLevelPlayData(int bigLevel);
    void loadOwlData();
    
    
//    PaoStatu getPaoStatuWithNum(long paoStatuN);
//    PaoName getPaoNameWithNum(long PaoNameN);
    
    int _currentBigLevel;
    
    rapidjson::Document _userLevelStarDocum;
    rapidjson::Document _userLevelDocum;
    rapidjson::Document _userDataDocum;
    rapidjson::Document _propInfoDocum;
    rapidjson::Document _shoppingDocum;
    rapidjson::Value _owlDataDocum;
    rapidjson::Value _owlObjectDataDocum;
    
    void saveLevelStar(int levelID,int levelStar);
    void saveLevelData(int levelID,bool isFail,std::string WinScore);
};



#endif /* defined(__beerpaopao__DataCenter__) */
