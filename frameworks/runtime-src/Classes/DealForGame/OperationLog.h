//
//  OperationLog.h
//  beerpaopao
//
//  Created by liupeng on 14-10-29.
//
//

#ifndef __beerpaopao__OperationLog__
#define __beerpaopao__OperationLog__

#include <iostream>
#include "../cocos2d.h"
#include "../extensions/cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class OperationLog : Ref{
public:
    virtual bool init();
    //本地操作记录
    CC_SYNTHESIZE(std::string, _operateDateTime, OperateDateTime);
    CC_SYNTHESIZE(std::string, _operateLog, OperateLog);//记录具体操作
    CC_SYNTHESIZE(std::string, _clientAppVersion, ClientAppVersion);
    CC_SYNTHESIZE(int, _logState, LogState);//状态－－是否提交服务器成功
    
    
    CREATE_FUNC(OperationLog);
private:
    
    int _levelNum;
};

#endif /* defined(__beerpaopao__OperationLog__) */
