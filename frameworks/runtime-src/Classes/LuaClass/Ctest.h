//
//  Ctest.h
//  luaTest
//
//  Created by liupeng on 14-10-7.
//
//

#ifndef __luaTest__Ctest__
#define __luaTest__Ctest__

#include <iostream>

class CTest

{
    
public:
    
    CTest(void);
    
    ~CTest(void);
    
    
    
    char* GetData();
    
    void SetData(const char* pData);
    
    
    
private:
    
    char m_szData[200];
    
};

#endif /* defined(__luaTest__Ctest__) */
