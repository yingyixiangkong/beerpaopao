#include "AppDelegate.h"
#include "SceneForGame/LoginScene.h"
#include "DealForGame/DataCenter.h"

USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        glview = GLView::create("My Game");
        director->setOpenGLView(glview);
    }
    
    glview->setDesignResolutionSize(1080.0, 1920.0, ResolutionPolicy::SHOW_ALL);
    
    // turn on display FPS
    director->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);
    
    log("W : %g, H : %g, X : %g, Y : %g",VisibleSize.width,VisibleSize.height,Origin.x,Origin.y);
    
    
    int a = 2;
    
    
    int b = 1;
    
    long c = 1;
    
    for (int index = 0; index < 32; index++)
    {
        b = b*a;
        c = c*a;
        
        log("a : %d,b : %d,c : %ld",a,b,c);
    }
    
    ScutExt::Init("src");
    ScriptEngineProtocol *mmengine = LuaEngine::getInstance();
    ScriptEngineManager::getInstance()->setScriptEngine(mmengine);
    mmengine->executeScriptFile("src/mainLua.lua");
    
    DataCenter::getInstance()->loadLocalData();
    
//    DataCenter::getInstance()->getLevelData(1);
    
    // create a scene. it's an autorelease object
    auto scene = LoginScene::newLoginScene();

    // run
    director->runWithScene(scene);
    
    log("PATH = %s",FileUtils::getInstance()->getWritablePath().c_str());
    
    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
