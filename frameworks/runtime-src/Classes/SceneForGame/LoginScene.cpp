//
//  LoginScene.cpp
//  beerpaopao
//
//  Created by Frank on 14-9-4.
//
//

#include "LoginScene.h"
#include "UserData.h"



Scene* LoginScene::newLoginScene()
{
    auto newLoginS = Scene::create();
    auto newLoginL = LoginScene::create();
    
    newLoginS->addChild(newLoginL);
    
    newLoginL->preLoad();
    
    return newLoginS;
}
bool LoginScene::init()
{
    if (!Layer::init())
    {
        return false;
    }
    
    auto backSprite = Sprite::create(DataCenter::pngFilePaths().LoginBack);
    backSprite->setPosition(Origin.x + VisibleSize.width/2, Origin.y + VisibleSize.height/2);
    
    this->addChild(backSprite);
    
    return true;
}

void LoginScene::preLoad()
{
    std::vector<std::string> gamePlayPlistV;
    gamePlayPlistV.push_back(DataCenter::plistNames().GamePlayUI);
    
    for (int index = 0; index < gamePlayPlistV.size(); index++)
    {
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile(gamePlayPlistV[index]+".plist", gamePlayPlistV[index] + ".png");
    }
    
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("res/GamePlay/destroy_effect.plist","res/GamePlay/destroy_effect.png");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("res/GamePlay/destroy_effectA.plist","res/GamePlay/destroy_effectA.png");
    std::vector<std::string> gameScenePlistV;
    gameScenePlistV.push_back(DataCenter::plistNames().GameSceneUI);
    
    for (int index = 0; index < gameScenePlistV.size(); index++)
    {
        SpriteFrameCache::getInstance()->addSpriteFramesWithFile(gameScenePlistV[index]+".plist", gameScenePlistV[index] + ".png");
    }
    
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(DataCenter::plistNames().MapBottomUI+".plist", DataCenter::plistNames().MapBottomUI + ".png");
    
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(DataCenter::plistNames().ToolsUI+".plist", DataCenter::plistNames().ToolsUI + ".png");
    
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(DataCenter::plistNames().PreFinishUI+".plist", DataCenter::plistNames().PreFinishUI + ".png");

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(DataCenter::plistNames().FinishUI+".plist", DataCenter::plistNames().FinishUI + ".png");

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(DataCenter::plistNames().BagLayerUI+".plist", DataCenter::plistNames().BagLayerUI + ".png");
    
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(DataCenter::plistNames().ShoppingMallUI+".plist", DataCenter::plistNames().ShoppingMallUI + ".png");
    
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(DataCenter::plistNames().OwlLayerUI+".plist", DataCenter::plistNames().OwlLayerUI + ".png");
    
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(DataCenter::plistNames().FriendLayerUI+".plist", DataCenter::plistNames().FriendLayerUI + ".png");
    
    
    
    std::vector<std::string> pngFilePathV;
    pngFilePathV.push_back(DataCenter::pngFilePaths().LoginBack);
    pngFilePathV.push_back(DataCenter::pngFilePaths().MapFirstBack);
    pngFilePathV.push_back(DataCenter::pngFilePaths().GamePlayBack);
//    pngFilePathV.push_back(DataCenter::pngFilePaths().MapThirdBottom1);
//    pngFilePathV.push_back(DataCenter::pngFilePaths().MapThirdBottom2);
    pngFilePathV.push_back(DataCenter::pngFilePaths().MapThirdMid);
    pngFilePathV.push_back(DataCenter::pngFilePaths().ThirdLogin);
    pngFilePathV.push_back(DataCenter::pngFilePaths().VisitorLogin);
    
    _fileCount = pngFilePathV.size();
    
    for (int index = 0; index < pngFilePathV.size(); index++)
    {
        Director::getInstance()->getTextureCache()->addImageAsync(pngFilePathV[index], CC_CALLBACK_1(LoginScene::preLoadCallback, this));
    }
    
}

void LoginScene::preLoadCallback(cocos2d::Ref *pSender)
{
    _fileCount -= 1;
    
    log("fileCount----***---%d",_fileCount);
    
    
    if (_fileCount == 0)
    {
        checkIfLogin();
    }
    
    
    
}

void LoginScene::checkIfLogin()
{
    addOtherUI(false);
}


void LoginScene::addOtherUI(bool isLogin)
{
    if (isLogin)
    {
        
    }
    else
    {
        auto visitorLoginMenuItemImage = MenuItemImage::create(DataCenter::pngFilePaths().VisitorLogin, DataCenter::pngFilePaths().VisitorLogin, CC_CALLBACK_1(LoginScene::onMenuItemCallback, this));
        
        visitorLoginMenuItemImage->setPosition(Origin.x + VisibleSize.width/2, Origin.y + 100);
        visitorLoginMenuItemImage->setTag(100);
        
        auto thirdLoginMenuItemImage = MenuItemImage::create(DataCenter::pngFilePaths().ThirdLogin, DataCenter::pngFilePaths().ThirdLogin, CC_CALLBACK_1(LoginScene::onMenuItemCallback, this));
        thirdLoginMenuItemImage->setPosition(visitorLoginMenuItemImage->getPositionX(), visitorLoginMenuItemImage->getPositionY() + 150);
        thirdLoginMenuItemImage->setTag(101);
        
        auto loginMenu = Menu::create(visitorLoginMenuItemImage,thirdLoginMenuItemImage, NULL);
        loginMenu->setPosition(Point::ZERO);
        
        this->addChild(loginMenu);
        
    }
    
}



void LoginScene::onMenuItemCallback(Ref *pSender)
{
    MenuItemImage *sender = (MenuItemImage*)pSender;
    
//    log("TAG = %d",sender->getTag());
//    UserData *userdata=UserData::create();
//    userdata=DataCenter::getInstance()->getUserData();
//    
//    std::map<int,int> userLevelStart;
//    userLevelStart=DataCenter::getInstance()->getUserLevelStart(userdata->getUserLevel());
//    
//    UserLevel *userLevel=UserLevel::create();
//    userLevel=DataCenter::getInstance()->getUserLevel(userdata->getUserLevel());
//    
//    DataCenter::getInstance()->saveLevel(userdata->getUserLevel(), 3, 1,"10000");
//    userdata->setUserId("user100000");
//    userdata->setSystemId("rrrrrrrrr");
//    DataCenter::getInstance()->saveUserData(userdata);
    
    NetWorkData *netData=NetWorkData::newNetWorkData();
    netData->sendNewUser(1, "abcd", "iqwabcd11", "111222", "123554");
//    netData->requestFriendsList("user11", "0", "0", 0);
    
    
    if (sender->getTag() == 100)
    {
        auto mapS = MapScene::newMapScene();
        
        Director::getInstance()->replaceScene(mapS);
    }
}

