//
//  UserBagLayer.h
//  beerpaopao
//
//  Created by liupeng on 14/11/14.
//
//

#ifndef __beerpaopao__UserBagLayer__
#define __beerpaopao__UserBagLayer__

#include <stdio.h>

#include "../DealForGame/DataCenter.h"
#include "CocosGUI.h"
#include "../SpecialSprite/PaoPao.h"

USING_NS_CC;
USING_NS_CC_EXT;

class UserData;
class PropInfo;
class UserBagLayer;
class UserBagLayerDelegate
{
public:

    virtual void onBagCloseClick();

};

class UserBagLayer : public Layer,virtual public extension::ScrollViewDelegate
{
public:
    static UserBagLayer* newUserBagLayer(UserBagLayerDelegate *delegate);
    
    virtual bool init();
    
    void setDelegate(UserBagLayerDelegate *delegate);
    
    CREATE_FUNC(UserBagLayer);
    
private:
    UserData *_userData;
    PropInfo *_propInfo;
    UserBagLayerDelegate *_delegate;
    extension::ScrollView *bagScrollView;
    virtual void scrollViewDidScroll(extension::ScrollView* view);
    virtual void scrollViewDidZoom(extension::ScrollView* view);
    
    Layer* bagRowLayer();
    Layer* _propInfoLayer;
    void addBaseUI();
    
    void onCloseMenuClick();
    void onBagClick(float x,float y,std::string pName);
};
#endif /* defined(__beerpaopao__UserBagLayer__) */
