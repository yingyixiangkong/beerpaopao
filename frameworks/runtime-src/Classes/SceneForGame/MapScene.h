//
//  MapScene.h
//  beerpaopao
//
//  Created by Frank on 14-9-5.
//
//

#ifndef __beerpaopao__MapScene__
#define __beerpaopao__MapScene__

#include <iostream>

#include "../DealForGame/DataCenter.h"
#include "cocostudio/CocoStudio.h"
#include "MapGuanLayer.h"
#include "GamePrePlayLayer.h"
#include "GamePlayScene.h"
#include "MapToolsLayer.h"
#include "GameFinishLayer.h"
#include "UserBagLayer.h"
#include "ShoppingLayer.h"
#include "OwlLayer.h"
#include "FriendsListLayer.h"
#include "../DealForGame/NetWorkData.h"


USING_NS_CC;
USING_NS_CC_EXT;


class MapScene : public Layer,virtual public extension::ScrollViewDelegate,virtual public MapGuanLayerDelegate,virtual public GamePrePlayLayerDelegate,virtual public MapToolsLayerDelegate,virtual public GameFinishLayerDelegate,virtual public UserBagLayerDelegate,virtual public GamePlaySceneDelegate,virtual public ShoppingLayerDelegate,virtual public OwlLayerDelegate,virtual public FriendsListLayerDelegate
{
public:
    static Scene* newMapScene();
    
    virtual bool init();
    
    void setFriendsData(std::vector<NetBackFriendData> friendsList,int FDType);
    
    CREATE_FUNC(MapScene);
    
protected:
    Point lastOffP;
    
private:
    
    extension::ScrollView *mapScrollView;
    MapGuanLayer *mapGuanL;
    bool scrollTopSwitch;
    
    FriendsListLayer *_frLayer;
    
    bool isSleep;
    Layer* owlMapLayer;
    Sprite* owlSleep;
    Sprite* owlCloud;
    Sprite* owlCloudS;
    Sprite* owlCloudS1;
    Sprite* sleepPao;
    EventListenerTouchOneByOne *_owlListener;
    
    EventListenerTouchOneByOne *_layerListener;
    
    void addFirstLayer();
    void addSecondLayer();
    void addThirdLayer();
    void addFourthLayer();
    void addOwl();
    void WakeUpOwl(float dt);
    void SleepOwl(float dt);
    
    virtual void scrollViewDidScroll(extension::ScrollView* view);
    virtual void scrollViewDidZoom(extension::ScrollView* view);
    
    bool onTouchOwl(Touch *touch, Event *unused_event);
    
    bool onTouchBegan(Touch *touch, Event *unused_event);
    
    virtual void onMenuItemClick(MenuItemImage *pSender);
    
    virtual void onStartGameClick();
    virtual void onClosePreLayerClick();
    
    virtual void onToolBarMenuItemClick(int pSender);
    
    GamePrePlayLayer* _prePlayLayer;
    GameFinishLayer* _finishLayer;
    
    void showFinishLayer(bool isWin,FinishPlayData fD);
    void showGamePreLayer(int level);
    void showUserBagLayer();
    void showShoppingLayer();
    void showOwlLayer();
    void showFriendListLayer();
    
//    virtual void gameFinish(bool s);
    
    virtual FinishPlayData getFinishPlayData();
    virtual void finishLayerClose();
    virtual void finishLayerNext(int levelId);
    virtual void finishLayerShare(int levelId);
    virtual void finishLayerRetry(int levelId);
    
    int _currenLevel;
    LevelData* _levelData;
    UserData* _userData;
    
    virtual LevelData* getPerLevel();
    virtual UserData* getPerUserData();
    
    
    virtual void onBagCloseClick();
    
    virtual void onShopCloseClick();
    
    virtual void onOwlCloseClick();
    
    virtual void onFDListCloseClick();

};

#endif /* defined(__beerpaopao__MapScene__) */
