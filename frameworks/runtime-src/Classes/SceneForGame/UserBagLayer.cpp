//
//  UserBagLayer.cpp
//  beerpaopao
//
//  Created by liupeng on 14/11/14.
//
//

#include "UserBagLayer.h"

UserBagLayer* UserBagLayer::newUserBagLayer(UserBagLayerDelegate* delegate)
{
    UserBagLayer *userBagLayer = UserBagLayer::create();
    
    userBagLayer->setDelegate(delegate);
    
    userBagLayer->addBaseUI();
    
    //    log("newGmaePrePlayLayer-----%d",levelNum);
    
    return userBagLayer;
}

bool UserBagLayer::init()
{
    if (!Layer::init())
    {
        return false;
    }
    
    return true;
}

void UserBagLayer::setDelegate(UserBagLayerDelegate *delegate)
{
    _delegate = delegate;
}

void UserBagLayer::addBaseUI()
{
    auto backLayerColor = LayerColor::create(Color4B(0, 0, 0, 100), VisibleSize.width, VisibleSize.height);
    
    this->addChild(backLayerColor);
    
//    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(DataCenter::plistNames().GamePlayUI+".plist", DataCenter::plistNames().GamePlayUI + ".png");
//    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(DataCenter::plistNames().BagLayerUI+".plist", DataCenter::plistNames().BagLayerUI + ".png");
    
    //background
    auto backSp = Sprite::createWithSpriteFrameName(DataCenter::pngNames().BagBack);
    backSp->setPosition(VisibleSize.width/2, VisibleSize.height/2);
    this->addChild(backSp);
    
    //Text
    auto bagText = Sprite::createWithSpriteFrameName(DataCenter::pngNames().BagText);
    bagText->setPosition(VisibleSize.width/2, VisibleSize.height-500);
    this->addChild(bagText);
    
    //close button
    auto menuCloseItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().Close), Sprite::createWithSpriteFrameName(DataCenter::pngNames().CloseS), CC_CALLBACK_0(UserBagLayer::onCloseMenuClick, this));

    menuCloseItem->setPosition(VisibleSize.width-85, VisibleSize.height-430);
    
    auto menu = Menu::create(menuCloseItem, NULL);
    menu->setPosition(Point::ZERO);
    this->addChild(menu);
    

    bagScrollView = extension::ScrollView::create(Size(backSp->getContentSize().width,backSp->getContentSize().height-300));
    bagScrollView->setContentSize(Size(backSp->getContentSize().width, backSp->getContentSize().height));
    bagScrollView->setPosition(Origin.x+50, Origin.y+500);

    bagScrollView->setDirection(extension::ScrollView::Direction::VERTICAL);
    this->addChild(bagScrollView, 3);
    bagScrollView->setDelegate(this);
    
    bagScrollView->addChild(bagRowLayer());
    bagScrollView->setBounceable(true);
    
//    bagScrollView->setContentSize(Size(800, 600));
    
    
//    auto menuBack = Sprite::createWithSpriteFrameName(DataCenter::pngNames().PropBack);
//    menuBack->setPosition(200,200);
//    this->addChild(menuBack);
}



Layer* UserBagLayer::bagRowLayer(){
    Layer* bagScrollLayer=Layer::create();
//    _userData=UserData::create();
    _userData=DataCenter::getInstance()->getUserData();
    std::vector<std::map<std::string,std::string>> userBagProp=_userData->getUserBagProp();
    
    std::vector<std::map<std::string,std::string>> userBagSpecialBall=_userData->getUserBagSpecialBall();
    
    bagScrollLayer->setContentSize(VisibleSize);
    std::map<std::string,std::string> upbMap;
//    PaoPaoData pD;
    int ccRow;
    Vector<MenuItem *>userPropItem;
    
    int row=0;
    ccRow=(int(userBagProp.size()/4)+1)*4;
    for (int i=0; i<ccRow; i++) {
        auto menuBack = Sprite::createWithSpriteFrameName(DataCenter::pngNames().BagPropBack);
        
        if (i>0 && i%4==0) {
            row++;
        }
        
        log("i----%d,i4-----%d",i,i%4);
        menuBack->setPosition(200+i%4*200,700-row*200);
        bagScrollLayer->addChild(menuBack);
        
        if (i<userBagProp.size()) {
            
            upbMap=userBagProp[i];
            
            float x=200+i%4*200;
            float y=700-row*200;
//            pD= DataCenter::getInstance()->getPaoPaoDataWithNum(turnStringToLong(upbMap["Data"]));
//            auto userProp=MenuItemSprite::create(Sprite::createWithSpriteFrameName(pD.TargetPaoFrameName), Sprite::createWithSpriteFrameName(pD.TargetPaoFrameName), CC_CALLBACK_0(UserBagLayer::onBagClick, this,x,y,upbMap["Data"]));
            
            auto userProp = MenuItemSprite::create(PaoPao::createWithNumber(turnStringToLong(upbMap["Data"])), PaoPao::createWithNumber(turnStringToLong(upbMap["Data"])), CC_CALLBACK_0(UserBagLayer::onBagClick, this,x,y,upbMap["Data"]));
            
            
            userProp->setPosition(x,y);
            userPropItem.pushBack(userProp);
            
        }
        
    }
    
    row++;
    ccRow=(int(userBagSpecialBall.size()/4)+1)*4;
    for (int i=0; i<ccRow; i++) {
        auto menuBack = Sprite::createWithSpriteFrameName(DataCenter::pngNames().BagPropBack);
        
        if (i>0 && i%4==0) {
            row++;
        }
        
        log("i----%d,i4-----%d",i,i%4);
        menuBack->setPosition(200+i%4*200,700-row*200);
        bagScrollLayer->addChild(menuBack);
        
        if (i<userBagSpecialBall.size()) {
            upbMap=userBagSpecialBall[i];
            
            
            float x=200+i%4*200;
            float y=700-row*200;
//            pD= DataCenter::getInstance()->getPaoPaoDataWithNum(turnStringToLong(upbMap["Data"]));
//            auto userProp=MenuItemSprite::create(Sprite::createWithSpriteFrameName(pD.TargetPaoFrameName), Sprite::createWithSpriteFrameName(pD.TargetPaoFrameName), CC_CALLBACK_0(UserBagLayer::onBagClick, this,x,y,upbMap["Data"]));
            
            auto userProp = MenuItemSprite::create(PaoPao::createWithNumber(turnStringToLong(upbMap["Data"])), PaoPao::createWithNumber(turnStringToLong(upbMap["Data"])), CC_CALLBACK_0(UserBagLayer::onBagClick, this,x,y,upbMap["Data"]));
            
            userProp->setPosition(x,y);
            userPropItem.pushBack(userProp);
        }
        
    }
    
    auto menuProp=Menu::createWithArray(userPropItem);
    menuProp->setPosition(0,0);
    bagScrollLayer->addChild(menuProp);
//    auto menuBack = Sprite::createWithSpriteFrameName(DataCenter::pngNames().PropBack);
//    menuBack->setPosition(200,200);
//    bagScrollLayer->addChild(menuBack);
    return bagScrollLayer;
}

void UserBagLayer::onCloseMenuClick(){
    if (_delegate != NULL)
    {
        _delegate->onBagCloseClick();
    }
    
    this->removeFromParent();
}

void UserBagLayer::onBagClick(float x,float y,std::string pName){
    PropInfo *prop=DataCenter::getInstance()->getPropInfo(pName);
    
    if (_propInfoLayer!=NULL) {
        log("prop-----%s",prop->getInfo().c_str());
        this->removeChild(_propInfoLayer);
    }
    
    _propInfoLayer=Layer::create();
    auto infoBack = Sprite::createWithSpriteFrameName(DataCenter::pngNames().BagPropBack);
    infoBack->setPosition(0,0);
    auto infoLabel=Label::createWithSystemFont(prop->getInfo(), "Arial", 24);
    infoBack->setPosition(10,10);
    _propInfoLayer->addChild(infoBack);
    _propInfoLayer->addChild(infoLabel);
    _propInfoLayer->setPosition(x+200,y+400);
    this->addChild(_propInfoLayer);
    
}

void UserBagLayer::scrollViewDidScroll(extension::ScrollView* view){
}
void UserBagLayer::scrollViewDidZoom(extension::ScrollView* view){
}

void UserBagLayerDelegate::onBagCloseClick()
{
    
}

