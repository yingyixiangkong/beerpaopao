//
//  OwlLayer.cpp
//  beerpaopao
//
//  Created by liupeng on 14/11/24.
//
//

#include "OwlLayer.h"


OwlLayer* OwlLayer::newOwlLayer(OwlLayerDelegate* delegate)
{
    OwlLayer *owlLayer = OwlLayer::create();
    
    owlLayer->setDelegate(delegate);
    
    owlLayer->addBaseUI();
    
    return owlLayer;
}

bool OwlLayer::init()
{
    if (!Layer::init())
    {
        return false;
    }
    
    return true;
}

void OwlLayer::setDelegate(OwlLayerDelegate *delegate)
{
    _delegate = delegate;
}

void OwlLayer::addBaseUI(){
    _userData=DataCenter::getInstance()->getUserData();
    _owlData=DataCenter::getInstance()->getOwlDataWithOwlLevel(_userData->getOwlLevel());
//    log("userData--owlLevel--%d",_userData->getOwlLevel());
    
    _owlListener = EventListenerTouchOneByOne::create();
    _owlListener->onTouchBegan=CC_CALLBACK_2(OwlLayer::onOwlClick, this);
    _owlListener->setSwallowTouches(true);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_owlListener, this);
    
    auto backLayerColor = LayerColor::create(Color4B(0, 0, 0, 100), VisibleSize.width, VisibleSize.height);
    
    this->addChild(backLayerColor);
    
    
    auto vLine1=Sprite::createWithSpriteFrameName(DataCenter::pngNames().VerticalLineL);
    auto vLine2=Sprite::createWithSpriteFrameName(DataCenter::pngNames().VerticalLineL);
    vLine1->setPosition(VisibleSize.width/2-300, VisibleSize.height/2+200);
    vLine2->setPosition(VisibleSize.width/2-100, VisibleSize.height/2+200);
    this->addChild(vLine1);
    this->addChild(vLine2);
    
    auto owlBranch=Sprite::createWithSpriteFrameName(DataCenter::pngNames().OwlBranch);
    owlBranch->setPosition(VisibleSize.width/2-200, VisibleSize.height/2+250);
    this->addChild(owlBranch);
    
    owlSleep=Sprite::createWithSpriteFrameName(DataCenter::pngNames().OwlSleep);
    owlSleep->setPosition(VisibleSize.width/2-200, VisibleSize.height/2+330);
    this->addChild(owlSleep);
    isSleep=true;
    sleepPao=Sprite::createWithSpriteFrameName(DataCenter::pngNames().SleepPao);
    sleepPao->setPosition(VisibleSize.width/2-200, VisibleSize.height/2+370);
    ScaleTo* scale=ScaleTo::create(0.8f,1.2f);
    ScaleTo* scale1=ScaleTo::create(0.8f,0.8f);
    Sequence* se=Sequence::create(scale,scale1, NULL);
    RepeatForever* sleepP=RepeatForever::create(se);
    sleepPao->setAnchorPoint(Vec2(1, 1));
    sleepPao->runAction(sleepP);
    this->addChild(sleepPao);
    
    auto owlPropInfoUI=Sprite::createWithSpriteFrameName(DataCenter::pngNames().OwlPropInfoUI);
    owlPropInfoUI->setPosition(VisibleSize.width/2-200, VisibleSize.height/2-300);
    this->addChild(owlPropInfoUI);
    
    auto vLinea=Sprite::createWithSpriteFrameName(DataCenter::pngNames().VerticalLineS);
    auto vlineb=Sprite::createWithSpriteFrameName(DataCenter::pngNames().VerticalLineS);
    vLinea->setPosition(VisibleSize.width/2+500, VisibleSize.height/2+220);
    vlineb->setPosition(VisibleSize.width/2+300, VisibleSize.height/2+220);
    this->addChild(vLinea);
    this->addChild(vlineb);
    
    auto bagBranch=Sprite::createWithSpriteFrameName(DataCenter::pngNames().BagBranch);
    bagBranch->setPosition(VisibleSize.width/2+400, VisibleSize.height/2+250);
    this->addChild(bagBranch);
    
    auto owlBagUI=Sprite::createWithSpriteFrameName(DataCenter::pngNames().OwlBagUI);
    owlBagUI->setPosition(VisibleSize.width/2+400, VisibleSize.height/2-300);
    this->addChild(owlBagUI);
    
    auto closeButton=MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().Close), Sprite::createWithSpriteFrameName(DataCenter::pngNames().CloseS),CC_CALLBACK_0(OwlLayer::onCloseClick, this));
    closeButton->setPosition(VisibleSize.width-80, VisibleSize.height-80);
    auto menu=Menu::create();
    menu->addChild(closeButton);
    menu->setPosition(Point::ZERO);
    this->addChild(menu);
    
    _rightPaoMenu=Menu::create();
    
    
    std::vector<std::map<std::string,std::string>> userPropV=_userData->getUserBagProp();
    
    size_t ulen=userPropV.size();
    std::vector<std::map<std::string,std::string>> owlObjectV=_owlData->getOwlPropertyMap();
    
    size_t len=owlObjectV.size();
    for (int i=0; i<len; i++) {
        std::map<std::string,std::string> owlMap=owlObjectV[i];
//        PaoPaoData pD= DataCenter::getInstance()->getPaoPaoDataWithNum(turnStringToInt(owlMap["Data"]));

        PaoPao *sp1=PaoPao::createWithNumber(turnStringToLong(owlMap["Data"]));
        
        MenuItemSprite* paoMenuItem=MenuItemSprite::create(sp1, sp1, CC_CALLBACK_1(OwlLayer::onLeftPaoClick, this));
        paoMenuItem->setPosition(VisibleSize.width-180, VisibleSize.height/2-100*i);
        
        
//        log("owlData----m_maxDestructive---%d",turnStringToInt(owlMap["LevelMax"]));
        if (turnStringToInt(owlMap["LevelMax"])<=0) {
            auto backLayerPao = LayerColor::create(Color4B(0, 0, 0, 100), sp1->getContentSize().width, sp1->getContentSize().height);

            backLayerPao->setPosition(paoMenuItem->getPosition().x-sp1->getContentSize().width/2,paoMenuItem->getPosition().y-sp1->getContentSize().height/2);
            
            this->addChild(backLayerPao);
            paoMenuItem->setTag(100);
        }else{
            paoMenuItem->setTag(turnStringToInt(owlMap["Data"]));
        }
        
        _rightPaoMenuItem.push_back(paoMenuItem);
        
        int useNum=0;
        for (int j=0; j<ulen; j++) {
            std::map<std::string,std::string> userProp=userPropV[j];
//            log("owlMap----%s,,,,,,userProp----%s",owlMap["Data"].c_str(),userProp["Data"].c_str());
            if (turnStringToInt(owlMap["Data"])==turnStringToInt(userProp["Data"])) {
                useNum=turnStringToInt(userProp["Count"]);
            }
            
        }
        
        Label *lb1=Label::createWithSystemFont(turnIntToString(useNum), "Arial", 40);
        lb1->setPosition(paoMenuItem->getPosition().x+100,paoMenuItem->getPosition().y);
        lb1->setColor(Color3B(0, 0, 0));
        
        _rightPaoLabel.push_back(lb1);
        
        addChild(lb1);
        _rightPaoMenu->addChild(paoMenuItem);
    }
    
    _rightPaoMenu->setPosition(Point::ZERO);
    this->addChild(_rightPaoMenu);
    
}

bool OwlLayer::onOwlClick(Touch *touch, Event *unused_event)
{
    
    
    float tx=touch->getLocation().x;
    float ty=touch->getLocation().y;
    
    float ox=owlSleep->getPosition().x;
    float oy=owlSleep->getPosition().y;
    float ow=owlSleep->getContentSize().width;
    float oh=owlSleep->getContentSize().height;
    
    if (tx>ox-ow/2 && tx<ox+ow/2 && ty>oy-oh/2 && ty<oy+oh/2) {
        SleepOwl();
    }
    
    log("owlSleep--%f,%f,%f,%f",owlSleep->getContentSize().width,owlSleep->getContentSize().height,owlSleep->getPosition().x,owlSleep->getPosition().y);
    return true;
}

void OwlLayer::SleepOwl(){
    if (isSleep) {
        owlSleep->setSpriteFrame(DataCenter::pngNames().OwlWakeUp);
        isSleep=false;
        sleepPao->setVisible(false);
//        _owlLayer->setVisible(true);
        addOwlDialog();
    }else{
        owlSleep->setSpriteFrame(DataCenter::pngNames().OwlSleep);
        isSleep=true;
        sleepPao->setVisible(true);
//        _owlLayer->setVisible(false);
        _owlLayer->removeFromParent();
    }
}

void OwlLayer::addOwlDialog()
{
    _userData=DataCenter::getInstance()->getUserData();
    _owlData=DataCenter::getInstance()->getOwlDataWithOwlLevel(_userData->getOwlLevel());
    
    _owlLayer=Layer::create();
    auto owlDia=Sprite::createWithSpriteFrameName(DataCenter::pngNames().OwlDialog);
    owlDia->setPosition(0, 0);
    _owlLayer->addChild(owlDia);
    
    //    std::vector<std::map<std::string,std::string>> owlObjectV=_owlData->getOwlPropertyMap();
    std::vector<std::map<std::string,std::string>> userSpV=_userData->getUserBagSpecialBall();
    size_t uslen=userSpV.size();
    std::vector<std::map<std::string,std::string>> owlNeedV=_owlData->getUnlockNeedMap();
    size_t nlen=owlNeedV.size();
    for (int i=0; i<nlen; i++) {
        std::map<std::string,std::string> owlNeed=owlNeedV[i];
        
        //        PaoPao *sp1=PaoPao::createWithNumber(turnStringToLong(owlNeed["Data"]));
        Sprite *sp1=Sprite::createWithSpriteFrameName(owlNeed["Data"] + ".png");
        
        if (i%2==0) {
            sp1->setPosition(-400,100-(i/2)*80);
        }else{
            sp1->setPosition(-20,100-(i/2)*80);
        }
        _owlLayer->addChild(sp1);
        
        int uobNum=0;
        for (int j=0; j<uslen; j++) {
            std::map<std::string,std::string> userObject=userSpV[j];
            
            if (turnStringToInt(owlNeed["Data"])==turnStringToInt(userObject["Data"])) {
                uobNum=turnStringToInt(userObject["Count"]);
            }
        }
        Label *lb1=Label::createWithSystemFont(turnIntToString(uobNum) + "/" + owlNeed["Count"], "Arial", 60);
        lb1->setPosition(sp1->getPosition().x+100,sp1->getPosition().y);
        lb1->setColor(Color3B(0, 0, 0));
        
        _owlLayer->addChild(lb1);
        
        
        
    }
    
    auto owlUpButton=MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().GoOnButton), Sprite::createWithSpriteFrameName(DataCenter::pngNames().GoOnButtonS), CC_CALLBACK_0(OwlLayer::owlUpgrade, this));
    owlUpButton->setPosition(10,-250);
    auto owlMenu=Menu::create();
    owlMenu->addChild(owlUpButton);
    owlMenu->setPosition(Point::ZERO);
    
    _owlLayer->addChild(owlMenu);
    
    _owlLayer->setContentSize(owlDia->getContentSize());
    _owlLayer->setPosition(VisibleSize.width/2, VisibleSize.height-350);
//    _owlLayer->setVisible(false);
    this->addChild(_owlLayer);
}

void OwlLayer::onLeftPaoClick(Ref *pSender)
{
    
    int paoTag=((MenuItemSprite*)pSender)->getTag();
    
    if (paoTag>100) {
        
        _owlObjetData=DataCenter::getInstance()->getOwlObjectData(turnIntToString(paoTag));
        
        _userData=DataCenter::getInstance()->getUserData();
        
        std::vector<std::map<std::string,std::string>> userSpV=_userData->getUserBagSpecialBall();
        
        size_t ulen=userSpV.size();
        std::vector<std::map<std::string,std::string>> owlObjectV=_owlObjetData->getMakeNeed();
        
        size_t len=owlObjectV.size();
        
        
        if (_leftPao.size()>0 && _rightPaoLabel.size()>0) {
            for (int i=0; i<len; i++) {
                std::map<std::string,std::string> owlObject=owlObjectV[i];
                
                int uobNum=0;
                for (int j=0; j<ulen; j++) {
                    std::map<std::string,std::string> userObject=userSpV[j];
                    
                    if (turnStringToInt(owlObject["Data"])==turnStringToInt(userObject["Data"])) {
                        uobNum=turnStringToInt(userObject["Count"]);
                    }
                }
                
                if (i<_leftPao.size()) {
//                    _rightPao[i]->setSpriteFrame(owlObject["Data"] + ".png");
                    
                    _leftPaoLabel[i]->setString(turnIntToString(uobNum) + "/" + owlObject["Count"]);
                }else{
                    PaoPao *sp1=PaoPao::createWithNumber(turnStringToLong(owlObject["Data"]));
                    
                    
                    if (i%2==0) {
                        sp1->setPosition(VisibleSize.width/2-400,VisibleSize.height/2-120*(i/2)-50);
                    }else{
                        sp1->setPosition(VisibleSize.width/2-180,VisibleSize.height/2-120*(i/2)-50);
                    }
                    
                    _leftPao.push_back(sp1);
                    
                    Label *lb1=Label::createWithSystemFont(turnIntToString(uobNum) + "/" + owlObject["Count"], "Arial", 60);
                    lb1->setPosition(sp1->getPosition().x+100,sp1->getPosition().y);
                    lb1->setColor(Color3B(0, 0, 0));
                    
                    _leftPaoLabel.push_back(lb1);
                }
                
                if (i==len-1 && len<_leftPao.size()) {
//                    PaoPao* ap = new PaoPao();
                    
//                    std::vector<PaoPao*>::iterator it = std::find(_rightPao.begin(), _rightPao.end(), ap);
                    
                    std::vector<PaoPao*>::iterator pp = _leftPao.begin()+i;
                    
                    _leftPao.erase(pp);
                    
                    std::vector<Label*>::iterator bb = _rightPaoLabel.begin()+i;
                    
                    _leftPaoLabel.erase(bb);
                    
                }
            }
            
            _makeButton->setTag(paoTag);
            
            _makeNum=1;
            
            _numLabel->setString(turnIntToString(_makeNum));
            
        }else{
            for (int i=0; i<len; i++) {
                
                std::map<std::string,std::string> owlObject=owlObjectV[i];
                
                PaoPao *sp1=PaoPao::createWithNumber(turnStringToLong(owlObject["Data"]));
                
                
                if (i%2==0) {
                    sp1->setPosition(VisibleSize.width/2-400,VisibleSize.height/2-120*(i/2)-50);
                }else{
                    sp1->setPosition(VisibleSize.width/2-180,VisibleSize.height/2-120*(i/2)-50);
                }
                
                int uobNum=0;
                for (int j=0; j<ulen; j++) {
                    std::map<std::string,std::string> userObject=userSpV[j];
                    
                    if (turnStringToInt(owlObject["Data"])==turnStringToInt(userObject["Data"])) {
                        uobNum=turnStringToInt(userObject["Count"]);
                    }
                }
                
                Label *lb1=Label::createWithSystemFont(turnIntToString(uobNum) + "/" + owlObject["Count"], "Arial", 60);
                lb1->setPosition(sp1->getPosition().x+100,sp1->getPosition().y);
                lb1->setColor(Color3B(0, 0, 0));
                
                
                _leftPao.push_back(sp1);
                _leftPaoLabel.push_back(lb1);
                
                this->addChild(sp1);
                this->addChild(lb1);
                
            }
            
            _makeButton=MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().GoOnButton), Sprite::createWithSpriteFrameName(DataCenter::pngNames().GoOnButtonS), CC_CALLBACK_1(OwlLayer::onMakeClick, this));
            _addNumButton=MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().AddButton), Sprite::createWithSpriteFrameName(DataCenter::pngNames().AddbuttonS), CC_CALLBACK_0(OwlLayer::addNum, this));
            
            _makeButton->setPosition(VisibleSize.width/2-400,VisibleSize.height/2-600);
            _addNumButton->setPosition(VisibleSize.width/2-50,VisibleSize.height/2-600);
            
            _makeButton->setTag(paoTag);
            
            _makeMenu=Menu::create(_makeButton,_addNumButton, NULL);
            _makeMenu->setPosition(Point::ZERO);
            
            this->addChild(_makeMenu);
            
            _makeNum=1;
            _numLabel=Label::createWithSystemFont(turnIntToString(_makeNum), "Arial", 60);
            _numLabel->setPosition(VisibleSize.width/2-100,VisibleSize.height/2-600);
            _numLabel->setColor(Color3B(0, 0, 0));
            
            this->addChild(_numLabel);
        }
        
        
        
    }
}

void OwlLayer::onMakeClick(Ref *pSender){
    int paoTag=((MenuItemSprite*)pSender)->getTag();
    
    _owlObjetData=DataCenter::getInstance()->getOwlObjectData(turnIntToString(paoTag));
    
    _userData=DataCenter::getInstance()->getUserData();
    
    std::vector<std::map<std::string,std::string>> userSpV=_userData->getUserBagSpecialBall();
    
    size_t ulen=userSpV.size();
    std::vector<std::map<std::string,std::string>> owlObjectV=_owlObjetData->getMakeNeed();
    
    size_t len=_owlObjetData->getMakeNeed().size();
    
    bool isSave=true;
    for (int i=0; i<len; i++) {
        std::map<std::string,std::string> owlObject=owlObjectV[i];
        
        int uobNum=0;
        for (int j=0; j<ulen; j++) {
            std::map<std::string,std::string> userObject=userSpV[j];
            
            if (turnStringToInt(owlObject["Data"])==turnStringToInt(userObject["Data"])) {
                uobNum=turnStringToInt(owlObject["Count"])*_makeNum;
                
                if (turnStringToInt(userObject["Count"])>=uobNum) {
                    uobNum=turnStringToInt(userObject["Count"])-uobNum;
                    userObject["Count"]=turnIntToString(uobNum);
                }else{
                    isSave=false;
                    break;
                }
                
            }
            
//            log("userObject---Data:%s,-----Count:%s",userObject["Data"].c_str(),userObject["Count"].c_str());
            userSpV[j]=userObject;
        }
        if (!isSave) {
            break;
        }
    }
    
    if (isSave) {
        _userData->setUserBagSpecialBall(userSpV);
        DataCenter::getInstance()->saveUserData(_userData);
    }
    
}

void OwlLayer::onPropClick(){
    
}
void OwlLayer::owlUpgrade(){
    log("owlUpgrade-----");
    
    
    _userData=DataCenter::getInstance()->getUserData();
    _owlData=DataCenter::getInstance()->getOwlDataWithOwlLevel(_userData->getOwlLevel());
    
    std::vector<std::map<std::string,std::string>> userSpV=_userData->getUserBagSpecialBall();
    
    size_t ulen=userSpV.size();
    std::vector<std::map<std::string,std::string>> owlObjectV=_owlData->getUnlockNeedMap();
    
    size_t len=owlObjectV.size();
    bool isSave=true;
    for (int i=0; i<len; i++) {
        std::map<std::string,std::string> owlObject=owlObjectV[i];
        
        int uobNum=0;
        for (int j=0; j<ulen; j++) {
            std::map<std::string,std::string> userObject=userSpV[j];
            
            if (turnStringToInt(owlObject["Data"])==turnStringToInt(userObject["Data"])) {
                uobNum=turnStringToInt(owlObject["Count"]);
                
                if (turnStringToInt(userObject["Count"])>=uobNum) {
                    uobNum=turnStringToInt(userObject["Count"])-uobNum;
                    userObject["Count"]=turnIntToString(uobNum);
                }else{
                    isSave=false;
                    break;
                }
                
            }
            
            userSpV[j]=userObject;
        }
        if (!isSave) {
            break;
        }
    }
    if (isSave) {
        _userData->setUserBagSpecialBall(userSpV);
        int owlLevel=_userData->getOwlLevel();
        _userData->setOwlLevel(owlLevel+1);
        DataCenter::getInstance()->saveUserData(_userData);
    }
    
    
    
    _owlLayer->removeFromParent();
    addOwlDialog();
}
void OwlLayer::makeProp(int num,std::string proString){
    
}
void OwlLayer::addNum(){
    _makeNum++;
    _numLabel->setString(turnIntToString(_makeNum));
}

void OwlLayer::onCloseClick(){
    if (_delegate!=NULL) {
        _delegate->onOwlCloseClick();
    }
    this->removeFromParent();
}

void OwlLayerDelegate::onOwlCloseClick(){
    
}

