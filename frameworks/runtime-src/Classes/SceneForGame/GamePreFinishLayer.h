//
//  GamePreFinishLayer.h
//  beerpaopao
//
//  Created by Frank on 14/11/10.
//
//

#ifndef __beerpaopao__GamePreFinishLayer__
#define __beerpaopao__GamePreFinishLayer__

#include <stdio.h>
#include <iostream>
#include "../DealForGame/DataCenter.h"
#include "../SpecialSprite/PaoPao.h"

class GamePreFinishLayer;
class GamePreFinishLayerDelegate
{
public:
    
    virtual void closedLayer(bool didNothing);
    virtual void clickTest(long clickTag);
};


class GamePreFinishLayer : public Layer
{
public:
    static GamePreFinishLayer* newGamePreFinishLayer(GamePreFinishLayerDelegate *delegate);
    
    virtual bool init();
    
    void startCountdown();
    
    CREATE_FUNC(GamePreFinishLayer);
    
private:
    
    GamePreFinishLayerDelegate *_delegate;
    
    bool _didNothing;
    
    int _countdown;
    
    Label *_countdownLabel;
    
    
    void setDelegate(GamePreFinishLayerDelegate *delegate);
    
    void addBaseUI();
    
    void autoCloseCountdown(float dt);
    
    
    void onClick(Ref *pSender);
    
    
    void close(bool didNothing);
    
    bool onTouchBegan(Touch *touch, Event *unused_event);
    
};







#endif /* defined(__beerpaopao__GamePreFinishLayer__) */
