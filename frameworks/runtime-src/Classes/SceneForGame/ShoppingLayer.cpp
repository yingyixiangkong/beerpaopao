//
//  ShoppingLayer.cpp
//  beerpaopao
//
//  Created by liupeng on 14/11/19.
//
//

#include "ShoppingLayer.h"

ShoppingLayer* ShoppingLayer::newShoppingLayer(ShoppingLayerDelegate* delegate)
{
    ShoppingLayer *shoppingLayer = ShoppingLayer::create();
    
    shoppingLayer->setDelegate(delegate);
    
    shoppingLayer->addBaseUI();
    shoppingLayer->addShopLayer();
    shoppingLayer->showPackLayer();
    
    //    log("newGmaePrePlayLayer-----%d",levelNum);
    
    return shoppingLayer;
}

bool ShoppingLayer::init()
{
    if (!Layer::init())
    {
        return false;
    }
    
    return true;
}

void ShoppingLayer::setDelegate(ShoppingLayerDelegate *delegate)
{
    _delegate = delegate;
}


void ShoppingLayer::addBaseUI(){
    
    _userData=DataCenter::getInstance()->getUserData();
    
    
    auto backLayerColor = LayerColor::create(Color4B(0, 0, 0, 100), VisibleSize.width, VisibleSize.height);
    
    this->addChild(backLayerColor);
    
    //background
    auto backSp = Sprite::createWithSpriteFrameName(DataCenter::pngNames().ShoppingBack);
    backSp->setPosition(VisibleSize.width/2, VisibleSize.height/2);
    this->addChild(backSp);
    
    //background
    auto backS = Sprite::createWithSpriteFrameName(DataCenter::pngNames().ShoppingGroud);
    backS->setPosition(VisibleSize.width/2, VisibleSize.height/2-60);
    this->addChild(backS);
    
    //close button
    auto menuCloseItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().Close), Sprite::createWithSpriteFrameName(DataCenter::pngNames().CloseS), CC_CALLBACK_0(ShoppingLayer::onCloseMenuClick, this));
    
    menuCloseItem->setPosition(VisibleSize.width-80, VisibleSize.height-80);
    
    //label
    auto menuPackItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().PackLabelS), Sprite::createWithSpriteFrameName(DataCenter::pngNames().PackLabel), CC_CALLBACK_0(ShoppingLayer::onPackLabelClick, this));
    menuPackItem->setPosition(VisibleSize.width-900, VisibleSize.height-200);
    
    auto menuPropItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().PropLabel), Sprite::createWithSpriteFrameName(DataCenter::pngNames().PropLabelS), CC_CALLBACK_0(ShoppingLayer::onPropLabelClick, this));
    menuPropItem->setPosition(VisibleSize.width/2, VisibleSize.height-200);
    
    auto menuCoinItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().CoinLabel), Sprite::createWithSpriteFrameName(DataCenter::pngNames().CoinLabelS), CC_CALLBACK_0(ShoppingLayer::onCoinLabelClick, this));
    menuCoinItem->setPosition(VisibleSize.width-180, VisibleSize.height-200);
    
    auto menu = Menu::create(menuCloseItem,menuPackItem,menuPropItem,menuCoinItem, NULL);
    menu->setPosition(Point::ZERO);
    this->addChild(menu);
    menu->setGlobalZOrder(100);
    
    
    shopScrollView = extension::ScrollView::create(Size(backS->getContentSize().width,backS->getContentSize().height));
    shopScrollView->setContentSize(Size(backS->getContentSize().width, backS->getContentSize().height));
    shopScrollView->setPosition(Origin.x+50, Origin.y+500);
    
    shopScrollView->setDirection(extension::ScrollView::Direction::VERTICAL);
    this->addChild(shopScrollView, 3);
    shopScrollView->setDelegate(this);
    
//    shopScrollView->addChild(bagRowLayer());
    shopScrollView->setBounceable(true);
    
}

void ShoppingLayer::onPackLabelClick(){
    
}
void ShoppingLayer::onPropLabelClick(){
    
}
void ShoppingLayer::onCoinLabelClick(){
    
}

void ShoppingLayer::onShopClick(std::string pName){
    
}

void ShoppingLayer::onPackClick(std::string packName,std::string packPrice){
    
}

void ShoppingLayer::onPropClick(std::string propName,std::string propPrice){
    
}

void ShoppingLayer::onCoinClick(std::string coinName,std::string coinPrice){
    
}

void ShoppingLayer::showPackLayer(){
    
}

void ShoppingLayer::showPropLayer(){
    
}

void ShoppingLayer::showCoinLayer(){
    
}


void ShoppingLayer::addShopLayer(){
    _shopping=DataCenter::getInstance()->getShopping();
    
    _packLayer=Layer::create();
    _propLayer=Layer::create();
    _coinLayer=Layer::create();
    
    std::vector<std::map<std::string,std::string>> _propPack;
    std::vector<std::map<std::string,std::string>> _propShop;
    std::vector<std::map<std::string,std::string>> _coinShop;
    
    _propPack=_shopping->getPropPack();
    _propShop=_shopping->getPropShop();
    _coinShop=_shopping->getCoinShop();
    
    auto packMenu=Menu::create();
    for (int i=0; i<_propPack.size(); i++) {
        
        std::map<std::string,std::string> spbMap;
        spbMap=_propPack[i];
        log("_propPack---i--%s",_propPack[i]["pngName"].c_str());
        auto packMenuItem=MenuItemSprite::create(Sprite::createWithSpriteFrameName(spbMap["pngName"]), Sprite::createWithSpriteFrameName(spbMap["pngName"]), CC_CALLBACK_0(ShoppingLayer::onPackClick, this,spbMap["nameString"],spbMap["paoCoin"]));
        packMenuItem->setPosition(0, 200*i);
        packMenu->addChild(packMenuItem);
        
    }
    packMenu->setPosition(VisibleSize.width/2, VisibleSize.height/2);
    _packLayer->addChild(packMenu);
    this->addChild(_packLayer);
    
    auto propMenu=Menu::create();
    for (int i=0; i<_propShop.size(); i++) {
        std::map<std::string,std::string> spbMap;
        spbMap=_propShop[i];
        log("_propShop---i--%s",_propShop[i]["pngName"].c_str());
        auto propMenuItem=MenuItemSprite::create(Sprite::createWithSpriteFrameName(spbMap["pngName"]), Sprite::createWithSpriteFrameName(spbMap["pngName"]), CC_CALLBACK_0(ShoppingLayer::onPropClick, this,spbMap["nameString"],spbMap["paoCoin"]));
        propMenuItem->setPosition(0, 200*i);
        propMenu->addChild(propMenuItem);
    }
    propMenu->setPosition(VisibleSize.width/2, VisibleSize.height/2);
    _propLayer->addChild(propMenu);
    this->addChild(_propLayer);
    
    auto coinMenu=Menu::create();
    for (int i=0; i<_coinShop.size(); i++) {
        std::map<std::string,std::string> spbMap;
        spbMap=_coinShop[i];
        log("_coinShop---i--%s",_coinShop[i]["pngName"].c_str());
        auto coinMenuItem=MenuItemSprite::create(Sprite::createWithSpriteFrameName(spbMap["pngName"]), Sprite::createWithSpriteFrameName(spbMap["pngName"]), CC_CALLBACK_0(ShoppingLayer::onCoinClick, this,spbMap["nameString"],spbMap["paoCoin"]));
        coinMenuItem->setPosition(0, 200*i);
        coinMenu->addChild(coinMenuItem);
    }
    coinMenu->setPosition(VisibleSize.width/2, VisibleSize.height/2);
    _coinLayer->addChild(coinMenu);
    this->addChild(_coinLayer);
    
}

void ShoppingLayer::onCloseMenuClick(){
    if (_delegate != NULL)
    {
        _delegate->onShopCloseClick();
    }
    
    this->removeFromParent();
}

void ShoppingLayer::scrollViewDidScroll(extension::ScrollView* view){
}
void ShoppingLayer::scrollViewDidZoom(extension::ScrollView* view){
}

void ShoppingLayerDelegate::onShopCloseClick()
{
    
}