//
//  GameFinishLayer.cpp
//  beerpaopao
//
//  Created by Frank on 14/11/10.
//
//

#include "GameFinishLayer.h"


GameFinishLayer* GameFinishLayer::newGameFinishLayer(GameFinishLayerDelegate *delegate)
{
    GameFinishLayer *newGameFL = GameFinishLayer::create();
    if (delegate != NULL)
    {
       newGameFL->setDelegate(delegate);
    }
    
    newGameFL->addBaseUI();
    
    
    return newGameFL;
}


bool GameFinishLayer::init()
{
    if (!Layer::init())
    {
        return false;
    }
    
    return true;
}

void GameFinishLayer::setDelegate(GameFinishLayerDelegate *delegate)
{
    _delegate = delegate;
}

void GameFinishLayer::setFinishPD(FinishPlayData var)
{
    _finishPD = var;
}

void GameFinishLayer::addBaseUI()
{
    if (_delegate != NULL)
    {
        _finishPD = _delegate->getFinishPlayData();
    }
    else
    {
        _finishPD.isWin = false;
        _finishPD.Score = 50000;
        _finishPD.Star = 2;
        _finishPD.LevelId=1;
    }
    
    _backLayerColor = LayerColor::create(Color4B(0, 0, 0, 100), VisibleSize.width, VisibleSize.height);
    addChild(_backLayerColor);
    
    _touchListener = EventListenerTouchOneByOne::create();
    _touchListener->onTouchBegan = CC_CALLBACK_2(GameFinishLayer::onTouchBegan, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_touchListener, this);
    
    if (_finishPD.isWin)
    {
        addWinUI();
    }
    else
    {
        addFailedUI();
    }
}

void GameFinishLayer::addWinUI()
{
    
    _backSprite = Sprite::createWithSpriteFrameName(DataCenter::pngNames().Win_Back);
    _backSprite->setPosition(_backLayerColor->getContentSize().width/2, _backLayerColor->getContentSize().height/2);
    _backLayerColor->addChild(_backSprite);
    
    
    auto levelSp = Sprite::createWithSpriteFrameName(DataCenter::pngNames().Win_Level);
    levelSp->setPosition(_backSprite->getContentSize().width/2, _backSprite->getContentSize().height-levelSp->getContentSize().height);
    _backSprite->addChild(levelSp);
    
    
    auto wordSp = Sprite::createWithSpriteFrameName(DataCenter::pngNames().Win_Word);
    wordSp->setPosition(_backSprite->getContentSize().width-wordSp->getContentSize().width/2-50, levelSp->getPositionY()-levelSp->getContentSize().height/2-wordSp->getContentSize().height/2-10);
    _backSprite->addChild(wordSp);
    
    float starStartW = _backSprite->getContentSize().width/2;
    float starStartH = _backSprite->getContentSize().height*2/3;
    
    for (int index = 0; index < 3; index++)
    {
        std::string spfName;
        if (index < _finishPD.Star)
        {
            spfName = DataCenter::pngNames().Win_StarFull;
        }
        else
        {
            spfName = DataCenter::pngNames().Win_StarEmpty;
        }
        
        auto starSp = Sprite::createWithSpriteFrameName(spfName);
        starSp->setPosition(starStartW+(index-1)*1.5*starSp->getContentSize().width, starStartH);
        _backSprite->addChild(starSp);
    }
    
    auto scoreSp = Sprite::createWithSpriteFrameName(DataCenter::pngNames().Win_Score);
    scoreSp->setPosition(_backSprite->getContentSize().width/2-scoreSp->getContentSize().width, _backSprite->getContentSize().height/2+scoreSp->getContentSize().height/2);
    _backSprite->addChild(scoreSp);
    
    auto scoreLabel = Label::createWithSystemFont(turnLongToString(_finishPD.Score), "Arial", 60);
    scoreLabel->setPosition(scoreSp->getPositionX()+scoreSp->getContentSize().width/2+scoreLabel->getContentSize().width/2+30, scoreSp->getPositionY());
    scoreLabel->setTextColor(Color4B(0,0,0,255));
    _backSprite->addChild(scoreLabel);
    
    auto coinSp = Sprite::createWithSpriteFrameName(DataCenter::pngNames().Win_Coin);
    coinSp->setPosition(_backSprite->getContentSize().width/4, _backSprite->getContentSize().height/4);
    _backSprite->addChild(coinSp);
    auto coinLabel = Label::createWithSystemFont("80", "Arial", 50);
    coinLabel->setPosition(coinSp->getContentSize().width-coinLabel->getContentSize().width/2, coinLabel->getContentSize().height/2);
    coinLabel->setTextColor(Color4B(0,0,0,255));
    coinSp->addChild(coinLabel);
    
    auto tiliSp = Sprite::createWithSpriteFrameName(DataCenter::pngNames().Win_Tili);
    tiliSp->setPosition(coinSp->getPositionX()+200, coinSp->getPositionY());
    _backSprite->addChild(tiliSp);
    auto tiliLabel = Label::createWithSystemFont("5", "Arial", 50);
    tiliLabel->setPosition(tiliSp->getContentSize().width-tiliLabel->getContentSize().width/2, tiliLabel->getContentSize().height/2);
    tiliLabel->setTextColor(Color4B(0,0,0,255));
    tiliSp->addChild(tiliLabel);
    
    auto closeMenuItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().Win_CloseNormal), Sprite::createWithSpriteFrameName(DataCenter::pngNames().Win_CloseSelected), CC_CALLBACK_1(GameFinishLayer::closeLayer, this));
    closeMenuItem->setPosition(_backSprite->getContentSize().width-closeMenuItem->getContentSize().width/2, _backSprite->getContentSize().height-closeMenuItem->getContentSize().height/2);
    
    auto nextMenuItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().Win_NextNormal), Sprite::createWithSpriteFrameName(DataCenter::pngNames().Win_NextSelected), CC_CALLBACK_1(GameFinishLayer::onNext, this));
    auto shareMenuItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().Win_ShareNormal), Sprite::createWithSpriteFrameName(DataCenter::pngNames().Win_ShareSelected), CC_CALLBACK_1(GameFinishLayer::onShare, this));
    float fixW = (_backSprite->getContentSize().width-nextMenuItem->getContentSize().width - shareMenuItem->getContentSize().width)/3/2;
    nextMenuItem->setPosition(_backSprite->getContentSize().width/2-fixW-nextMenuItem->getContentSize().width/2, 0);
    shareMenuItem->setPosition(_backSprite->getContentSize().width/2+fixW+shareMenuItem->getContentSize().width/2, 0);
    
    auto menu = Menu::create(closeMenuItem,nextMenuItem,shareMenuItem, NULL);
    menu->setPosition(Point::ZERO);
    
    _backSprite->addChild(menu);
    
}
void GameFinishLayer::addFailedUI()
{
    _backSprite = Sprite::createWithSpriteFrameName(DataCenter::pngNames().Fail_Back);
    _backSprite->setPosition(_backLayerColor->getContentSize().width/2, _backLayerColor->getContentSize().height/2);
    _backLayerColor->addChild(_backSprite);
    
    auto levelSp = Sprite::createWithSpriteFrameName(DataCenter::pngNames().Fail_Level);
    levelSp->setPosition(_backSprite->getContentSize().width/2, _backSprite->getContentSize().height-levelSp->getContentSize().height);
    _backSprite->addChild(levelSp);
    
    auto wordSp = Sprite::createWithSpriteFrameName(DataCenter::pngNames().Fail_Word);
    wordSp->setPosition(_backSprite->getContentSize().width/2, _backSprite->getContentSize().height*3/4);
    _backSprite->addChild(wordSp);
    
    auto funnyPNGSp = Sprite::createWithSpriteFrameName(DataCenter::pngNames().Fail_FunnyPNG);
    funnyPNGSp->setPosition(_backSprite->getContentSize().width/2, wordSp->getPositionY()-wordSp->getContentSize().height/2-funnyPNGSp->getContentSize().height/2-50);
    _backSprite->addChild(funnyPNGSp);
    
    
    auto closeMenuItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().Fail_CloseNormal), Sprite::createWithSpriteFrameName(DataCenter::pngNames().Fail_CloseSelected), CC_CALLBACK_1(GameFinishLayer::closeLayer, this));
    closeMenuItem->setPosition(_backSprite->getContentSize().width-closeMenuItem->getContentSize().width/2, _backSprite->getContentSize().height-closeMenuItem->getContentSize().height/2);
    
    auto tryMenuItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().Fail_TryNormal), Sprite::createWithSpriteFrameName(DataCenter::pngNames().Fail_TrySelected), CC_CALLBACK_1(GameFinishLayer::onRetry, this));
    tryMenuItem->setPosition(_backSprite->getContentSize().width/2, 0);
    
    auto menu = Menu::create(closeMenuItem,tryMenuItem, NULL);
    menu->setPosition(Point::ZERO);
    
    _backSprite->addChild(menu);
}

bool GameFinishLayer::onTouchBegan(Touch *touch, Event *unused_event)
{
    log("GameFinishLayer::onTouchBegan");
    
    return true;
}

void GameFinishLayer::closeLayer(Ref *pSender)
{
    
    log("void GameFinishLayer::closeLayer(Ref *pSender)");
    
    this->removeFromParent();
    
    if (_delegate != NULL)
    {
        _delegate->finishLayerClose();
    }
}

void GameFinishLayer::onNext(cocos2d::Ref *pSender)
{
    log("void GameFinishLayer::onNext(cocos2d::Ref *pSender)");
    
    this->removeFromParent();
    
    if (_delegate != NULL)
    {
        _delegate->finishLayerNext(_finishPD.LevelId);
    }
}
void GameFinishLayer::onShare(cocos2d::Ref *pSender)
{
    log("void GameFinishLayer::onShare(cocos2d::Ref *pSender)");
    
    if (_delegate != NULL)
    {
        _delegate->finishLayerShare(_finishPD.LevelId);
    }
}

void GameFinishLayer::onRetry(Ref *pSender)
{
    log("void GameFinishLayer::onTry(Ref *pSender)");
    
    this->removeFromParent();
    
    if (_delegate != NULL)
    {
        _delegate->finishLayerRetry(_finishPD.LevelId);
    }
}








FinishPlayData GameFinishLayerDelegate::getFinishPlayData()
{
    FinishPlayData fpd;
    
    return fpd;
}

void GameFinishLayerDelegate::finishLayerClose()
{
    
}
void GameFinishLayerDelegate::finishLayerNext(int levelId)
{
    
}
void GameFinishLayerDelegate::finishLayerShare(int levelId)
{
    
}
void GameFinishLayerDelegate::finishLayerRetry(int levelId)
{
    
}



