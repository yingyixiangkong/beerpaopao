//
//  GamePlayScene.cpp
//  beerpaopao
//
//  Created by Frank on 14-9-13.
//
//

#include "GamePlayScene.h"

Armature *beerBoy;


Scene* GamePlayScene::newGamePlayScene(int level)
{
    Scene *gamePlayS = Scene::create();
    GamePlayScene *gamePlayL = GamePlayScene::create();
    
    gamePlayL->setLevel(level);
    
    gamePlayL->addBaseUI();
    
    gamePlayL->setTag(33333);
    
    gamePlayS->addChild(gamePlayL);
    
    return gamePlayS;
}

bool GamePlayScene::init()
{
    if (!Layer::init())
    {
        return false;
    }
    
    return true;
}

void GamePlayScene::setLevel(int level)
{
    _level = level;
}

void GamePlayScene::addBaseUI()
{
    _lData = DataCenter::getInstance()->getLevelData(_level);
    
    _row = _lData->getRow();
    _col = _lData->getCol();
    _ballUpVector = _lData->getBallUpVector();
    _ballDownCount = _lData->getBallDownCount();
    _ballDownVector = _lData->getBallDwonVector();
    _winType = _lData->getWinType();
    _gameSeconds = _lData->getGameSeconds();
    _gameWinScore = _lData->getGameWinScore();
    _specialBallCount = _lData->getSpecialBallCount();
    _specailBallDataV = _lData->getSpecailBallDataV();
    _promptBall = _lData->getPromptBall();
    _oneStarScore = _lData->getOneStarScore();
    _twoStarScore = _lData->getTwoStarScore();
    _threeStarScore = _lData->getThreeStarScore();
    _isFireBallDeath = false;
    _fanActionLock = false;
    _isDeath = false;
    _isSwpiderWeb = false;
    _specialColor = PaoColorNone;
    _sColorM.insert(std::pair<PaoColor,int>(PaoColorBlue,0));
    _sColorM.insert(std::pair<PaoColor,int>(PaoColorGreen,0));
    _sColorM.insert(std::pair<PaoColor,int>(PaoColorPink,0));
    _sColorM.insert(std::pair<PaoColor,int>(PaoColorRed,0));
    _sColorM.insert(std::pair<PaoColor,int>(PaoColorYellow,0));
    randomNewPaoNameV();
    
    
    _transformFixH = 0;
    _fixXDistance = (VisibleSize.width-11*CIRCLED-CIRCLER)/2;
    _fixYDistance = 113;
    _playStatu = PlayStatuPrepare;
    _startFire = false;
    
    auto gameBack = Sprite::create("res/Map/GamePlayScene.png");
    gameBack->setPosition(VisibleSize.width/2, VisibleSize.height/2);
    addChild(gameBack,1);
//    auto back = LayerColor::create(Color4B::BLACK, VisibleSize.width, VisibleSize.height);
//    addChild(back,1);
    
    _qiuLayer = Layer::create();
    
    addChild(_qiuLayer,2);
    
    _lastMaxRow = RowStandard-1;
    
    std::vector<std::vector<long>> ld = _lData->getBallUpVector();
    
    _currentMaxRow = _lData->getRow()-1;
    _baseRoundStartRow = _currentMaxRow-RowStandard+1;
    
    for (int y = 0; y < _lData->getRow(); y++)
    {
        for (int x = 0; x < _lData->getCol(); x++)
        {
            if (ld[y][x] != 0)
            {
                int trNum = transformWithLevelRandomNewPaoIndexV(ld[y][x]);
                auto ball = PaoPao::createWithNumber(trNum);
                ball->setPosition(getPosByRowAndCol(y, x));
                ball->setTag(y*100+x);
                _qiuLayer->addChild(ball,1);
                
                _paoV.push_back(ball);
//                log("ball->getPaoNumber() = %ld,_paoV.size() = %ld",ball->getPaoNumber(),_paoV.size());
            }
        }
    }
    
//    log("_paoV.size() = %ld,_lData->getRow() = %d,_lData->getCol() = %d",_paoV.size(),_lData->getRow(),_lData->getCol());
    
    _topL = LayerColor::create(Color4B(0, 0, 0, 150), VisibleSize.width, _fixYDistance);
    _topL->setPosition(0, VisibleSize.height-113);
    _qiuLayer->addChild(_topL,3);
    
    _bottomL = LayerColor::create(Color4B(0, 0, 0, 0), VisibleSize.width, 960);
    _qiuLayer->addChild(_bottomL,2);
    
    auto touchListener = EventListenerTouchOneByOne::create();
    touchListener->onTouchBegan = CC_CALLBACK_2(GamePlayScene::onMyTouchBegan, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(GamePlayScene::onMyTouchEnded, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
    
    
    _fireBallStartPos.x = _qiuLayer->getContentSize().width/2-90;
    _fireBallStartPos.y = 200+100;
    
    _spBallStartPos.x = _qiuLayer->getContentSize().width/2-200;
    _spBallStartPos.y = 200+100;
    
    
    //动画
    ArmatureDataManager::getInstance()->addArmatureFileInfo(DataCenter::getInstance()->armatureJson().BeerBoyLeftPng, DataCenter::getInstance()->armatureJson().BeerBoyLeftplist, DataCenter::getInstance()->armatureJson().BeerBoyLeftJson);
    beerBoy=Armature::create("beerBoy");
    
    beerBoy->setPosition(Point(VisibleSize.width * 0.5, 200));
    
    beerBoy->setTag(100001);
    
    _bottomL->addChild(beerBoy);
}

#pragma mark -
#pragma mark 调整所有球的整体位置
#pragma mark -

void GamePlayScene::adjustQiuLayer()
{
    int maxRow = 0;
    
    for (int index = 0; index < _paoV.size(); index++)
    {
        int row = (_paoV[index]->getTag())/100;
        
        if (row > maxRow)
        {
            maxRow = row;
        }
    }
    
    _currentMaxRow = maxRow;
    
    if (_currentMaxRow <= 10)
    {
        _baseRoundStartRow = 0;
    }
    else if (_currentMaxRow+1-RowStandard < _baseRoundStartRow)
    {
        _baseRoundStartRow = _currentMaxRow+1-RowStandard;
    }
    
    log("_currentMaxRow = %d,_lastMaxRow = %d,_baseRoundStartRow = %d",_currentMaxRow,_lastMaxRow,_baseRoundStartRow);
    
    if (_currentMaxRow != _lastMaxRow && (_lastMaxRow > RowStandard || _currentMaxRow > RowStandard))
    {
        int moveRow = 0;
        
        if (_currentMaxRow < RowStandard && _lastMaxRow > RowStandard)
        {
            moveRow = RowStandard-_lastMaxRow;
            _lastMaxRow = RowStandard;
        }
        else if (_currentMaxRow > RowStandard && _lastMaxRow < RowStandard)
        {
            moveRow = _currentMaxRow-RowStandard;
            _lastMaxRow = _currentMaxRow;
        }
        else
        {
            moveRow = _currentMaxRow-_lastMaxRow;
            _lastMaxRow = _currentMaxRow;
        }
        
        float timeAct = moveRow>0?moveRow*0.2:-moveRow*0.2;
        
        float moveH = CIRCLED*sin(PI/3)*(moveRow);
        
        _transformFixH = _transformFixH+moveH;
        
        Point p1 = Point(_qiuLayer->getPositionX(),_qiuLayer->getPositionY()+moveH);
        Point p2 = Point(_topL->getPositionX(),_topL->getPositionY()-moveH);
        Point p3 = Point(_bottomL->getPositionX(),_bottomL->getPositionY()-moveH);
        _fireBallStartPos.y -= moveH;
        _spBallStartPos.y -= moveH;
        
        
        _qiuLayer->runAction(MoveTo::create(timeAct, p1));
        _topL->runAction(MoveTo::create(timeAct, p2));
        _bottomL->runAction(MoveTo::create(timeAct, p3));
        
        if (_waitingBall != NULL)
        {
            _waitingBall->runAction(MoveTo::create(timeAct, Point(_waitingBall->getPositionX(),_waitingBall->getPositionY()-moveH)));
        }
        
        scheduleOnce(schedule_selector(GamePlayScene::doSomethingAfterAdjust), timeAct);
    }
    else
    {
        scheduleOnce(schedule_selector(GamePlayScene::doSomethingAfterAdjust), StarnderdDeley);
    }
}

#pragma mark -
#pragma mark 调整整体位置之后做的事
#pragma mark -

void GamePlayScene::doSomethingAfterAdjust(float dt)
{
    _triggerPaoIndexV.clear();
    _isFireBallDeath = false;
    _isDeath = false;
    _isSwpiderWeb = false;
    
    if (_playStatu == PlayStatuPrepare)
    {
        for (int index = _paoV.size()-1; index >= 0; index--)
        {
            PaoPao *temP = _paoV[index];
            
            if (temP->getPaoName() == PaoNameTimeBomb)
            {
                if (getAbsoluteRowColByPos(temP->getPosition()).m_nRow < _baseRoundStartRow)
                {
                    temP->setDeathRound(TIMEBOMBROUND+1);
                }
            }
        }
        
        if (_waitingBall == NULL)
        {
            createWaitingBall();
            createFireBall();
            _waitingBall->removeFromParent();
            createWaitingBall();
        }
        else
        {
            createFireBall();
            _waitingBall->removeFromParent();
            createWaitingBall();
        }
        
        _playStatu = PlayStatuFire;
    }
    else if (_playStatu == PlayStatuPaoDrop)
    {
        if (_specialColor != PaoColorNone && _specialColor != PaoColorColorful)
        {
            _fireBall = PaoPao::createWithNumber(1300+_specialColor);
            _fireBall->setPosition(_spBallStartPos);
            
            _qiuLayer->addChild(_fireBall, 1);
            
            _specialColor = PaoColorNone;
            
            _playStatu = PlayStatuPaoRunning;
            
            schedule(schedule_selector(GamePlayScene::updateFireBall), 0.01);
        }
        else
        {
            paoRoundDeal(1000);
        }
    }
    else if (_playStatu == PlayStatuPaoDropAgain)
    {
        randomPosForBee();
    }
}

void GamePlayScene::randomPosForBee()
{
    std::vector<int> beeIndexV;
    std::vector<Point> beePosV;
    
    std::vector<int> randomPaoIndexV;
    
    int paoVSize = _paoV.size();
    
    for (int index = 0; index < paoVSize; index++)
    {
        randomPaoIndexV.push_back(index);
        
        PaoPao *temP = _paoV[index];
        
        if (temP->getPaoName() == PaoNameBee && getAbsoluteRowColByPos(temP->getPosition()).m_nRow >= _baseRoundStartRow)
        {
            beeIndexV.push_back(index);
            beePosV.push_back(_paoV[index]->getPosition());
        }
    }
    
    int beeIndexVSize = beeIndexV.size();
    
    if (beeIndexVSize == 0)
    {
        finishedRunForBee();
        return;
    }
    
    std::random_shuffle(randomPaoIndexV.begin(), randomPaoIndexV.end());
    
    std::vector<int> targetIndexV;
    std::vector<Point> targetPosV;
    
    for (int index = 0; index < paoVSize; index++)
    {
        PaoPao *temP = _paoV[randomPaoIndexV[index]];
        
        if (temP->getPaoName() == PaoNamePao && temP->getPaoStatu() == PaoStatuNormal && getAbsoluteRowColByPos(temP->getPosition()).m_nRow >= _baseRoundStartRow)
        {
            targetIndexV.push_back(randomPaoIndexV[index]);
            targetPosV.push_back(temP->getPosition());
            beeIndexVSize -= 1;
        }
        
        if (beeIndexVSize == 0)
        {
            break;
        }
    }
    
    if (targetIndexV.size() == 0)
    {
        finishedRunForBee();
        return;
    }
    else
    {
        finishedRunForBee();
    }
}
void GamePlayScene::finishedRunForBee()
{
    if (isWin())
    {
        _playStatu = PlayStatuDataDeal;
        scheduleOnce(schedule_selector(GamePlayScene::winDeal), StatuAnimationDeley);
        return;
    }
    if (isFail())
    {
        _playStatu = PlayStatuDataDeal;
        scheduleOnce(schedule_selector(GamePlayScene::failDeal), StatuAnimationDeley);
        return;
    }
    
    if (_waitingBall == NULL)
    {
        createWaitingBall();
        createFireBall();
        _waitingBall->removeFromParent();
        createWaitingBall();
    }
    else
    {
        createFireBall();
        _waitingBall->removeFromParent();
        createWaitingBall();
    }
    
    _playStatu = PlayStatuFire;
}

#pragma mark -
#pragma mark 触摸发射球
#pragma mark -

bool GamePlayScene::onMyTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event)
{
    return true;
}

void GamePlayScene::onMyTouchEnded(Touch *touch, Event *unused_event)
{
//    InView
//    log("P tx : %g,ty : %g",touch->getLocation().x,touch->getLocation().y);
//    log("P px : %g,py : %g",_qiuLayer->getPositionX(),_qiuLayer->getPositionY());
    
    if (_playStatu != PlayStatuFire)
    {
        log("_playStatu != PlayStatuFire");
        log("_playStatu == %d",_playStatu);
        return;
    }
    
    _startFire = true;
    
    Point newP = Point(touch->getLocation().x,touch->getLocation().y-_transformFixH);
    
    if (newP.y < 500-_transformFixH)
    {
//        log("newP.y = %g, 500-_transformFixH = %g",newP.y,500-_transformFixH);
        
        exchangeBall();
        
        return;
    }
    
    _playStatu = PlayStatuPaoRunning;
    
    _fireVector = (newP-_fireBallStartPos).getNormalized()*1.5;
    
    _lastFireVector = _fireVector;
    
    schedule(schedule_selector(GamePlayScene::updateFireBall), 0.01);
}

#pragma mark -
#pragma mark 创建fireball
#pragma mark -

void GamePlayScene::createFireBall()
{
    if (_ballDownCount == 0)
    {
        return;
    }
    
    long fireBallNum = 1101;
    
    if (_waitingBall != NULL)
    {
        fireBallNum = _waitingBall->getPaoNumber();
    }
    
    _fireBall = PaoPao::createWithNumber(fireBallNum);
    _fireBall->setPosition(_fireBallStartPos);
    _qiuLayer->addChild(_fireBall,1);
    
    _ballDownCount--;
    
//    log("createFireBall - %ld, _waitingBall->getPaoNumber() - %ld",fireBallNum,_waitingBall->getPaoNumber());
}

#pragma mark -
#pragma mark 创建等待的球
#pragma mark -

void GamePlayScene::createWaitingBall()
{
    if (_ballDownCount == 0)
    {
        return;
    }
    
    long waitingBallNum = 1101;
    
    if (_ballDownVector.size() > 0)
    {
        if (_ballDownVector[0] !=0)
        {
            waitingBallNum = _ballDownVector[0];
        }
        _ballDownVector.erase(_ballDownVector.begin());
    }
//    transformWithLevelRandomNewPaoIndexV(waitingBallNum)
    //2007
    _waitingBall = PaoPao::createWithNumber(transformWithLevelRandomNewPaoIndexV(waitingBallNum));
    _waitingBall->setPosition(_fireBallStartPos.x,_fireBallStartPos.y-100);
    
    _qiuLayer->addChild(_waitingBall,1);
    
//    log("createWaitingBall - %ld, _waitingBall->getPaoNumber() - %ld",waitingBallNum,_waitingBall->getPaoNumber());
}

#pragma mark -
#pragma mark 交换球
#pragma mark -

void GamePlayScene::exchangeBall()
{
    if (_fireBall == NULL || _waitingBall == NULL)
    {
        return;
    }
    
    long fPaoNum = _fireBall->getPaoNumber();
    long wPaoNum = _waitingBall->getPaoNumber();
    
    _fireBall->removeFromParent();
    _waitingBall->removeFromParent();
    
    _fireBall = PaoPao::createWithNumber(wPaoNum);
    _fireBall->setPosition(_fireBallStartPos);
    _qiuLayer->addChild(_fireBall,1);
    
    _waitingBall = PaoPao::createWithNumber(fPaoNum);
    _waitingBall->setPosition(_fireBallStartPos.x, _fireBallStartPos.y-100);
    _qiuLayer->addChild(_waitingBall,1);
    
    
}

#pragma mark -
#pragma mark fireball在跑的过程
#pragma mark -

void GamePlayScene::updateFireBall(float dt)
{
//    log("schedule paoV : %ld",_paoV.size());
    
    if (isCollisionWithBorder())
    {
        _fireVector.x = -_fireVector.x;
    }
    
    int paoSpeed = 30;
    
    Point pos = _fireBall->getPosition();
    
//    _fireBall->setPosition(pos.x+_fireVector.x*paoSpeed, pos.y+_fireVector.y*paoSpeed);
    
    Point firePos = Point(pos.x+_fireVector.x*paoSpeed, pos.y+_fireVector.y*paoSpeed);
    
    _fireBall->setPosition(firePos);
    
    RowCol temRC = getAbsoluteRowColByPos(firePos);
    
    if (temRC.m_nRow > _currentMaxRow+1)
    {
        return;
    }
    
    if (isCollisionWithBallOrTop())
    {
        unschedule(schedule_selector(GamePlayScene::updateFireBall));
        
        RowCol fireRC = getAbsoluteRowColByPos(_fireBall->getPosition());
        Point firePos = getPosByRowAndCol(fireRC);
        _fireBall->setPosition(firePos);
        fireRC = getAbsoluteRowColByPos(firePos);
        _fireBall->setTag(fireRC.m_nRow*100+fireRC.m_nCol);
        
//        if (fireRC.m_nRow < _baseRoundStartRow)
//        {
//            _baseRoundStartRow = fireRC.m_nRow;
//        }
//        log("_baseRoundStartRow = %d",_baseRoundStartRow);
        
        cleanPaoPao(1000);
//        dealPaoAfterFireBallStopRunning();
    }
}

#pragma mark -
#pragma mark fireball是否碰到左右边界
#pragma mark -

bool GamePlayScene::isCollisionWithBorder()
{
    if(_qiuLayer->getContentSize().width-_fireBall->getPositionX() <= CIRCLER+_fixXDistance || _fireBall->getPositionX() <= CIRCLER+_fixXDistance)
    {
        return true;
    }
    
    return false;
}

#pragma mark -
#pragma mark fireball是否碰到其他球或者顶部
#pragma mark -

bool GamePlayScene::isCollisionWithBallOrTop()
{
    if (_qiuLayer->getContentSize().height -_fixYDistance - _fireBall->getPositionY() <= CIRCLER)
    {
        return true;
    }
    
//    for (int indexY = _currentMaxRow; indexY >= 0; indexY--)
//    {
//        for (int indexX = ColCount-1; indexX >= 0; indexX--)
//        {
//            if (_qiuLayer->getChildByTag(indexY*100+indexX) == nullptr)
//            {
//                continue;
//            }
//            
//            PaoPao *temPao = (PaoPao*)_qiuLayer->getChildByTag(indexY*100+indexX);
//            
//            Point pos1 = temPao->getPosition();
//            Point pos2 = _fireBall->getPosition();
//            
//            if (isCollisionWithOtherBall(pos1, CIRCLER, pos2, CIRCLER-PassFix))
//            {
//                return true;
//            }
//        }
//    }
    
    Point firePos = _fireBall->getPosition();
    
    for (int index = _paoV.size()-1; index >= 0; index--)
    {
        if (isCollisionWithOtherBall(_paoV[index]->getPosition(), CIRCLER, firePos, CIRCLER))
        {
            return true;
        }
    }
    
    return false;
}

#pragma mark -
#pragma mark 两球是否碰撞或连接
#pragma mark -

bool GamePlayScene::isCollisionWithOtherBall(Point pos1, float radius1, Point pos2, float radius2)
{
//    log("pos2.getDistance(pos1) ---- %g,CIRCLED ---- %d",pos2.getDistance(pos1),CIRCLED);
//    log("x2 : %g,y2 : %g,x2+y2 : %g,z2 : %g",pow(pos1.x-pos2.x, 2),pow(pos1.y-pos2.y, 2),pow(pos1.x-pos2.x, 2)+pow(pos1.y-pos2.y, 2),pow(radius1+radius2, 2));
//    return pow(pos1.x-pos2.x, 2)+pow(pos1.y-pos2.y, 2) <= pow(radius1+radius2, 2);
    
    if (pos1 == pos2)
    {
        return false;
    }
    
    int distance = pos2.getDistance(pos1);
    
    if (distance <= (radius1+radius2))
    {
        return true;
    }
    
    return false;
}

#pragma mark -
#pragma mark 清除泡泡的过程开始
#pragma mark -

void GamePlayScene::cleanPaoPao(float dt)
{
    if (_playStatu == PlayStatuPaoRunning)
    {
        _playStatu = PlayStatuPaoClean;
        
        bool dealResult = dealPaoAfterFireBallStopRunning();
        
//        while (true)
//        {
//            if (_fanActionLock == false)
//            {
//                break;
//            }
//        }
        
        if (_isDeath)
        {
            log("ISDEATH");
            _playStatu = PlayStatuDataDeal;
            
            scheduleOnce(schedule_selector(GamePlayScene::failDeal), StatuAnimationDeley);
            
            return;
        }
        
        if (dealResult)
        {
            scheduleOnce(schedule_selector(GamePlayScene::dropPaoPao), StatuAnimationDeley);
        }
        else
        {
            log("NO DEAL");
            if (std::find(_paoV.begin(), _paoV.end(), _fireBall) == _paoV.end())
            {
                _paoV.push_back(_fireBall);
            }
            
            scheduleOnce(schedule_selector(GamePlayScene::dropPaoPao), StarnderdDeley);
        }
        
        log("++++++ %d",getAbsoluteRowColByPos(_fireBall->getPosition()).m_nRow);
        log("++++++pos - _f X:%g,Y:%g",_fireBall->getPositionX(),_fireBall->getPositionY());
    }
}

#pragma mark -
#pragma mark 掉落泡泡的过程开始
#pragma mark -

void GamePlayScene::dropPaoPao(float dt)
{
    if (_playStatu == PlayStatuPaoClean)
    {
        _playStatu = PlayStatuPaoDrop;
    }
    else if (_playStatu == PlayStatuPaoRoundDeal)
    {
        _playStatu = PlayStatuPaoDropAgain;
    }
    
    if (dropDownPaoPao(findDropDownPaoPao()))
    {
        scheduleOnce(schedule_selector(GamePlayScene::adjustLocation), StatuAnimationDeley);
    }
    else
    {
        scheduleOnce(schedule_selector(GamePlayScene::adjustLocation), StarnderdDeley);
//        adjustLocation(1000);
    }
}

#pragma mark -
#pragma mark 每回合球的回合触发状态开始
#pragma mark -

void GamePlayScene::paoRoundDeal(float dt)
{
    if (_playStatu == PlayStatuPaoDrop)
    {
        _playStatu = PlayStatuPaoRoundDeal;
        
        if (roundDeathDeal())
        {
            if (_isDeath)
            {
                _playStatu = PlayStatuDataDeal;
                scheduleOnce(schedule_selector(GamePlayScene::failDeal), StatuAnimationDeley);
            }
            else
            {
                scheduleOnce(schedule_selector(GamePlayScene::dropPaoPao), StatuAnimationDeley);
            }
        }
        else
        {
            scheduleOnce(schedule_selector(GamePlayScene::dropPaoPao), StarnderdDeley);
//            dropPaoPao(1000);
        }
    }
}

#pragma mark -
#pragma mark 调整位置开始
#pragma mark -

void GamePlayScene::adjustLocation(float dt)
{
    adjustQiuLayer();
}

#pragma mark -
#pragma mark 碰撞后判断并处理需要清除的球，并返回是否有球被清除
#pragma mark -

bool GamePlayScene::dealPaoAfterFireBallStopRunning()
{
//    log("pao name %d,pao num = %ld",_fireBall->getPaoName(),_fireBall->getPaoNumber());
    
    if (_fireBall->isPaoPao())
    {
        std::vector<int> nearPaoIndexV = findPaoIndexNearTargetPao(_fireBall->getPaoColor(), _fireBall->getPosition(),true);
        
        if (_isDeath)
        {
            log("OK FALSE Near");
            return false;
        }
        if (_isSwpiderWeb)
        {
            _isFireBallDeath = true;
            nearPaoIndexV.clear();
            clearPaoPao(nearPaoIndexV);
            return true;
        }
        
        std::vector<int> triggerClearPaoIndexV = findTriggerClearPao();
        
        if (_isDeath)
        {
            log("OK FALSE Trigger");
            return false;
        }
        
//        if (triggerClearPaoIndexV.size() == 1)
//        {
//            if (_paoV[triggerClearPaoIndexV[0]]->getPaoName() == PaoNameSpiderWeb)
//            {
//                clearPaoPao(triggerClearPaoIndexV);
//                return true;
//            }
//        }
        
        std::vector<int> temCIV = findPaoSamePao(nearPaoIndexV);
        
        temCIV = sortAndDelDuplicateVector(temCIV);
        
        if (temCIV.size() < 2)
        {
            temCIV.clear();
        }
        else
        {
            _isFireBallDeath = true;
        }
        
        temCIV.insert(temCIV.end(), triggerClearPaoIndexV.begin(), triggerClearPaoIndexV.end());
        
        std::vector<int> temClearIndexV = sortAndDelDuplicateVector(temCIV);
        
//        if (temClearIndexV.size() == 1 && _paoV[temClearIndexV[0]]->isTrigger())
//        {
//            clearPaoPao(temClearIndexV);
//            return true;
//        }
        if (temClearIndexV.size() == 0)
        {
            return false;
        }
        
        clearPaoPao(temClearIndexV);
        
        return true;
    }
    else if (_fireBall->getPaoColor() == PaoColorColorful)
    {
        std::vector<int> nearPaoIndexV = findPaoIndexNearTargetPao(_fireBall->getPaoColor(), _fireBall->getPosition(),true);
        
        if (_isDeath)
        {
            log("OK FALSE Near");
            return false;
        }
        if (_isSwpiderWeb)
        {
            _isFireBallDeath = true;
            nearPaoIndexV.clear();
            clearPaoPao(nearPaoIndexV);
            return true;
        }
        
        std::vector<int> triggerClearPaoIndexV = findTriggerClearPao();
        
        if (_isDeath)
        {
            log("OK FALSE Trigger");
            return false;
        }
        
//        if (triggerClearPaoIndexV.size() == 1)
//        {
//            if (_paoV[triggerClearPaoIndexV[0]]->getPaoName() == PaoNameSpiderWeb)
//            {
//                clearPaoPao(triggerClearPaoIndexV);
//                return true;
//            }
//        }
        
        std::vector<int> temClearIndexV;
        
        //将与fireball连接的一串彩球找出来，并将对应每颗彩球周围的球找出来
        std::vector<int> temNearColorBallIndexV = findLinkedColorPao(_fireBall->getPosition());
        
        for (int index = 0; index < temNearColorBallIndexV.size(); index++)
        {
            std::vector<int> temNV = findPaoIndexNearTargetPao(PaoColorColorful, _paoV[temNearColorBallIndexV[index]]->getPosition(),false);
            nearPaoIndexV.insert(nearPaoIndexV.end(), temNV.begin(), temNV.end());
        }
        
        nearPaoIndexV = sortAndDelDuplicateVector(nearPaoIndexV);
        
        //按颜色将找出来的球分类
        std::vector<std::vector<int>> branchVV;
        for (int index = 0; index < nearPaoIndexV.size(); index++)
        {
            std::vector<int>::iterator it = nearPaoIndexV.begin()+index;
            int itIndex = *it;
            PaoColor branchColor = _paoV[itIndex]->getPaoColor();
            
            if (branchColor == PaoColorColorful)
            {
                continue;
            }
            
            std::vector<int> aBranchV;
            
            aBranchV.push_back(itIndex);
            
            for (int sign = 0; sign < nearPaoIndexV.size(); sign++)
            {
                std::vector<int>::iterator temIt = nearPaoIndexV.begin()+sign;
                int temItIndex = *temIt;
                
                PaoColor temItColor = _paoV[temItIndex]->getPaoColor();
                
                if (temItColor == PaoColorColorful)
                {
                    aBranchV.push_back(temItIndex);
                    continue;
                }
                if (sign <= index)
                {
                    continue;
                }
                if (temItColor == branchColor)
                {
                    aBranchV.push_back(temItIndex);
                }
            }
            branchVV.push_back(aBranchV);
        }
        
        //按照每个分类，对应找到所有需消除的球
        std::vector<int> temCIV;
        
        int branchVVCount = branchVV.size();
        
        for (int index = 0; index < branchVVCount; index++)
        {
            std::vector<int> temBranchIndexV = findPaoSamePao(branchVV[index]);
            if (temBranchIndexV.size() < 2)
            {
                continue;
            }
            temCIV.insert(temCIV.end(), temBranchIndexV.begin(), temBranchIndexV.end());
        }
        
        temCIV.insert(temCIV.end(), temNearColorBallIndexV.begin(), temNearColorBallIndexV.end());
        
        temCIV = sortAndDelDuplicateVector(temCIV);
        
        if (temCIV.size() < 2)
        {
            temCIV.clear();
        }
        else
        {
            _isFireBallDeath = true;
        }
        
        temCIV.insert(temCIV.end(), triggerClearPaoIndexV.begin(), triggerClearPaoIndexV.end());
        //排重
        temClearIndexV = sortAndDelDuplicateVector(temCIV);
    
//        if (temClearIndexV.size() == 1 && _paoV[temClearIndexV[0]]->isTrigger())
//        {
//            clearPaoPao(temClearIndexV);
//            return true;
//        }
        if (temClearIndexV.size() == 0)
        {
            return false;
        }
        else
        {
            clearPaoPao(temClearIndexV);
            return true;
        }
    }
    else if (_fireBall->getPaoName() == PaoNameMeteorite || _fireBall->getPaoName() == PaoNameCustomMeteorite)
    {
        _isFireBallDeath = true;
        std::vector<int> nearPaoIndexV = findMeteoriteCleanPao(_fireBall->getDestruction());
        
        if (_isDeath)
        {
            log("_isDeath");
            _triggerPaoIndexV.clear();
            return false;
        }
        if (_isSwpiderWeb)
        {
            log("_isSwpiderWeb");
            _triggerPaoIndexV.clear();
            clearPaoPao(nearPaoIndexV);
            return true;
        }
        
        for (int giddyIndex = 0; giddyIndex < nearPaoIndexV.size(); giddyIndex++)
        {
            PaoPao *giddyPao = _paoV[nearPaoIndexV[giddyIndex]];
            if (giddyPao->isPaoPao() && giddyPao->getPaoStatu() == PaoStatuNormal)
            {
                giddyPao->setPaoStatu(PaoStatuGiddy);
                giddyPao->addStatu();
            }
        }
        
        nearPaoIndexV.clear();
        
        std::vector<int> triggerClearPaoIndexV = findTriggerClearPao();
        
        if (_isDeath)
        {
            log("OK FALSE");
            return false;
        }
        
        clearPaoPao(triggerClearPaoIndexV);
        
        return true;
    }
    else if (_fireBall->getPaoName() == PaoNameBomb || _fireBall->getPaoName() == PaoNameCustomBomb)
    {
        _isFireBallDeath = true;
//        _fireBall->getDestruction()
        std::vector<int> nearPaoIndexV = findBombCleanPao(3);
        
        if (_isDeath)
        {
            log("_isDeath");
            _triggerPaoIndexV.clear();
            return false;
        }
        if (_isSwpiderWeb)
        {
            log("_isSwpiderWeb");
            _triggerPaoIndexV.clear();
            clearPaoPao(nearPaoIndexV);
            return true;
        }
        
        std::vector<int> triggerClearPaoIndexV = findTriggerClearPao();
        
        if (_isDeath)
        {
            log("OK FALSE");
            return false;
        }
        
        nearPaoIndexV.insert(nearPaoIndexV.end(), triggerClearPaoIndexV.begin(), triggerClearPaoIndexV.end());
        
        clearPaoPao(sortAndDelDuplicateVector(nearPaoIndexV));
        
        return true;
    }
    else if (_fireBall->getPaoName() == PaoNameFan || _fireBall->getPaoName() == PaoNameCustomFan)
    {
        log("Fan");
        _fanActionLock = true;
        _fanCleanColFix = _fireBall->getDestruction()/2;
        _fanCleanColFix = 2;
        
        //产生一个光点，分裂成对应数目的光点，范围是几就是几个光点，然后再往下一个对应位置走，结束后消除到达的行要消除的球，然后继续向下走
        
        RowCol fireRC = getAbsoluteRowColByPos(_fireBall->getPosition());
        
        for (int targetCol = fireRC.m_nCol-_fanCleanColFix; targetCol<=fireRC.m_nCol+_fanCleanColFix; targetCol++)
        {
            log("targetCol = %d",targetCol);
            if (targetCol < 0)
            {
                continue;
            }
            if (targetCol >= ColCount)
            {
                break;
            }
            
            ParticleSun *pSun = ParticleSun::createWithTotalParticles(260);
//            auto *pSun = ParticleSystemQuad::create("res/GamePlay/lineSpot.plist");
//            auto *pSun = Sprite::create("res/GamePlay/snow.png");
            
            log("r - %d,c - %d",fireRC.m_nRow,targetCol);
            log("pos - _f X:%g,Y:%g P X:%g,Y:%g",_fireBall->getPositionX(),_fireBall->getPositionY(),pSun->getPositionX(),pSun->getPositionY());
            pSun->setPosition(getPosByRowAndCol(fireRC.m_nRow, targetCol));
            pSun->setTag(10000+targetCol);
            
            _qiuLayer->addChild(pSun,3);
            
            
            log("------%d",getAbsoluteRowColByPos(pSun->getPosition()).m_nRow);
            log("pos - _f X:%g,Y:%g P X:%g,Y:%g",_fireBall->getPositionX(),_fireBall->getPositionY(),pSun->getPositionX(),pSun->getPositionY());
            
//            return false;
            
            Vector<cocos2d::FiniteTimeAction *> fanA;
            
            auto startA = CallFunc::create(
                                           [&](){
                                               std::vector<int> temV;
                                               _isFireBallDeath = true;
                                               this->clearPaoPao(temV);
                                           });
            
            fanA.pushBack(startA);
            
            for (int sign = fireRC.m_nRow; sign > fireRC.m_nRow-FANCLEANROW; sign--)
            {
                if (sign < 0)
                {
                    break;
                }
                log("SIGN = %d",sign);
                auto pSunRunAction = MoveTo::create(StatuAnimationDeley/(FANCLEANROW*1.0), getPosByRowAndCol(sign, targetCol));
                
                auto pSunRunAfterAction = CallFunc::create(
                                                           [&,sign,targetCol](){
                                                               log("1");
                                                               log("sign == %d,targetCol == %d",sign,targetCol);
                                                               if (_qiuLayer->getChildByTag(sign*100+targetCol) != NULL)
                                                               {log("2");
                                                                   PaoPao *temP = (PaoPao*)(_qiuLayer->getChildByTag(sign*100+targetCol));
                                                                   
                                                                   auto temIt = std::find(_paoV.begin(), _paoV.end(), temP);
                                                                   
                                                                   if (temIt != _paoV.end())
                                                                   {log("3");
                                                                       int temPIndex = temIt-_paoV.begin();
                                                                       
                                                                       
                                                                       if (temP->isTrigger())
                                                                       {log("4-1");
                                                                           _triggerPaoIndexV.push_back(temPIndex);
                                                                           this->clearPaoPao(this->findTriggerClearPao());
                                                                           _triggerPaoIndexV.clear();
                                                                           
                                                                           if (_isDeath)
                                                                           {
                                                                               log("ISDEATH");
                                                                               _playStatu = PlayStatuDataDeal;
                                                                               
                                                                               scheduleOnce(schedule_selector(GamePlayScene::failDeal), StatuAnimationDeley);
                                                                           }
                                                                       }
                                                                       else
                                                                       {log("4-2");
                                                                           std::vector<int> cleanIV;
                                                                           cleanIV.push_back(temPIndex);
                                                                           this->clearPaoPao(cleanIV);
                                                                       }
                                                                   }
                                                               }
                                                               
                                                           });
                
                fanA.pushBack(pSunRunAction);
                fanA.pushBack(pSunRunAfterAction);
            }
            
            auto endA = CallFunc::create(
                                         [&,pSun](){
                                             
                                             pSun->removeFromParent();
                                             
                                             bool targetLock = false;
                                             
                                             for (Vector<Node*>::iterator it = _qiuLayer->getChildren().begin(); it != _qiuLayer->getChildren().end(); it++)
                                             {
                                                 if ((*it)->getTag() > 10000)
                                                 {
                                                     targetLock = true;
                                                     break;
                                                 }
                                             }
                                             _fanActionLock = targetLock;
                                         });
            fanA.pushBack(endA);
            pSun->runAction(Sequence::create(fanA));
        }
        return true;
    }
//    log("pao name %d,pao num = %ld",_fireBall->getPaoName(),_fireBall->getPaoNumber());
    
    return false;
}

void GamePlayScene::fanClean(float dt)
{
//    RowCol fireRC = getRowColByPos(_fireBall->getPosition());
//    
//    for (int targetCol = fireRC.m_nCol-_fanCleanColFix; targetCol<=fireRC.m_nCol+_fanCleanColFix; targetCol++)
//    {
//        if (targetCol < 0)
//        {
//            continue;
//        }
//        if (targetCol >= ColCount)
//        {
//            break;
//        }
//        
//        
//    }
}

#pragma mark -
#pragma mark 按条件返回目标位置周围的球
#pragma mark -

std::vector<int> GamePlayScene::findPaoIndexNearTargetPao(PaoColor temColor, Point pos, bool chooseTrigger)
{
    std::vector<int> temV;
    
    for (int index = 0; index < _paoV.size(); index++)
    {
        PaoPao *temPao = _paoV[index];
        
        if (isCollisionWithOtherBall(pos, CIRCLER, temPao->getPosition(), CIRCLER))
        {
            if (temPao->getPaoStatu() == PaoStatuRattanCover)
            {
                continue;
            }
            
            if (temPao->isTrigger())
            {
                if (chooseTrigger)
                {
//                    _triggerPaoIndexV.push_back(index);
//                    log("_triggerPaoIndexV.push_back(%d)",index);
//                    if (temPao->getPaoName() == PaoNameBombPao || temPao->getPaoName() == PaoNameSpiderWeb)
//                    {
//                        _isFireBallDeath = true;
//                    }
                    
                    if (temPao->getPaoName() == PaoNameDemonPao)
                    {
                        _isDeath = true;
                        temV.clear();
                        _triggerPaoIndexV.clear();
                        return temV;
                    }
                    if (temPao->getPaoName() == PaoNameSpiderWeb)
                    {
                        _isSwpiderWeb = true;
                    }
                    else if (temPao->getPaoName() == PaoNameBombPao)
                    {
                        _isFireBallDeath = true;
                        _triggerPaoIndexV.push_back(index);
                    }
                    else
                    {
                        _triggerPaoIndexV.push_back(index);
                    }
                }
                continue;
            }
            if (temColor == PaoColorColorful || temColor == PaoColorNone)
            {
                temV.push_back(index);
            }
            else
            {
                if (temPao->getPaoColor() == temColor)
                {
                    temV.push_back(index);
                }
            }
            
        }
    }
    return temV;
}

std::vector<int> GamePlayScene::findMeteoriteCleanPao(int circleCount)
{
    log("circleCount : %d",circleCount);
    
    std::vector<int> targetIndexV;
    
    std::vector<int> midIndexV;
    
    for (int index = 0; index < _paoV.size(); index++)
    {
        PaoPao *temP = _paoV[index];
        
        if (isCollisionWithOtherBall(_fireBall->getPosition(), CIRCLER, temP->getPosition(), CIRCLER))
        {
            if (temP->getPaoName() == PaoNameDemonPao)
            {
                _isDeath = true;
                targetIndexV.clear();
                return targetIndexV;
            }
            if (temP->getPaoName() == PaoNameSpiderWeb)
            {
                _isSwpiderWeb = true;
                targetIndexV.clear();
                return targetIndexV;
            }
            
            if (temP->isTrigger())
            {
                midIndexV.push_back(index);
                _triggerPaoIndexV.push_back(index);
            }
            else if (temP->isPaoPao() && temP->getPaoStatu() != PaoStatuRattanCover && temP->getPaoStatu() != PaoStatuRattanSource)
            {
                midIndexV.push_back(index);
            }
        }
    }
    
    targetIndexV.insert(targetIndexV.end(), midIndexV.begin(), midIndexV.end());
    
    log("***************\n%ld\n***************",targetIndexV.size());
    
    for (int index = 1; index < circleCount; index++)
    {
        std::vector<int> temIndexV = midIndexV;
        
        midIndexV.clear();
        
        int temIndexVCount = temIndexV.size();
        
        for (int sign = 0; sign < temIndexVCount; sign++)
        {
            PaoPao *temPP = _paoV[temIndexV[sign]];
            
            if (temPP->getPaoName() == PaoNameSpiderWeb)
            {
                continue;
            }
            
            for (int temIndex = 0; temIndex < _paoV.size(); temIndex++)
            {
                PaoPao *temPao = _paoV[temIndex];
                
                if (isCollisionWithOtherBall(temPP->getPosition(), CIRCLER, temPao->getPosition(), CIRCLER))
                {
                    if (temPao->getPaoName() == PaoNameDemonPao)
                    {
                        _isDeath = true;
                        targetIndexV.clear();
                        return targetIndexV;
                    }
                    if (temPao->getPaoName() == PaoNameSpiderWeb)
                    {
                        midIndexV.push_back(temIndex);
                        continue;
                    }
                    
                    if (std::find(targetIndexV.begin(), targetIndexV.end(), temIndex) != targetIndexV.end())
                    {
                        continue;
                    }
                    if (std::find(midIndexV.begin(), midIndexV.end(), temIndex) != midIndexV.end())
                    {
                        continue;
                    }
                    
                    if (temPao->isTrigger())
                    {
                        _triggerPaoIndexV.push_back(temIndex);
                        midIndexV.push_back(temIndex);
                    }
                    else if (temPao->isPaoPao() && temPao->getPaoStatu() != PaoStatuRattanCover && temPao->getPaoStatu() != PaoStatuRattanSource)
                    {
                        midIndexV.push_back(temIndex);
                    }
                }
            }
        }
        
        targetIndexV.insert(targetIndexV.end(), midIndexV.begin(), midIndexV.end());
    }
    
    log("***************\n%ld\n***************",targetIndexV.size());
    
    return sortAndDelDuplicateVector(targetIndexV);
}
std::vector<int> GamePlayScene::findBombCleanPao(int circleCount)
{
    
    log("circleCount : %d",circleCount);
    
    std::vector<int> targetIndexV;
    
    std::vector<int> midIndexV;
    
    for (int index = 0; index < _paoV.size(); index++)
    {
        PaoPao *temP = _paoV[index];
        
        if (isCollisionWithOtherBall(_fireBall->getPosition(), CIRCLER, temP->getPosition(), CIRCLER))
        {
            if (temP->getPaoName() == PaoNameDemonPao)
            {
                _isDeath = true;
                targetIndexV.clear();
                return targetIndexV;
            }
            if (temP->getPaoName() == PaoNameSpiderWeb)
            {
                _isSwpiderWeb = true;
                targetIndexV.clear();
                return targetIndexV;
            }
            
            if (temP->isTrigger())
            {
                midIndexV.push_back(index);
                _triggerPaoIndexV.push_back(index);
            }
            else if (temP->isPaoPao() && temP->getPaoStatu() != PaoStatuRattanCover)
            {
                midIndexV.push_back(index);
            }
        }
    }
    
    targetIndexV.insert(targetIndexV.end(), midIndexV.begin(), midIndexV.end());
    
    log("***************\n%ld\n***************",targetIndexV.size());
    
    for (int index = 1; index < circleCount; index++)
    {
        std::vector<int> temIndexV = midIndexV;
        
        midIndexV.clear();
        
        int temIndexVCount = temIndexV.size();
        
        for (int sign = 0; sign < temIndexVCount; sign++)
        {
            PaoPao *temPP = _paoV[temIndexV[sign]];
            
            if (temPP->getPaoStatu() == PaoStatuRattanSource)
            {
                continue;
            }
            if (temPP->getPaoName() == PaoNameSpiderWeb)
            {
                continue;
            }
            
            for (int temIndex = 0; temIndex < _paoV.size(); temIndex++)
            {
                PaoPao *temPao = _paoV[temIndex];
                
                if (isCollisionWithOtherBall(temPP->getPosition(), CIRCLER, temPao->getPosition(), CIRCLER))
                {
                    if (temPao->getPaoName() == PaoNameDemonPao)
                    {
                        _isDeath = true;
                        targetIndexV.clear();
                        return targetIndexV;
                    }
                    if (temPao->getPaoName() == PaoNameSpiderWeb)
                    {
                        midIndexV.push_back(temIndex);
                        continue;
                    }
                    
                    if (std::find(targetIndexV.begin(), targetIndexV.end(), temIndex) != targetIndexV.end())
                    {
                        continue;
                    }
                    if (std::find(midIndexV.begin(), midIndexV.end(), temIndex) != midIndexV.end())
                    {
                        continue;
                    }
                    
                    if (temPao->isTrigger())
                    {
                        _triggerPaoIndexV.push_back(temIndex);
                        midIndexV.push_back(temIndex);
                    }
                    else if (temPao->isPaoPao() && temPao->getPaoStatu() != PaoStatuRattanCover)
                    {
                        midIndexV.push_back(temIndex);
                    }
                }
            }
        }
        
        targetIndexV.insert(targetIndexV.end(), midIndexV.begin(), midIndexV.end());
    }
    
    log("***************\n%ld\n***************",targetIndexV.size());
    
    return sortAndDelDuplicateVector(targetIndexV);
}

#pragma mark -
#pragma mark 查找目标数组中相同颜色的球
#pragma mark -

std::vector<int> GamePlayScene::findPaoSamePao(std::vector<int> temClearIndexV)
{
    int temClearIndexVCount = temClearIndexV.size();
    int paoVCount = _paoV.size();
    
    
    PaoColor targetColor = PaoColorNone;
    
    for (int index = 0; index < temClearIndexVCount; index++)
    {
        targetColor = _paoV[temClearIndexV[index]]->getPaoColor();
        if (targetColor != PaoColorColorful)
        {
            break;
        }
    }
    //出错控制
    if (targetColor == PaoColorNone)
    {
        temClearIndexV.clear();
        return temClearIndexV;
    }
    
    for (int index = 0; index < temClearIndexV.size(); index++)
    {
        PaoStatu aPaoPaoStatu = _paoV[temClearIndexV[index]]->getPaoStatu();
        if (aPaoPaoStatu == PaoStatuRattanSource)
        {
            continue;
        }
        
        Point aPaoPaoPos = _paoV[temClearIndexV[index]]->getPosition();
        for (int sign = 0; sign < paoVCount; sign++)
        {
            if (std::find(temClearIndexV.begin(), temClearIndexV.end(), sign) == temClearIndexV.end())
            {
                PaoStatu bPaoPaoStatu = _paoV[sign]->getPaoStatu();
                if (bPaoPaoStatu == PaoStatuRattanCover)
                {
                    continue;
                }
                PaoColor bPaoPaoColor = _paoV[sign]->getPaoColor();
                if (bPaoPaoColor == targetColor || bPaoPaoColor == PaoColorColorful)
                {
                    Point bPaoPaoPos = _paoV[sign]->getPosition();
                    if (isCollisionWithOtherBall(aPaoPaoPos, CIRCLER, bPaoPaoPos, CIRCLER))
                    {
                        temClearIndexV.push_back(sign);
                    }
                }
            }
        }
    }
    
    return temClearIndexV;
}

#pragma mark -
#pragma mark 按位置查找目标彩球位置附近连接彩球
#pragma mark -

std::vector<int> GamePlayScene::findLinkedColorPao(Point sourcePos)
{
    std::vector<int> finalV;
    
    int paoVCount = _paoV.size();
    
    int index = -1;
    
    do
    {
        Point temPos = index==-1?sourcePos:_paoV[finalV[index]]->getPosition();
        
        index++;
        
        for (int sign = 0; sign < paoVCount; sign++)
        {
            if (_paoV[sign]->getPaoColor() == PaoColorColorful)
            {
                if (std::find(finalV.begin(), finalV.end(), sign) != finalV.end())
                {
                    continue;
                }
                if (isCollisionWithOtherBall(temPos, CIRCLER, _paoV[sign]->getPosition(), CIRCLER))
                {
                    finalV.push_back(sign);
                }
            }
        }
    }
    while (index < finalV.size());
    
    return finalV;
}

#pragma mark -
#pragma mark 查找直接触发的球，触发效果后符合消除条件的球
#pragma mark -

std::vector<int> GamePlayScene::findTriggerClearPao()
{
    std::vector<int> triggerClearPaoIndexV;
    
    for (int index = 0; index < _triggerPaoIndexV.size(); index++)
    {
        PaoPao *temPP = *(_paoV.begin() + _triggerPaoIndexV[index]);
        log("_triggerPaoIndexV[index] = %d",_triggerPaoIndexV[index]);
        
        if (temPP->getPaoName() == PaoNameDemonPao)
        {
            _triggerPaoIndexV.clear();
            triggerClearPaoIndexV.clear();
            _isDeath = true;
            log("OK");
            return triggerClearPaoIndexV;
        }
        
        std::vector<int> temBPN = findTriggerClearPaoByTrigger(_triggerPaoIndexV[index]);
        triggerClearPaoIndexV.insert(triggerClearPaoIndexV.end(),temBPN.begin(), temBPN.end());
        triggerClearPaoIndexV.push_back(_triggerPaoIndexV[index]);
    }
    
    triggerClearPaoIndexV = sortAndDelDuplicateVector(triggerClearPaoIndexV);
    
    /*
    int temTriggerCount = _triggerPaoIndexV.size();
    log("temTriggerCount = %d",temTriggerCount);
    
    bool hasSpiderWeb = false;
    int spwIndex;
    
    for (int index = 0; index < _triggerPaoIndexV.size(); index++)
    {
        PaoPao *temPP = *(_paoV.begin() + _triggerPaoIndexV[index]);
        log("_triggerPaoIndexV[index] = %d",_triggerPaoIndexV[index]);
        if (temPP->getPaoName() == PaoNameDemonPao)
        {
            _triggerPaoIndexV.clear();
            triggerClearPaoIndexV.clear();
            _isDeath = true;
            log("OK");
            break;
        }
        if (temPP->getPaoName() == PaoNameSpiderWeb)
        {
            spwIndex = _triggerPaoIndexV[index];
            hasSpiderWeb = true;
            continue;
        }
        
        std::vector<int> temBPN = findTriggerClearPaoByTrigger(_triggerPaoIndexV[index]);
        triggerClearPaoIndexV.insert(triggerClearPaoIndexV.end(),temBPN.begin(), temBPN.end());
        triggerClearPaoIndexV.push_back(_triggerPaoIndexV[index]);
    }
    
    if (hasSpiderWeb)
    {
        _isFireBallDeath = true;
        _triggerPaoIndexV.clear();
        triggerClearPaoIndexV.clear();
        triggerClearPaoIndexV.push_back(spwIndex);
    }
    */
    
    return triggerClearPaoIndexV;
}

std::vector<int> GamePlayScene::findTriggerClearPaoByTrigger(int triggerIndex)
{
    PaoPao *triggerPao = _paoV[triggerIndex];
    PaoName triggerPaoName = triggerPao->getPaoName();
    
    std::vector<int> triggerClearPaoIndexV;
    
    int paoVCount = _paoV.size();
    
    if (triggerPaoName == PaoNameBombPao)
    {
        Point triggerPos = triggerPao->getPosition();
        
        for (int index = 0; index < paoVCount; index++)
        {
            PaoPao *pPao = _paoV[index];
            
            if (isCollisionWithOtherBall(triggerPos, CIRCLER, pPao->getPosition(), CIRCLER))
            {
                if (pPao->getPaoStatu() == PaoStatuRattanCover)
                {
                    continue;
                }
                if (pPao->isTrigger() && pPao->getPaoName() != PaoNameSpiderWeb)
                {
                    if (std::find(_triggerPaoIndexV.begin(), _triggerPaoIndexV.end(), index) == _triggerPaoIndexV.end())
                    {
                        _triggerPaoIndexV.push_back(index);
                    }
                    continue;
                }
                if (pPao->isPaoPao() || pPao->getPaoColor() == PaoColorColorful)
                {
                    triggerClearPaoIndexV.push_back(index);
                }
            }
        }
    }
    else if (triggerPaoName == PaoNameFlashing)
    {
        int triggerTag = triggerPao->getTag();
        
        std::map<int,int> possibleClearIndexM;
        
        for (int index = 0; index < paoVCount; index++)
        {
            PaoPao *pPao = _paoV[index];
            int pPaoTag = pPao->getTag();
            if (pPaoTag/100 == triggerTag/100)
            {
                possibleClearIndexM[pPaoTag%100] = index;
            }
        }
        
//        for (std::map<int,int>::iterator it = possibleClearIndexM.begin(); it != possibleClearIndexM.end(); it++)
//        {
//            log("it->first -- %d, it->second -- %d",it->first,it->second);
//        }
        
        if (possibleClearIndexM.size() > 0)
        {
            for (int colL = triggerTag%100-1; colL >= 0; colL--)
            {
                if (possibleClearIndexM.find(colL) == possibleClearIndexM.end())
                {
                    continue;
                }
                PaoPao *keyPao = _paoV[possibleClearIndexM.at(colL)];
                
                if (keyPao->isTrigger() && keyPao->getPaoName() != PaoNameSpiderWeb)
                {
                    if (std::find(_triggerPaoIndexV.begin(), _triggerPaoIndexV.end(), possibleClearIndexM.at(colL)) == _triggerPaoIndexV.end())
                    {
                        _triggerPaoIndexV.push_back(possibleClearIndexM.at(colL));
                    }
                    continue;
                }
                
                if (keyPao->getPaoName() == PaoNameStone || keyPao->getPaoStatu() == PaoStatuRattanCover)
                {
                    break;
                }
                
                if (keyPao->getPaoStatu() == PaoStatuRattanSource)
                {
                    triggerClearPaoIndexV.push_back(possibleClearIndexM.at(colL));
                    break;
                }
                triggerClearPaoIndexV.push_back(possibleClearIndexM.at(colL));
            }
            for (int colR = triggerTag%100+1; colR < ColCount; colR++)
            {
                if (possibleClearIndexM.find(colR) == possibleClearIndexM.end())
                {
                    continue;
                }
                PaoPao *keyPao = _paoV[possibleClearIndexM.at(colR)];
                
                if (keyPao->isTrigger() && keyPao->getPaoName() != PaoNameSpiderWeb)
                {
                    if (std::find(_triggerPaoIndexV.begin(), _triggerPaoIndexV.end(), possibleClearIndexM.at(colR)) == _triggerPaoIndexV.end())
                    {
                        _triggerPaoIndexV.push_back(possibleClearIndexM.at(colR));
                    }
                    continue;
                }
                
                if (keyPao->getPaoName() == PaoNameStone || keyPao->getPaoStatu() == PaoStatuRattanCover)
                {
                    break;
                }
                
                if (keyPao->getPaoStatu() == PaoStatuRattanSource)
                {
                    triggerClearPaoIndexV.push_back(possibleClearIndexM.at(colR));
                    break;
                }
                triggerClearPaoIndexV.push_back(possibleClearIndexM.at(colR));
            }
        }
    }
    
    return triggerClearPaoIndexV;
}

#pragma mark -
#pragma mark 查找符合掉落条件的球
#pragma mark -

std::vector<int> GamePlayScene::findDropDownPaoPao()
{
    std::vector<int> temClearIV;
    std::vector<int> temUsedIV;
    
    for (int index = 0; index < _paoV.size(); index++)
    {
        std::vector<int>::iterator it = std::find(temUsedIV.begin(), temUsedIV.end(), index);
        if (it != temUsedIV.end())
        {
            continue;
        }
        
        std::vector<int> temPIV;
        temPIV.push_back(index);
        bool isDrop = true;
        
        for (int sign = 0; sign < temPIV.size(); sign++)
        {
            PaoPao* temPao = _paoV[temPIV[sign]];
            
            if ((temPao->getTag())/100 == 0)
            {
                isDrop = false;
            }
            
            for (int temIndex = 0; temIndex < _paoV.size(); temIndex++)
            {
                std::vector<int>::iterator temIt = std::find(temPIV.begin(), temPIV.end(), temIndex);
                if (temIt != temPIV.end())
                {
                    continue;
                }
                
                PaoPao* temPao2 = _paoV[temIndex];
                if (isCollisionWithOtherBall(temPao->getPosition(), CIRCLER, temPao2->getPosition(), CIRCLER))
                {
                    temPIV.push_back(temIndex);
                }
            }
            
        }
        
        for (int temSign = 0; temSign < temPIV.size(); temSign++)
        {
            temUsedIV.push_back(temPIV[temSign]);
            if (isDrop)
            {
                temClearIV.push_back(temPIV[temSign]);
            }
        }
    }
    
    return temClearIV;
}

#pragma mark -
#pragma mark 清除需要消除的球
#pragma mark -

void GamePlayScene::clearPaoPao(std::vector<int> clearPaoIndexV)
{
    if (_isFireBallDeath)
    {
        addSpecialColorIncrease(_fireBall->getPaoColor());
        _fireBall->paoDeathAnimation();
    }
    else
    {
        _paoV.push_back(_fireBall);
    }
    
    std::vector<PaoPao*> clearPaoV;
    
    for (int index = 0; index < clearPaoIndexV.size(); index++)
    {
        clearPaoV.push_back(_paoV[clearPaoIndexV[index]]);
    }
    
    for (int index = clearPaoV.size()-1; index >= 0; index--)
    {
        std::vector<PaoPao*>::iterator it = std::find(_paoV.begin(), _paoV.end(), clearPaoV[index]);
        if (it == _paoV.end())
        {
            continue;
        }
        
        PaoPao *tPaoPao = *it;
        
        PaoName tPPName = tPaoPao->getPaoName();
        PaoStatu tPPStatu = tPaoPao->getPaoStatu();
        
        if (tPPName == PaoNameBombPao)
        {
            addSpecialColorIncrease(tPaoPao->getPaoColor());
            tPaoPao->paoDeathAnimation();
            _paoV.erase(it);
        }
        else if (tPPName == PaoNameSpiderWeb)
        {
            continue;
        }
        else if (tPPStatu == PaoStatuRattanSource)
        {
            int tTag = tPaoPao->getTag();
            RowCol tRC(tTag/100,tTag%100);
            
            tPaoPao->removeStatu();
            //                tPaoPao->removeFromParent();
            addSpecialColorIncrease(tPaoPao->getPaoColor());
            tPaoPao->paoDeathAnimation();
            _paoV.erase(it);
            
            for (int colLIndex = tRC.m_nCol-1; colLIndex >= 0; colLIndex--)
            {
                int temTag = tRC.m_nRow*100+colLIndex;
                if (_qiuLayer->getChildByTag(temTag) == NULL)
                {
                    break;
                }
                PaoPao *temPao = (PaoPao*)(_qiuLayer->getChildByTag(temTag));
                if (temPao->getPaoStatu() != PaoStatuRattanCover)
                {
                    break;
                }
                temPao->removeStatu();
            }
            for (int colRIndex = tRC.m_nCol+1; colRIndex < ColCount; colRIndex++)
            {
                int temTag = tRC.m_nRow*100+colRIndex;
                if (_qiuLayer->getChildByTag(temTag) == NULL)
                {
                    break;
                }
                PaoPao *temPao = (PaoPao*)(_qiuLayer->getChildByTag(temTag));
                if (temPao->getPaoStatu() != PaoStatuRattanCover)
                {
                    break;
                }
                temPao->removeStatu();
            }
        }
        else if (tPPStatu == PaoStatuRattanCover)
        {
            continue;
        }
        else if (tPPStatu == PaoStatuMud)
        {
            tPaoPao->removeStatu();
        }
        else if (tPPStatu == PaoStatuIce)
        {
            tPaoPao->removeStatu();
        }
        else
        {
            addSpecialColorIncrease(tPaoPao->getPaoColor());
            tPaoPao->paoDeathAnimation();
            _paoV.erase(it);
        }
    }
    
    clearPaoIndexV.clear();
    clearPaoV.clear();
}

void GamePlayScene::addSpecialColorIncrease(PaoColor var)
{
    if (_specialColor != PaoColorNone)
    {
        return;
    }
    
    if (var == PaoColorNone || var == PaoColorColorful)
    {
        return;
    }
    
    _sColorM[var] = _sColorM[var]+1;
    
    if (_sColorM[var] >= 10)
    {
        _specialColor = var;
        _sColorM[PaoColorBlue] = 0;
        _sColorM[PaoColorGreen] = 0;
        _sColorM[PaoColorPink] = 0;
        _sColorM[PaoColorRed] = 0;
        _sColorM[PaoColorYellow] = 0;
        
        log("_specialColor = %d",_specialColor);
    }
}

#pragma mark -
#pragma mark 找到并清除要掉落的球，并返回是否有球掉落
#pragma mark -

bool GamePlayScene::dropDownPaoPao(std::vector<int> dropDownPaoIndexV)
{
    std::vector<int> finalDropDownIndexV = sortAndDelDuplicateVector(dropDownPaoIndexV);
    
    if (finalDropDownIndexV.size() == 0)
    {
        return false;
    }
    
    std::vector<PaoPao*> clearPaoV;
    
    for (int index = 0; index < finalDropDownIndexV.size(); index++)
    {
        clearPaoV.push_back(_paoV[finalDropDownIndexV[index]]);
    }
    
    for (int index = 0; index < clearPaoV.size(); index++)
    {
        std::vector<PaoPao*>::iterator it = std::find(_paoV.begin(), _paoV.end(), clearPaoV[index]);
        if (it == _paoV.end())
        {
            continue;
        }
        
        PaoPao *tPaoPao = *it;
        addSpecialColorIncrease(tPaoPao->getPaoColor());
        
        int col=getAbsoluteRowColByPos(tPaoPao->getPosition()).m_nCol;
        
//        tPaoPao->removeFromParent();
        _paoV.erase(it);
        tPaoPao->paoDropAnimation(col);
        
    }
    
    dropDownPaoIndexV.clear();
    finalDropDownIndexV.clear();
    clearPaoV.clear();
    return false;
}

#pragma mark -
#pragma mark 每回合的回合触发状态处理，并返回是否有球触发
#pragma mark -

bool GamePlayScene::roundDeathDeal()
{
    bool targetSwitch = false;
    std::vector<PaoPao*> mudPaoV;
    //处理眩晕球，同时把污泥球加入数组待处理
    for (int index = _paoV.size()-1; index >= 0; index--)
    {
        std::vector<PaoPao*>::iterator it = _paoV.begin()+index;
        PaoPao *temP = *it;
        
        if (temP->getPaoName() == PaoNameTimeBomb)
        {
            if (getAbsoluteRowColByPos(temP->getPosition()).m_nRow < _baseRoundStartRow)
            {
                continue;
            }
        }
        
        temP->finishedARound();
        
        if (temP->getDeathRound() == 0)
        {
            addSpecialColorIncrease(temP->getPaoColor());
            temP->paoDeathAnimation();
            _paoV.erase(it);
            _isDeath = true;
            
            return true;
        }
        if (temP->getDismissRound() == 0)
        {
            addSpecialColorIncrease(temP->getPaoColor());
            temP->paoDeathAnimation();
            _paoV.erase(it);
            targetSwitch = true;
            continue;
        }
        if (temP->getPaoStatu() == PaoStatuMud)
        {
            if (getAbsoluteRowColByPos(temP->getPosition()).m_nRow >= _baseRoundStartRow)
            {
                mudPaoV.push_back(temP);
            }
            
        }
    }
    
    if (mudPaoV.size() > 0)
    {
        std::random_shuffle(mudPaoV.begin(), mudPaoV.end());
        
        for (std::vector<PaoPao*>::iterator it = mudPaoV.begin(); it != mudPaoV.end(); it++)
        {
            PaoPao *temMudPao = *it;
            
            std::vector<int> nearIndexV = findPaoIndexNearTargetPao(PaoColorNone,temMudPao->getPosition(),false);
            
            std::random_shuffle(nearIndexV.begin(), nearIndexV.end());
            
            bool isMuded = false;
            
            for (int nearIndex = 0; nearIndex < nearIndexV.size(); nearIndex++)
            {
                PaoPao *targetNearPao = _paoV[nearIndexV[nearIndex]];
                if (targetNearPao->getPaoStatu() == PaoStatuNormal && targetNearPao->isPaoPao() && getAbsoluteRowColByPos(targetNearPao->getPosition()).m_nRow >= getAbsoluteRowColByPos(temMudPao->getPosition()).m_nRow)
                {
                    targetNearPao->setPaoStatu(PaoStatuMud);
                    targetNearPao->addStatu();
                    isMuded = true;
                    break;
                }
            }
            
            if (isMuded)
            {
                targetSwitch = true;
                break;
            }
        }
    }
    
    return targetSwitch;
}

#pragma mark -
#pragma mark 按照行列返回位置
#pragma mark -

Point GamePlayScene::getPosByRowAndCol(RowCol index)
{
    return getPosByRowAndCol(index.m_nRow, index.m_nCol);
}
Point GamePlayScene::getPosByRowAndCol(int indexX, int indexY)
{
    float x = _fixXDistance+CIRCLER*(indexX%2)+CIRCLER+indexY*CIRCLED;
    float y = VisibleSize.height - _fixYDistance - (CIRCLER+CIRCLED*sin(PI/3)*indexX);
    
    return Point(x, y);
}



#pragma mark -
#pragma mark 按照位置返回行列
#pragma mark -

RowCol GamePlayScene::getRowColByPos(Point pos)
{
    return getRowColByPos(pos.x, pos.y);
}
RowCol GamePlayScene::getRowColByPos(float posX, float posY)
{
    int nRow,nCol;
    
    nRow = (VisibleSize.height-_fixYDistance-posY-CIRCLER)/(CIRCLED*sin(PI/3))+0.5;
    
    nCol = (posX-_fixXDistance-CIRCLER*(nRow%2)-CIRCLER)/CIRCLED+0.5;
    
    if (nCol > ColCount-1)
    {
        nCol = ColCount-1;
        if (_qiuLayer->getChildByTag(nRow*100+nCol) != NULL)
        {
            nRow+=1;
        }
    }
    else if (nCol < 0)
    {
        nCol = 0;
        if (_qiuLayer->getChildByTag(nRow*100+nCol) != NULL)
        {
            nRow+=1;
        }
    }
    
    for (int index = 0; true; index++)
    {
        if (_qiuLayer->getChildByTag(nRow*100+nCol) != NULL)
        {
            nRow+=1;
        }
        else
        {
            break;
        }
    }
    
    
    if (_qiuLayer->getChildByTag(nRow*100+nCol) != NULL)
    {
        log("ERROR!!!");
    }
    
    return RowCol(nRow, nCol);
}
RowCol GamePlayScene::getAbsoluteRowColByPos(Point pos)
{
    return getAbsoluteRowColByPos(pos.x, pos.y);
}
RowCol GamePlayScene::getAbsoluteRowColByPos(float posX, float posY)
{
    int nRow,nCol;
    
    nRow = (VisibleSize.height-_fixYDistance-posY-CIRCLER)/(CIRCLED*sin(PI/3))+0.5;
    
    nCol = (posX-_fixXDistance-CIRCLER*(nRow%2)-CIRCLER)/CIRCLED+0.5;
    
    if (nCol > ColCount-1)
    {
        nCol = ColCount-1;
    }
    else if (nCol < 0)
    {
        nCol = 0;
    }
    
    return RowCol(nRow, nCol);
}


#pragma mark -
#pragma mark 本局游戏球的颜色与名字的对应数组
#pragma mark -

void GamePlayScene::randomNewPaoNameV()
{
    if (_newPaoIndexV.size()>0)
    {
        _newPaoIndexV.clear();
    }
    
    for (int index = 2; index < 7; index++)
    {
        _newPaoIndexV.push_back(index);
    }
    
    std::random_shuffle(_newPaoIndexV.begin(), _newPaoIndexV.end());
    
    
//    for (int index = 0; index < _newPaoIndexV.size(); index++)
//    {
//        log("_newPaoIndexV[index] = %d",_newPaoIndexV[index]);
//    }
}

#pragma mark -
#pragma mark 将数据转换为本局的颜色数组中对应的颜色
#pragma mark -

long GamePlayScene::transformWithLevelRandomNewPaoIndexV(long var)
{
    /*
    long firstL = (var/10000)%100;
    long secondL = (var/100)%100;
    long thirdL = var%100;
    
    if (secondL == 0)
    {
        struct timeval tpTimeA;
        gettimeofday(&tpTimeA,NULL);
        srand(tpTimeA.tv_usec);
        
        secondL = rand()%5+1;
    }
    
    log("_newPaoIndexV ------- %ld",_newPaoIndexV.size());
    
    if ((firstL == 11 || firstL == 12))
    {
        log("_newPaoIndexV ------- %ld",_newPaoIndexV.size());
        return firstL*10000+(_newPaoIndexV[secondL-1])*100+thirdL;
    }
    else
    {
        return var;
    }
     */
    
    long styleL = var/100;
    
    if (styleL != 11 && styleL != 13 && styleL != 14 && styleL != 15 && styleL != 16)
    {
        return var;
    }
    
    long firstL = var/10;
    long colorL = var%10;
    
    if (colorL == 0)
    {
        return var;
    }
    
    if (colorL == 1)
    {
        struct timeval tpTimeA;
        gettimeofday(&tpTimeA,NULL);
        srand(tpTimeA.tv_usec);
        
        colorL = rand()%5+2;
    }
    
    return firstL*10+_newPaoIndexV[colorL-2];
    
}

void GamePlayScene::prefinishLayerAppear()
{
    
}

void GamePlayScene::addPreFinishLayer()
{
    GamePreFinishLayer *gamePreFL = GamePreFinishLayer::newGamePreFinishLayer(this);
    
    addChild(gamePreFL,5);
    
    gamePreFL->startCountdown();
}

void GamePlayScene::clickTest(long clickTag)
{
    log("clickTest : %ld",clickTag);
}
void GamePlayScene::closedLayer(bool didNothing)
{
    _finishD.isWin=true;
    _finishD.LevelId=1;
    _finishD.Score=1000;
    _finishD.Star=2;
    if (_delegate != NULL)
    {
        _delegate->showFinishLayer(didNothing,_finishD);
    }
    Director::getInstance()->popScene();
    
    log("GamePlayScene::closedLayer");
}

void GamePlayScene::setDelegate(GamePlaySceneDelegate* delegate)
{
    _delegate=delegate;
}

void GamePlaySceneDelegate::showFinishLayer(bool isWin,FinishPlayData fD)
{
    
}

void GamePlayScene::testPao(std::vector<int> temV)
{
    for (int index = 0; index < temV.size(); index++)
    {
        std::vector<PaoPao*>::iterator it = _paoV.begin()+temV[index];
        PaoPao *temP = *it;
        temP->test();
    }
}

bool GamePlayScene::isWin()
{
    if (_playStatu == PlayStatuPrepare)
    {
        return false;
    }
    
    
    return false;
}

bool GamePlayScene::isFail()
{
    log("_paoV.size() = %ld, _ballDownCount = %d",_paoV.size(),_ballDownCount);
    if (_paoV.size() == 0)
    {
        return true;
    }
    if (_ballDownCount == 0)
    {
        return true;
    }
    return false;
}

void GamePlayScene::winDeal(float dt)
{
    log("winDeal");
    addPreFinishLayer();
}

void GamePlayScene::failDeal(float dt)
{
    log("failDeal");
    addPreFinishLayer();
}



