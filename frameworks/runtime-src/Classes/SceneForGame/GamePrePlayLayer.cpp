//
//  GamePrePlayLayer.cpp
//  beerpaopao
//
//  Created by Frank on 14-9-16.
//
//

#include "GamePrePlayLayer.h"


GamePrePlayLayer* GamePrePlayLayer::newGamePrePlayLayer(int levelNum, GamePrePlayLayerDelegate* delegate)
{
    GamePrePlayLayer *gamePrePlayLayer = GamePrePlayLayer::create();
    
    gamePrePlayLayer->setDelegate(delegate);
    
    gamePrePlayLayer->setLevelNum(levelNum);
    
    gamePrePlayLayer->addBaseUI();
    
//    log("newGmaePrePlayLayer-----%d",levelNum);
    
    return gamePrePlayLayer;
}



bool GamePrePlayLayer::init()
{
    if (!Layer::init())
    {
        return false;
    }
    
    return true;
}


void GamePrePlayLayer::addBaseUI()
{
    
//    _levelData=_delegate->getPerLevel();
    log("levelDta------%d",_levelNum);
//    _levelData->setLevel(_levelNum);
    _levelData=DataCenter::getInstance()->getLevelData(_levelNum);
    
    int winType=_levelData->getWinType();
    int gameSeconds=_levelData->getGameSeconds();
    int spBallCount=_levelData->getSpecialBallCount();
    std::vector<std::map<std::string,long>>specailBallV=_levelData->getSpecailBallDataV();
    
    auto backLayerColor = LayerColor::create(Color4B(0, 0, 0, 100), VisibleSize.width, VisibleSize.height);
    
    this->addChild(backLayerColor);
    
    //background
    auto backSp = Sprite::createWithSpriteFrameName(DataCenter::pngNames().PreBackGroup);
    backSp->setPosition(VisibleSize.width/2, VisibleSize.height/2);
    this->addChild(backSp);
    
    std::string levelString=DataCenter::getInstance()->fontStrings().LevelString;
    std::string winString;
    switch (winType) {
        case 1:
            winString=DataCenter::getInstance()->fontStrings().WinString1;
            break;
        case 2:
            winString=DataCenter::getInstance()->fontStrings().WinString2;
            break;
        default:
            break;
    }
    
    levelString=Replace_String(levelString, "~", turnIntToString(_levelData->getLevel()));

    auto levelLable=Label::createWithSystemFont(levelString, "Arial", 80);
    
    levelLable->enableGlow(Color4B::GREEN);//发光
    levelLable->enableShadow();//阴影
    levelLable->enableOutline(Color4B::RED,2);//轮廓
    levelLable->setPosition(VisibleSize.width/2, VisibleSize.height/2+450);
    this->addChild(levelLable);
    
    auto winLable=Label::createWithSystemFont(winString, "Arial", 50);
    
//    winLable->enableGlow(Color4B::GREEN);//发光
    winLable->enableShadow();//阴影
    winLable->enableOutline(Color4B::RED,2);//轮廓
    winLable->setPosition(VisibleSize.width/2-180, VisibleSize.height/2+280);
    this->addChild(winLable);
    
    log("spBallCount-----%d",spBallCount);
    
    if (spBallCount>0) {
        std::map<std::string,long> spbMap;
//        PaoPaoData pD;
        PaoPao *sp1;
        PaoPao *sp2;
        PaoPao *sp3;
        switch (spBallCount) {
            case 1:
                spbMap=specailBallV[0];
//                pD= DataCenter::getInstance()->getPaoPaoDataWithNum(spbMap["Data"]);
//                sp1=Sprite::createWithSpriteFrameName(pD.TargetPaoFrameName);
                sp1 = PaoPao::createWithNumber(spbMap["Data"]);
                sp1->setPosition(VisibleSize.width/2+220, VisibleSize.height/2+280);
                this->addChild(sp1);
                break;
            case 2:
                spbMap=specailBallV[0];
//                pD= DataCenter::getInstance()->getPaoPaoDataWithNum(spbMap["Data"]);
                sp1=PaoPao::createWithNumber(spbMap["Data"]);
                sp1->setPosition(VisibleSize.width/2+220, VisibleSize.height/2+280);
                sp1->setRotation(-60);
                this->addChild(sp1);
                
                spbMap=specailBallV[1];
//                pD= DataCenter::getInstance()->getPaoPaoDataWithNum(spbMap["Data"]);
                sp2=PaoPao::createWithNumber(spbMap["Data"]);
                sp2->setPosition(VisibleSize.width/2+220+50, VisibleSize.height/2+280);
                sp2->setRotation(60);
                this->addChild(sp2);
                break;
            case 3:
                spbMap=specailBallV[1];
//                pD= DataCenter::getInstance()->getPaoPaoDataWithNum(spbMap["Data"]);
                sp2=PaoPao::createWithNumber(spbMap["Data"]);
                sp2->setPosition(VisibleSize.width/2+220+50, VisibleSize.height/2+250);
                sp2->setRotation(60);
                this->addChild(sp2);
                
                spbMap=specailBallV[2];
//                pD= DataCenter::getInstance()->getPaoPaoDataWithNum(spbMap["Data"]);
                sp3=PaoPao::createWithNumber(spbMap["Data"]);
                sp3->setPosition(VisibleSize.width/2+220-50, VisibleSize.height/2+250);
                sp3->setRotation(-60);
                this->addChild(sp3);
                
                spbMap=specailBallV[0];
//                pD= DataCenter::getInstance()->getPaoPaoDataWithNum(spbMap["Data"]);
                sp1=PaoPao::createWithNumber(spbMap["Data"]);
                sp1->setPosition(VisibleSize.width/2+220, VisibleSize.height/2+280);
                this->addChild(sp1);
                
                break;
            default:
                break;
        }
    }
    
    
    
    //start button
    auto menuStartItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().StartButton), Sprite::createWithSpriteFrameName(DataCenter::pngNames().StartButtonS), CC_CALLBACK_1(GamePrePlayLayer::onStartMenuClick, this));
    menuStartItem->setPosition(VisibleSize.width/2, VisibleSize.height/2-280);
    
    //close button
    auto menuCloseItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().Close), Sprite::createWithSpriteFrameName(DataCenter::pngNames().CloseS), CC_CALLBACK_1(GamePrePlayLayer::onCloseMenuClick, this));
    menuCloseItem->setPosition(VisibleSize.width-85, VisibleSize.height-430);
    
    //first daoju button
//    auto menuDaojuItem1 = MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().Bee), Sprite::createWithSpriteFrameName(DataCenter::pngNames().Bee), CC_CALLBACK_1(GamePrePlayLayer::onDaojuMenuClick, this));
//    menuDaojuItem1->setPosition(VisibleSize.width-200, VisibleSize.height-200);
//    menuDaojuItem1->setTag(10001);
//    
//    //second daoju button
//    auto menuDaojuItem2 = MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().Bee), Sprite::createWithSpriteFrameName(DataCenter::pngNames().Bee), CC_CALLBACK_1(GamePrePlayLayer::onDaojuMenuClick, this));
//    menuDaojuItem2->setPosition(VisibleSize.width-200, VisibleSize.height-200);
//    menuDaojuItem2->setTag(10002);
//    
//    //third daoju button
//    auto menuDaojuItem3 = MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().Bee), Sprite::createWithSpriteFrameName(DataCenter::pngNames().Bee), CC_CALLBACK_1(GamePrePlayLayer::onDaojuMenuClick, this));
//    menuDaojuItem3->setPosition(VisibleSize.width-200, VisibleSize.height-200);
//    menuDaojuItem3->setTag(10003);
    
    
//    auto menu = Menu::create(menuStartItem,menuCloseItem,menuDaojuItem1,menuDaojuItem2,menuDaojuItem3, NULL);
    auto menu = Menu::create(menuStartItem,menuCloseItem, NULL);
    menu->setPosition(Point::ZERO);
    
    
    this->addChild(menu);
    
}

void GamePrePlayLayer::setDelegate(GamePrePlayLayerDelegate* delegate)
{
    _delegate = delegate;
}


void GamePrePlayLayer::onStartMenuClick(Ref* pSender)
{
    log("GamePrePlayLayer");
    if (_delegate != NULL)
    {
        _delegate->onStartGameClick();
    }
}

void GamePrePlayLayer::onCloseMenuClick(Ref* pSender)
{
    if (_delegate != NULL)
    {
        _delegate->onClosePreLayerClick();
    }
    
    this->removeFromParent();
}

void GamePrePlayLayer::onDaojuMenuClick(Ref* pSender)
{
    int temTag = ((MenuItemSprite*)pSender)->getTag();
    
    if (_delegate != NULL)
    {
        _delegate->onDaojuClick(temTag);
    }
    
}

void GamePrePlayLayer::setLevelNum(int var)
{
    _levelNum = var;
}



void GamePrePlayLayerDelegate::onStartGameClick()
{
    
}
void GamePrePlayLayerDelegate::onClosePreLayerClick()
{
    
}

void GamePrePlayLayerDelegate::onDaojuClick(long daojuNum)
{
    
}

LevelData* GamePrePlayLayerDelegate::getPerLevel(){
    return NULL;
}

UserData* GamePrePlayLayerDelegate::getPerUserData(){
    return NULL;
}
