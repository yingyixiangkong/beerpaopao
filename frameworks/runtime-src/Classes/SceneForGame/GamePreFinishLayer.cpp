//
//  GamePreFinishLayer.cpp
//  beerpaopao
//
//  Created by Frank on 14/11/10.
//
//

#include "GamePreFinishLayer.h"



GamePreFinishLayer* GamePreFinishLayer::newGamePreFinishLayer(GamePreFinishLayerDelegate *delegate)
{
    GamePreFinishLayer *newPreFinishLayer = GamePreFinishLayer::create();
    
    newPreFinishLayer->setDelegate(delegate);
    
    newPreFinishLayer->addBaseUI();
    
    
    return newPreFinishLayer;
}


bool GamePreFinishLayer::init()
{
    if (!Layer::init())
    {
        return false;
    }
    
    _didNothing = true;
    _countdown = 10;
    
    return true;
}


void GamePreFinishLayer::setDelegate(GamePreFinishLayerDelegate *delegate)
{
    _delegate = delegate;
    
}


void GamePreFinishLayer::addBaseUI()
{
    auto backLayerColor = LayerColor::create(Color4B(0, 0, 0, 100), VisibleSize.width, VisibleSize.height);
    
    this->addChild(backLayerColor);
    
    auto touchListener = EventListenerTouchOneByOne::create();
    touchListener->onTouchBegan = CC_CALLBACK_2(GamePreFinishLayer::onTouchBegan, this);
    touchListener->setSwallowTouches(true);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, backLayerColor);
    
    
    _countdownLabel = Label::createWithSystemFont(turnIntToString(_countdown), "", 50);
    
    _countdownLabel->setPosition(700, 500);
    
    backLayerColor->addChild(_countdownLabel);
    
    //background
    auto backSp = Sprite::createWithSpriteFrameName(DataCenter::pngNames().PreFinishBack);
    backSp->setPosition(VisibleSize.width/2, VisibleSize.height/2);
    this->addChild(backSp);
    
    auto testMenuItem = MenuItemSprite::create(PaoPao::createWithNumber(1104), PaoPao::createWithNumber(1102), CC_CALLBACK_1(GamePreFinishLayer::onClick, this));
    testMenuItem->setPosition(500, 500);
    
    auto testMenu = Menu::createWithItem(testMenuItem);
    testMenu->setPosition(Point::ZERO);
    
    backLayerColor->addChild(testMenu);
}


void GamePreFinishLayer::startCountdown()
{
    schedule(schedule_selector(GamePreFinishLayer::autoCloseCountdown), 1);
}

void GamePreFinishLayer::autoCloseCountdown(float dt)
{
    if (!_didNothing)
    {
        unschedule(schedule_selector(GamePreFinishLayer::autoCloseCountdown));
        
        return;
    }
    
    if (_countdown <= 0)
    {
        unschedule(schedule_selector(GamePreFinishLayer::autoCloseCountdown));
        
        _didNothing = true;
        
        close(_didNothing);
        
        return;
    }
    _countdown -= 1;
    
    _countdownLabel->setString(turnIntToString(_countdown));
}



void GamePreFinishLayer::onClick(Ref *pSender)
{
    log("onClick");
    
    if (_delegate != NULL)
    {
        _delegate->clickTest(1000);
    }
}


void GamePreFinishLayer::close(bool didNothing)
{
    if (_delegate != NULL)
    {
        _delegate->closedLayer(didNothing);
    }
    
    this->removeFromParent();
}


bool GamePreFinishLayer::onTouchBegan(Touch *touch, Event *unused_event)
{
    log("GamePreFinishLayer OnTouchBegan");
    return true;
}


void GamePreFinishLayerDelegate::clickTest(long clickTag)
{
    
}

void GamePreFinishLayerDelegate::closedLayer(bool didNothing)
{
    
}