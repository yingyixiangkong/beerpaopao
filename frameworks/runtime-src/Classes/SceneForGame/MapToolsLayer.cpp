//
//  MapToolsLayer.cpp
//  beerpaopao
//
//  Created by liupeng on 14-11-4.
//
//

#include "MapToolsLayer.h"

MapToolsLayer* MapToolsLayer::newMapToolsLayer()
{
    MapToolsLayer *aNewMapToolsLayer = MapToolsLayer::create();
    
    return aNewMapToolsLayer;
}

bool MapToolsLayer::init()
{
    if (!Layer::init())
    {
        return false;
    }
    
    addBaseUI();

    return true;
}

void MapToolsLayer::setDelegate(MapToolsLayerDelegate *delegate)
{
    _delegate = delegate;
}


void MapToolsLayer::addBaseUI()
{
    auto toolbar = Sprite::createWithSpriteFrameName("ToolCloud.png");
    toolbar->setPosition(VisibleSize.width/2, VisibleSize.height-toolbar->getContentSize().height/2);
    addChild(toolbar);
    
    auto settingMenuItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName("SettingIcon.png"), Sprite::createWithSpriteFrameName("SettingIcon.png"), CC_CALLBACK_1(MapToolsLayer::onMenuClick, this));
    auto missionMenuItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName("MissionIcon.png"), Sprite::createWithSpriteFrameName("MissionIcon.png"), CC_CALLBACK_1(MapToolsLayer::onMenuClick, this));
    auto bagMenuItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName("BagIcon.png"), Sprite::createWithSpriteFrameName("BagIcon.png"), CC_CALLBACK_1(MapToolsLayer::onMenuClick, this));
    auto signMenuItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName("SignIcon.png"), Sprite::createWithSpriteFrameName("SignIcon.png"), CC_CALLBACK_1(MapToolsLayer::onMenuClick, this));
    auto shopMenuItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName("ShopIcon.png"), Sprite::createWithSpriteFrameName("ShopIcon.png"), CC_CALLBACK_1(MapToolsLayer::onMenuClick, this));
    auto setMenuItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName("SpecialIcon.png"), Sprite::createWithSpriteFrameName("SpecialIcon.png"), CC_CALLBACK_1(MapToolsLayer::onMenuClick, this));
    
    float hW = toolbar->getContentSize().width;
    
    float cW = (toolbar->getContentSize().width - settingMenuItem->getContentSize().width - missionMenuItem->getContentSize().width - bagMenuItem->getContentSize().width - signMenuItem->getContentSize().width - shopMenuItem->getContentSize().width - setMenuItem->getContentSize().width)/7.0;
    float cH = toolbar->getContentSize().height/2;
    
    setMenuItem->setPosition(hW-setMenuItem->getContentSize().width/2-cW, cH);
    shopMenuItem->setPosition(setMenuItem->getPositionX()-setMenuItem->getContentSize().width/2-shopMenuItem->getContentSize().width/2-cW, cH);
    signMenuItem->setPosition(shopMenuItem->getPositionX()-shopMenuItem->getContentSize().width/2-signMenuItem->getContentSize().width/2-cW, cH);
    bagMenuItem->setPosition(signMenuItem->getPositionX()-signMenuItem->getContentSize().width/2-bagMenuItem->getContentSize().width/2-cW, cH);
    missionMenuItem->setPosition(bagMenuItem->getPositionX()-bagMenuItem->getContentSize().width/2-missionMenuItem->getContentSize().width/2-cW, cH);
    settingMenuItem->setPosition(cW+settingMenuItem->getContentSize().width/2, cH);
    
    settingMenuItem->setTag(1);
    missionMenuItem->setTag(2);
    bagMenuItem->setTag(3);
    signMenuItem->setTag(4);
    shopMenuItem->setTag(5);
    setMenuItem->setTag(6);
    
    auto toolbarMenu = Menu::create(settingMenuItem,missionMenuItem,bagMenuItem,signMenuItem,shopMenuItem,setMenuItem, NULL);
    toolbarMenu->setPosition(Point::ZERO);
    toolbar->addChild(toolbarMenu);
    
}



void MapToolsLayer::onMenuClick(Ref *pSender)
{
    log("Tag : %d",((MenuItemSprite*)pSender)->getTag());
    if (_delegate != NULL)
    {
        _delegate->onToolBarMenuItemClick(((MenuItemSprite*)pSender)->getTag());
    }
}


void MapToolsLayerDelegate::onToolBarMenuItemClick(int pSender)
{
    
}



