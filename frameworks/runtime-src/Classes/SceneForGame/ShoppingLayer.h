//
//  ShoppingLayer.h
//  beerpaopao
//
//  Created by liupeng on 14/11/19.
//
//

#ifndef __beerpaopao__ShoppingLayer__
#define __beerpaopao__ShoppingLayer__

#include <iostream>

#include "../DealForGame/DataCenter.h"

USING_NS_CC;
USING_NS_CC_EXT;

class UserData;
class ShoppingMall;
class ShoppingLayer;
class ShoppingLayerDelegate
{
public:
    
    virtual void onShopCloseClick();
    
};

class ShoppingLayer : public Layer,virtual public extension::ScrollViewDelegate
{
public:
    static ShoppingLayer* newShoppingLayer(ShoppingLayerDelegate *delegate);
    
    virtual bool init();
    
    void setDelegate(ShoppingLayerDelegate *delegate);
    
    CREATE_FUNC(ShoppingLayer);
    
private:
    UserData *_userData;
    ShoppingMall *_shopping;
    ShoppingLayerDelegate *_delegate;
    extension::ScrollView *shopScrollView;
    virtual void scrollViewDidScroll(extension::ScrollView* view);
    virtual void scrollViewDidZoom(extension::ScrollView* view);
    
    Layer* _packLayer;
    Layer* _propLayer;
    Layer* _coinLayer;
    
    void addBaseUI();
    void addShopLayer();
    
    void onPackClick(std::string packName,std::string packPrice);
    void onPropClick(std::string propName,std::string propPrice);
    void onCoinClick(std::string coinName,std::string coinPrice);
    
    void onPackLabelClick();
    void onPropLabelClick();
    void onCoinLabelClick();
    
    void showPackLayer();
    void showPropLayer();
    void showCoinLayer();
    
    void onCloseMenuClick();
    void onShopClick(std::string pName);
};

#endif /* defined(__beerpaopao__ShoppingLayer__) */
