//
//  MapToolsLayer.h
//  beerpaopao
//
//  Created by liupeng on 14-11-4.
//
//

#ifndef __beerpaopao__MapToolsLayer__
#define __beerpaopao__MapToolsLayer__

#include <iostream>

#include "../DealForGame/DataCenter.h"

USING_NS_CC;
USING_NS_CC_EXT;


class MapToolsLayer;
class MapToolsLayerDelegate
{
public:
    virtual void onToolBarMenuItemClick(int pSender);
};

class MapToolsLayer : public Layer
{
public:
    static MapToolsLayer* newMapToolsLayer();
    
    virtual bool init();
    
    void setDelegate(MapToolsLayerDelegate *delegate);
    
    CREATE_FUNC(MapToolsLayer);
    
private:
    
    MapToolsLayerDelegate *_delegate;
    
    void addBaseUI();
    
    void onMenuClick(Ref *pSender);
};

#endif /* defined(__beerpaopao__MapToolsLayer__) */
