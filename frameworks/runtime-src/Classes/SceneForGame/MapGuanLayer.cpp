//
//  MapGuanLayer.cpp
//  beerpaopao
//
//  Created by Frank on 14-9-12.
//
//

#include "MapGuanLayer.h"





MapGuanLayer* MapGuanLayer::newMapGuanLayer()
{
    MapGuanLayer *aNewMapGuanLayer = MapGuanLayer::create();
    
    return aNewMapGuanLayer;
}

bool MapGuanLayer::init()
{
    if (!Layer::init())
    {
        return false;
    }
    
    _mapGuanNum = 0;
    _displayerFix = 0;
    _addBase = false;
    
    return true;
}

float MapGuanLayer::getCurrentH()
{
    return _currentH-300-_displayerFix/2;
}

void MapGuanLayer::setCurrentH(float var)
{
    _currentH = var;
}

void MapGuanLayer::setDelegate(MapGuanLayerDelegate *delegate)
{
    _delegate = delegate;
}

void MapGuanLayer::addBaseMapGuan(int baseNum)
{
    log("MapGuanBottom1------%s",DataCenter::pngNames().MapGuanBottom1.c_str());
    auto thirdBottom1 = Sprite::createWithSpriteFrameName(DataCenter::pngNames().MapGuanBottom1);
    thirdBottom1->setPosition(VisibleSize.width/2, thirdBottom1->getContentSize().height/2);
    this->addChild(thirdBottom1);
    auto thirdBottom2 = Sprite::createWithSpriteFrameName(DataCenter::pngNames().MapGuanBottom2);
    thirdBottom2->setPosition(VisibleSize.width/2+55, thirdBottom2->getContentSize().height-120);
    this->addChild(thirdBottom2);
    
    auto frontGrass = Sprite::createWithSpriteFrameName(DataCenter::pngNames().MapGuanBottom3);
    frontGrass->setPosition(VisibleSize.width/2+115, thirdBottom2->getContentSize().height-250);
    this->addChild(frontGrass);
    
    auto frontLeaf1=Sprite::createWithSpriteFrameName(DataCenter::pngNames().MapBottomLeaf1);
    frontLeaf1->setPosition(VisibleSize.width/2+118-frontLeaf1->getContentSize().width/2, thirdBottom2->getContentSize().height-100+frontLeaf1->getContentSize().height/2);
    this->addChild(frontLeaf1);
    
//    DeccelAmplitude *dee=DeccelAmplitude::create(<#cocos2d::Action *action#>, <#float duration#>)
    
    ActionInterval * rotateto = CCRotateTo::create(0.1, 8);
    ActionInterval * rotateto1 = CCRotateTo::create(0.15, -8);
    Sequence* se=Sequence::create(rotateto,rotateto1,rotateto,rotateto1,DelayTime::create(5), NULL);
    
//    Repeat* ss=Repeat::create(se, 3);
    RepeatForever* rotateP=RepeatForever::create(se);
    frontLeaf1->setAnchorPoint(Vec2(0, 1));
    frontLeaf1->runAction(rotateP);
    
    auto frontLeaf3=Sprite::createWithSpriteFrameName(DataCenter::pngNames().MapBottomLeaf3);
    frontLeaf3->setPosition(VisibleSize.width/2+50, thirdBottom2->getContentSize().height-350);
    this->addChild(frontLeaf3);
    
    auto frontLeaf2=Sprite::createWithSpriteFrameName(DataCenter::pngNames().MapBottomLeaf2);
    frontLeaf2->setPosition(VisibleSize.width/2-90, thirdBottom2->getContentSize().height-300);
    this->addChild(frontLeaf2);
    
    _addBase = true;
//    _currentH = thirdBottom1->getContentSize().height+thirdBottom2->getContentSize().height;
    _currentH = 780;
    
    for (int index = 0; index < baseNum; index++)
    {
        addMapGuan();
    }
}

void MapGuanLayer::addMapGuan()
{
    log("addMapGuan");
    if (_addBase)
    {
        Sprite *thirdMid;
        Sprite *leftLeaf;
        Sprite *rightLeaf;
        
        thirdMid = Sprite::create(DataCenter::pngFilePaths().MapThirdMid);
        leftLeaf=Sprite::createWithSpriteFrameName(DataCenter::pngNames().LeafLeft);
        rightLeaf=Sprite::createWithSpriteFrameName(DataCenter::pngNames().LeafRight);
        
        if (_mapGuanNum%2==0) {
            thirdMid->setPosition(VisibleSize.width/2, _currentH+thirdMid->getContentSize().height/2);
            leftLeaf->setPosition(VisibleSize.width/2-180, _currentH+thirdMid->getContentSize().height/2-120);
            rightLeaf->setPosition(VisibleSize.width/2+210, _currentH+thirdMid->getContentSize().height-225);
        }else{
            thirdMid->setPosition(VisibleSize.width/2, _currentH+thirdMid->getContentSize().height/2);
        }
        
        this->addChild(thirdMid);
        this->addChild(leftLeaf);
        this->addChild(rightLeaf);
//        log("MapThirdMid----%s",DataCenter::pngFilePaths().MapThirdMid.c_str());
        _currentH += thirdMid->getContentSize().height;
        
        if (_displayerFix == 0)
        {
            _displayerFix = thirdMid->getContentSize().height;
        }
        
        std::vector<MapGuanUIData> mapGuanUIDataV = DataCenter::mapGuanUIDataVector();
        
        for (int index = 0; index < mapGuanUIDataV.size(); index++)
        {
            MapGuanUIData temGuanUIData = mapGuanUIDataV[index];
            
            MenuItemImage *guanMenuItem = MenuItemImage::create(temGuanUIData.NormalImage, temGuanUIData.SelectedImage, CC_CALLBACK_1(MapGuanLayer::onMenuClick, this));
            guanMenuItem->setPosition(temGuanUIData.PositionX, temGuanUIData.PositionY);
            
            guanMenuItem->setTag(_mapGuanNum*10+index+1);
            
            Menu *guanMenu = Menu::create(guanMenuItem, NULL);
            guanMenu->setPosition(Point::ZERO);
            
            thirdMid->addChild(guanMenu);
        }
        _mapGuanNum++;
    }
    else
    {
        addBaseMapGuan(2);
    }
}

void MapGuanLayer::onMenuClick(cocos2d::Ref *pSender)
{
    log("TAG : %d",((MenuItemImage*)pSender)->getTag());
    if (_delegate != NULL)
    {
        _delegate->onMenuItemClick((MenuItemImage*)pSender);
    }
}




void MapGuanLayerDelegate::onMenuItemClick(MenuItemImage *pSender)
{
    log("No!!!!!!!!!!!!!!!! Tag : %d",pSender->getTag());
}

