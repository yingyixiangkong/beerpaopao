//
//  GamePlayScene.h
//  beerpaopao
//
//  Created by Frank on 14-9-13.
//
//

#ifndef __beerpaopao__GamePlayScene__
#define __beerpaopao__GamePlayScene__

#include <iostream>

#include "../DealForGame/DataCenter.h"
#include "../SpecialSprite/PaoPao.h"
#include "GamePreFinishLayer.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocostudio;


#define PaoSpeed 30
#define RowStandard 10
#define ColCount 11
#define PassFix 10
#define StatuAnimationDeley 0.02
#define StarnderdDeley 0.001

enum PlayStatu
{
    PlayStatuPrepare = 1,
    PlayStatuPaoClean = 2,
    PlayStatuPaoDrop = 3,
    PlayStatuPaoRoundDeal = 4,
    PlayStatuPaoDropAgain = 5,
    PlayStatuDataDeal = 6,
    PlayStatuFire = 7,
    PlayStatuPaoRunning = 8,
};

class GamePlayScene;
class GamePlaySceneDelegate
{
public:
    virtual void showFinishLayer(bool isWin,FinishPlayData fD);
};

class GamePlayScene : public Layer, virtual public GamePreFinishLayerDelegate
{
public:
    static Scene* newGamePlayScene(int level);
    
    virtual bool init();
    
    void adjustQiuLayer();
    
    CREATE_FUNC(GamePlayScene);
    
private:
    
    int _level;
    
    int _row;
    int _col;
    std::vector<std::vector<long>> _ballUpVector;
    int _ballDownCount;
    std::vector<long> _ballDownVector;
    int _winType;
    int _gameSeconds;
    long _gameWinScore;
    int _specialBallCount;
    long _promptBall;
    long _oneStarScore;
    long _twoStarScore;
    long _threeStarScore;
    std::vector<std::map<std::string,long>> _specailBallDataV;
    std::vector<int> _newPaoIndexV;
    
    LevelData *_lData;
    
    bool _startFire;
    
    LayerColor *_topL;
    LayerColor *_bottomL;
    Layer *_qiuLayer;
    
    float _fixXDistance;
    
    float _fixYDistance;
    
    float _transformFixH;
    
    Point _fireBallStartPos;
    Point _spBallStartPos;
    PaoPao *_fireBall;
    PaoPao *_waitingBall;
    Point _fireVector;
    Point _lastFireVector;
    
    int _lastMaxRow;
    int _currentMaxRow;
    int _baseRoundStartRow;
    int _fanCleanColFix;
    
    PlayStatu _playStatu;
    
    std::vector<PaoPao*> _paoV;
    
    GamePlaySceneDelegate *_delegate;
    
    FinishPlayData _finishD;
    
    std::vector<int> _triggerPaoIndexV;
    
    bool _isFireBallDeath;
    bool _fanActionLock;
    bool _isDeath;
    bool _isSwpiderWeb;
    PaoColor _specialColor;
    std::map<PaoColor,int> _sColorM;
    
    void setDelegate(GamePlaySceneDelegate* delegate);
    
    void setLevel(int level);
    
    void addBaseUI();
    
    Point getPosByRowAndCol(RowCol index);
    Point getPosByRowAndCol(int indexX, int indexY);
    
    RowCol getRowColByPos(Point pos);
    RowCol getRowColByPos(float posX, float posY);
    RowCol getAbsoluteRowColByPos(Point pos);
    RowCol getAbsoluteRowColByPos(float posX, float posY);
    
    void doSomethingAfterAdjust(float dt);
    
    bool onMyTouchBegan(Touch *touch, Event *unused_event);
    void onMyTouchEnded(Touch *touch, Event *unused_event);
    
    void updateFireBall(float dt);
    
    void createFireBall();
    void createWaitingBall();
    void exchangeBall();
    
    bool isCollisionWithBorder();
    bool isCollisionWithBallOrTop();
    bool isCollisionWithOtherBall(Point pos1, float radius1, Point pos2, float radius2);
    
    void cleanPaoPao(float dt);
    void dropPaoPao(float dt);
    void paoRoundDeal(float dt);
    void adjustLocation(float dt);
    
    bool dealPaoAfterFireBallStopRunning();
    
    void fanClean(float dt);
    
    std::vector<int> findPaoIndexNearTargetPao(PaoColor temColor, Point pos, bool chooseTrigger);
    
    std::vector<int> findMeteoriteCleanPao(int circleCount);
    std::vector<int> findBombCleanPao(int circleCount);
    
    std::vector<int> findPaoSamePao(std::vector<int> temClearIndexV);
    std::vector<int> findLinkedColorPao(Point sourcePos);
    
    std::vector<int> findTriggerClearPao();
    
    std::vector<int> findTriggerClearPaoByTrigger(int triggerIndex);
    
    std::vector<int> findDropDownPaoPao();
    
    void clearPaoPao(std::vector<int> clearPaoIndexV);
    
    void addSpecialColorIncrease(PaoColor var);
    
    bool dropDownPaoPao(std::vector<int> dropDownPaoIndexV);
    
    bool roundDeathDeal();
    
    void randomPosForBee();
    void finishedRunForBee();
    
    void prefinishLayerAppear();
    
    void addPreFinishLayer();
    
    void randomNewPaoNameV();
    
    long transformWithLevelRandomNewPaoIndexV(long var);//所有泡泡都从此方法转化，得到本关统一规则转换后的泡泡
    
    
    bool isWin();
    bool isFail();
    
    void winDeal(float dt);
    void failDeal(float dt);
    
    void testPao(std::vector<int> temV);
    
    
    
    
    
    
    
    
    
    
    
    
    
    virtual void clickTest(long clickTag);
    
    virtual void closedLayer(bool didNothing);
};

#endif /* defined(__beerpaopao__GamePlayScene__) */
