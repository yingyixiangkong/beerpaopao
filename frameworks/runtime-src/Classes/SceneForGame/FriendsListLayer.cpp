//
//  FriendsListLayer.cpp
//  beerpaopao
//
//  Created by liupeng on 14/12/22.
//
//

#include "FriendsListLayer.h"

FriendsListLayer* FriendsListLayer::newFriendsListLayer()
{
    FriendsListLayer *friendsListLayer = FriendsListLayer::create();
    
//    friendsListLayer->setDelegate(delegate);
    
    friendsListLayer->addBaseUI();
    
    //    log("newGmaePrePlayLayer-----%d",levelNum);
    
    return friendsListLayer;
}

bool FriendsListLayer::init()
{
    if (!Layer::init())
    {
        return false;
    }
    
    return true;
}

void FriendsListLayer::setDelegate(FriendsListLayerDelegate *delegate)
{
    _delegate = delegate;
}


void FriendsListLayer::addBaseUI(){
    
//    _userData=DataCenter::getInstance()->getUserData();
//    NetWorkData *netData=NetWorkData::newNetWorkData();
//    netData->requestFriendsList("user11", "0", "0", 0);
    
    auto backLayerColor = LayerColor::create(Color4B(0, 0, 0, 100), VisibleSize.width, VisibleSize.height);
    
    this->addChild(backLayerColor);
    
    //background
    auto backSp = Sprite::createWithSpriteFrameName(DataCenter::pngNames().FriendBack);
    backSp->setPosition(VisibleSize.width/2, VisibleSize.height/2);
    this->addChild(backSp);
    
    //background
    auto backS = Sprite::createWithSpriteFrameName(DataCenter::pngNames().BlankBack);
    backS->setPosition(VisibleSize.width/2, VisibleSize.height/2-60);
    this->addChild(backS);
    
    //close button
    auto menuCloseItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().Close), Sprite::createWithSpriteFrameName(DataCenter::pngNames().CloseS), CC_CALLBACK_1(FriendsListLayer::onMenuClick, this));
    
    menuCloseItem->setPosition(VisibleSize.width-80, VisibleSize.height-80);
    menuCloseItem->setTag(1001);
    
    //button
    auto menuAddItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().AddFriend), Sprite::createWithSpriteFrameName(DataCenter::pngNames().AddFriendS), CC_CALLBACK_1(FriendsListLayer::onMenuClick, this));
    menuAddItem->setPosition(180, 40);
    menuAddItem->setTag(1002);
    
    auto menuDelItem = MenuItemSprite::create(Sprite::createWithSpriteFrameName(DataCenter::pngNames().DelFriend), Sprite::createWithSpriteFrameName(DataCenter::pngNames().DelFriendS), CC_CALLBACK_1(FriendsListLayer::onMenuClick, this));
    menuDelItem->setPosition(VisibleSize.width-180, 40);
    menuDelItem->setTag(1003);
    
    auto menu = Menu::create(menuCloseItem,menuAddItem,menuDelItem, NULL);
    menu->setPosition(Point::ZERO);
    this->addChild(menu);
}

void FriendsListLayer::onMenuClick(Ref *pSender){
   int tt= ((MenuItemSprite*)pSender)->getTag();
    
    switch (tt) {
        case 1001:
            if (_delegate != NULL)
            {
                _delegate->onFDListCloseClick();
            }
            this->removeFromParent();
            break;
        case 1002:
            addFriends();
            break;
        case 1003:
            delFriends();
            break;
        default:
            break;
    }
}

void FriendsListLayer::addFriends(){
    
}

void FriendsListLayer::delFriends(){
    
}

void FriendsListLayer::updataFriendsLayer(std::vector<NetBackFriendData> friendsList)
{
    log("FriendsListLayer::updataFriendsLayer");
    
    for (int i=0; i<friendsList.size(); i++) {
        NetBackFriendData ff=friendsList[i];
        
        
    }
}

void FriendsListLayerDelegate::onFDListCloseClick()
{
    
}

void FriendsListLayer::scrollViewDidScroll(extension::ScrollView* view){
}
void FriendsListLayer::scrollViewDidZoom(extension::ScrollView* view){
}