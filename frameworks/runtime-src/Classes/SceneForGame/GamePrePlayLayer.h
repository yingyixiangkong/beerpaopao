//
//  GamePrePlayLayer.h
//  beerpaopao
//
//  Created by Frank on 14-9-16.
//
//

#ifndef __beerpaopao__GamePrePlayLayer__
#define __beerpaopao__GamePrePlayLayer__

#include <iostream>
#include "../DealForGame/DataCenter.h"
#include "../SpecialSprite/PaoPao.h"


USING_NS_CC;
USING_NS_CC_EXT;

class LevelData;
class UserData;

class GamePrePlayLayer;
class GamePrePlayLayerDelegate
{
public:
    virtual void onStartGameClick();
    virtual void onClosePreLayerClick();
    virtual void onDaojuClick(long daojuNum);
    virtual LevelData* getPerLevel();
    virtual UserData* getPerUserData();
};

class GamePrePlayLayer : public Layer
{
public:
    
    static GamePrePlayLayer* newGamePrePlayLayer(int levelNum, GamePrePlayLayerDelegate* delegate);
    
    virtual bool init();
    
    
    void getLevelNum();
    
    CREATE_FUNC(GamePrePlayLayer);
    
private:
    
    GamePrePlayLayerDelegate *_delegate;
    
    int _levelNum;
    
    LevelData* _levelData;
    
    void setDelegate(GamePrePlayLayerDelegate* delegate);
    
    void setLevelNum(int var);
    
    void addBaseUI();
    
    void onStartMenuClick(Ref* pSender);
    
    void onCloseMenuClick(Ref* pSender);
    
    void onDaojuMenuClick(Ref* pSender);
    
};







#endif /* defined(__beerpaopao__GamePrePlayLayer__) */
