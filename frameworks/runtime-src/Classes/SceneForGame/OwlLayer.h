//
//  OwlLayer.h
//  beerpaopao
//
//  Created by liupeng on 14/11/24.
//
//

#ifndef __beerpaopao__OwlLayer__
#define __beerpaopao__OwlLayer__

#include <iostream>

#include "../DealForGame/DataCenter.h"
#include "../SpecialSprite/PaoPao.h"

USING_NS_CC;
USING_NS_CC_EXT;

class UserData;
class OwlData;
class OwlObjectData;

class OwlLayer;
class OwlLayerDelegate
{
public:
    
    virtual void onOwlCloseClick();
    
};

class OwlLayer : public Layer
{
public:
    static OwlLayer* newOwlLayer(OwlLayerDelegate *delegate);
    
    virtual bool init();
    
    void setDelegate(OwlLayerDelegate *delegate);
    
    CREATE_FUNC(OwlLayer);
    
private:
    UserData *_userData;
    OwlData *_owlData;
    OwlObjectData *_owlObjetData;
    
    OwlLayerDelegate *_delegate;
    EventListenerTouchOneByOne *_owlListener;
    
    bool isSleep;
    void addBaseUI();
    
    void SleepOwl();
    
    bool onOwlClick(Touch *touch, Event *unused_event);
    void onPropClick();
    void owlUpgrade();
    void makeProp(int num,std::string proString);
    void addNum();
    Sprite* owlSleep;
    Sprite* sleepPao;
    
    Menu* _rightPaoMenu;
    Menu* _makeMenu;
    Label* _numLabel;
    
    std::vector<MenuItemSprite*> _rightPaoMenuItem;
    std::vector<Label*> _rightPaoLabel;
    
    std::vector<PaoPao*> _leftPao;
    std::vector<Label*> _leftPaoLabel;

    Layer* _owlLayer;
    
    MenuItemSprite* _makeButton;
    MenuItemSprite* _addNumButton;
    int _makeNum;
    
    void addOwlDialog();
    void onMakeClick(Ref *pSender);
    void onLeftPaoClick(Ref *pSender);
    void onCloseClick();
};

#endif /* defined(__beerpaopao__OwlLayer__) */
