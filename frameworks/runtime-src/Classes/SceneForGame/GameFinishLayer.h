//
//  GameFinishLayer.h
//  beerpaopao
//
//  Created by Frank on 14/11/10.
//
//

#ifndef __beerpaopao__GameFinishLayer__
#define __beerpaopao__GameFinishLayer__

#include <stdio.h>
#include <iostream>
#include "../DealForGame/DataCenter.h"

USING_NS_CC;
USING_NS_CC_EXT;

class GameFinishLayer;
class GameFinishLayerDelegate
{
public:
    virtual FinishPlayData getFinishPlayData();
    virtual void finishLayerClose();
    virtual void finishLayerNext(int levelId);
    virtual void finishLayerShare(int levelId);
    virtual void finishLayerRetry(int levelId);
};

class GameFinishLayer : public Layer
{
public:
    
    static GameFinishLayer* newGameFinishLayer(GameFinishLayerDelegate *delegate);
    
    virtual bool init();
    
    CREATE_FUNC(GameFinishLayer);
    
private:
    
    LayerColor *_backLayerColor;
    EventListenerTouchOneByOne *_touchListener;
    
    Sprite *_backSprite;
    
    FinishPlayData _finishPD;
    
    GameFinishLayerDelegate *_delegate;
    
    void setDelegate(GameFinishLayerDelegate *delegate);
    
    void setFinishPD(FinishPlayData var);
    
    void addBaseUI();
    void addFailedUI();
    void addWinUI();
    
    bool onTouchBegan(Touch *touch, Event *unused_event);
    
    
    void closeLayer(Ref *pSender);
    void onNext(Ref *pSender);
    void onShare(Ref *pSender);
    void onRetry(Ref *pSender);
    
};


#endif /* defined(__beerpaopao__GameFinishLayer__) */
