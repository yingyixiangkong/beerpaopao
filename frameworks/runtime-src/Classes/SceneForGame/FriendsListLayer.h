//
//  FriendsListLayer.h
//  beerpaopao
//
//  Created by liupeng on 14/12/22.
//
//

#ifndef __beerpaopao__FriendsListLayer__
#define __beerpaopao__FriendsListLayer__

#include <iostream>
#include "../DealForGame/DataCenter.h"
#include "CocosGUI.h"
#include "../DealForGame/NetWorkData.h"

USING_NS_CC;
USING_NS_CC_EXT;

class FriendsListLayer;
class FriendsListLayerDelegate
{
public:
    
    virtual void onFDListCloseClick();
//    virtual void updataFriendsLayer(bool isFD);
};

class FriendsListLayer : public Layer,virtual public extension::ScrollViewDelegate
{
public:
    static FriendsListLayer* newFriendsListLayer();
    
    virtual bool init();
    
    void setDelegate(FriendsListLayerDelegate *delegate);
    
    void updataFriendsLayer(std::vector<NetBackFriendData> friendsList);
    
    CREATE_FUNC(FriendsListLayer);
    
private:
    
    FriendsListLayerDelegate *_delegate;
    
    extension::ScrollView *friendScrollView;
    virtual void scrollViewDidScroll(extension::ScrollView* view);
    virtual void scrollViewDidZoom(extension::ScrollView* view);
    
    void addBaseUI();
    
    void addFriends();
    
    void delFriends();
    
    void onMenuClick(Ref *pSender);
};

#endif /* defined(__beerpaopao__FriendsListLayer__) */
