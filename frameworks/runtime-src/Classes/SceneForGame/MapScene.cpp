//
//  MapScene.cpp
//  beerpaopao
//
//  Created by Frank on 14-9-5.
//
//

#include "MapScene.h"

//static Scene *s_mapScene = NULL;

Scene* MapScene::newMapScene()
{
    
//    if (s_mapScene == NULL)
//    {
//        s_mapScene = Scene::create();
//        MapScene *newMapL = MapScene::create();
//        
//        newMapL->addFirstLayer();
//        newMapL->addSecondLayer();
//        newMapL->addThirdLayer();
//        newMapL->addFourthLayer();
//        newMapL->addOwl();
//        s_mapScene->addChild(newMapL);
//    }
    
    
    auto newMapS = Scene::create();
    MapScene *newMapL = MapScene::create();
    newMapL->setTag(888);
    newMapL->addFirstLayer();
    newMapL->addSecondLayer();
    newMapL->addThirdLayer();
    newMapL->addFourthLayer();
    newMapL->addOwl();
//    newMapS->setName("GreatPaoPao");
    newMapS->addChild(newMapL);
    
    return newMapS;
}

bool MapScene::init()
{
    if (!Layer::init())
    {
        return false;
    }
    
    _userData=DataCenter::getInstance()->getUserData();
//    _levelData = LevelData::create();
//    _levelData->setLevel(_userData->getUserLevel());
    _levelData=DataCenter::getInstance()->getLevelData(_userData->getUserLevel());
    _currenLevel=_userData->getUserLevel();
    return true;
}


void MapScene::addFirstLayer()
{
    //最底层的不动的蓝天,层标识1
    log("mapFirstBack = %s",DataCenter::pngFilePaths().MapFirstBack.c_str());
    auto firstBack = Sprite::create(DataCenter::pngFilePaths().MapFirstBack);
    firstBack->setPosition(Origin.x+VisibleSize.width/2, Origin.y+VisibleSize.height/2);
    this->addChild(firstBack,1);
}

void MapScene::addSecondLayer()
{
    //缓慢移动的白云层,层标识2
    auto secondBack = Sprite::create("res/Map/MapSecondBack.png");
    secondBack->setPosition(50+secondBack->getContentSize().width/2, VisibleSize.height-200);
    secondBack->setTag(2000);
    this->addChild(secondBack, 2);
}

void MapScene::addThirdLayer()
{
    lastOffP = Point(-1, 0);
    scrollTopSwitch = true;
    mapScrollView = extension::ScrollView::create(VisibleSize);
    mapScrollView->setPosition(Origin.x, Origin.y);
    mapScrollView->setDirection(extension::ScrollView::Direction::VERTICAL);
    this->addChild(mapScrollView, 3);
    mapScrollView->setDelegate(this);
    
    
    mapGuanL = MapGuanLayer::create();
    mapGuanL->addBaseMapGuan(2);
    mapGuanL->setPosition(0, -100);
    mapGuanL->setDelegate(this);
    mapScrollView->addChild(mapGuanL);
    
    mapScrollView->setContentSize(Size(VisibleSize.width, mapGuanL->getCurrentH()));
    mapScrollView->setBounceable(true);
    
//    mapScrollView->setContentOffset(Size(0, -1500),true);
    
}

void MapScene::addFourthLayer()
{
    //放固定按钮的,层标识4
    
    auto toolbar = MapToolsLayer::create();
    
    toolbar->setDelegate(this);
    
    
    this->addChild(toolbar,4);
    
}

void MapScene::addOwl(){
    owlMapLayer=Layer::create();
    
    
    owlSleep=Sprite::createWithSpriteFrameName(DataCenter::pngNames().OwlSleep);
    owlCloud=Sprite::createWithSpriteFrameName(DataCenter::pngNames().OwlCloud);
    owlCloudS=Sprite::createWithSpriteFrameName(DataCenter::pngNames().OwlCloudS);
    owlCloudS1=Sprite::createWithSpriteFrameName(DataCenter::pngNames().OwlCloudS);
    sleepPao=Sprite::createWithSpriteFrameName(DataCenter::pngNames().SleepPao);
    
    owlCloud->setPosition(0, -100);
    owlCloudS->setPosition(-65, -100);
    owlCloudS1->setPosition(65, -100);
    owlSleep->setPosition(0, 0);
    sleepPao->setPosition(0, 30);
    
    owlMapLayer->addChild(owlCloud,5);
    owlMapLayer->addChild(owlCloudS,6);
    owlMapLayer->addChild(owlSleep,7);
    owlMapLayer->addChild(owlCloudS1,8);
    owlMapLayer->addChild(sleepPao,9);
    owlMapLayer->setContentSize(Size(305,223));
    owlMapLayer->setPosition(VisibleSize.width/2+400, VisibleSize.height/2-100-550);
    this->addChild(owlMapLayer,8);
    
    
    ScaleTo* scale=ScaleTo::create(0.8f,1.2f);
    ScaleTo* scale1=ScaleTo::create(0.8f,0.8f);
    Sequence* se=Sequence::create(scale,scale1, NULL);
    RepeatForever* sleepP=RepeatForever::create(se);
    sleepPao->setAnchorPoint(Vec2(1, 1));
    sleepPao->runAction(sleepP);
    isSleep=true;
    
    _owlListener = EventListenerTouchOneByOne::create();
    _owlListener->onTouchBegan = CC_CALLBACK_2(MapScene::onTouchOwl, this);
    _owlListener->setSwallowTouches(false);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_owlListener, owlMapLayer);

}


void MapScene::scrollViewDidScroll(extension::ScrollView* view)
{
    log("scrollViewDidScroll");
    
    Point offP = view->getContentOffset();
    
    log("OFFP X : %g, Y : %g, Con H : %g",offP.x,offP.y,view->getContentSize().height-VisibleSize.height);
    
//    if (offP.y == lastOffP.y && offP.y != 0)
    if ((long)(view->getContentSize().height+offP.y) <= (long)VisibleSize.height && offP.y < 0)
    {
        mapGuanL->addMapGuan();
        mapScrollView->setContentSize(Size(VisibleSize.width, mapGuanL->getCurrentH()));
    }
    else if (offP.y < -400)
    {
        auto targetCloud = this->getChildByTag(2000);
        
        targetCloud->setPosition(targetCloud->getPositionX(), targetCloud->getPositionY()+(offP.y-lastOffP.y)/10.0);
        
        if (targetCloud->getPositionY() <= targetCloud->getContentSize().height)
        {
            targetCloud->setPosition(targetCloud->getPositionX(), VisibleSize.height-200);
        }
        else if (targetCloud->getPositionY() >= VisibleSize.height-200)
        {
            targetCloud->setPosition(targetCloud->getPositionX(), targetCloud->getContentSize().height);
        }
        
    }
    lastOffP = offP;
    
    if (offP.y > 50)
    {
        view->setContentOffset(Point(offP.x, 50));
    }
    
}
void MapScene::scrollViewDidZoom(extension::ScrollView* view)
{
    
    log("scrollViewDidZoom");
}

void MapScene::WakeUpOwl(float dt){
    if (isSleep) {
        owlSleep->setSpriteFrame(DataCenter::pngNames().OwlWakeUp);
        isSleep=false;
        sleepPao->setVisible(false);
    }else{
        owlSleep->setSpriteFrame(DataCenter::pngNames().OwlSleep);
        isSleep=true;
        sleepPao->setVisible(true);
    }
    
    scheduleOnce(schedule_selector(MapScene::SleepOwl), 3.0f);
}

void MapScene::SleepOwl(float dt){
    if (isSleep) {
        owlSleep->setSpriteFrame(DataCenter::pngNames().OwlWakeUp);
        isSleep=false;
        sleepPao->setVisible(false);
    }else{
        owlSleep->setSpriteFrame(DataCenter::pngNames().OwlSleep);
        isSleep=true;
        sleepPao->setVisible(true);
    }
}

bool MapScene::onTouchOwl(Touch *touch, Event *unused_event)
{
    auto target = static_cast<Sprite*>(unused_event->getCurrentTarget());
    
    
    float tx=touch->getLocation().x;
    float ty=touch->getLocation().y;
    
    float ox=target->getPosition().x;
    float oy=target->getPosition().y;
    float ow=target->getContentSize().width;
    float oh=target->getContentSize().height;
    
    if (tx>ox-ow/2 && tx<ox+ow/2 && ty>oy-oh/2 && ty<oy+oh/2) {
        showOwlLayer();
        
        
    }else{
        log("HEHE---%f,%f",target->getPosition().x,target->getPosition().y);
        log("HAHA---%f,%f",owlMapLayer->getPosition().x,owlMapLayer->getPosition().y);
        log("HEHE---%f,%f",touch->getLocation().x,touch->getLocation().y);
        scheduleOnce(schedule_selector(MapScene::WakeUpOwl), 0.1f);
        
        
    }
    
    return true;
}

bool MapScene::onTouchBegan(Touch *touch, Event *unused_event)
{
    
    log("HEHE---");
    
//    WakeUpOwl();
    return true;
}


void MapScene::setFriendsData(std::vector<NetBackFriendData> friendsList,int FDType){
    log("setFriendsData--FDType------%d",FDType);
    log("setFriendsData--friendsList------%lu",friendsList.size());
    
    switch (FDType) {
        case 1:
            if (_frLayer) {
                _frLayer->updataFriendsLayer(friendsList);
            }
            break;
        case 2:
            break;
        default:
            break;
    }
}

void MapScene::showGamePreLayer(int level){
    _prePlayLayer = GamePrePlayLayer::newGamePrePlayLayer(level,this);
    
    this->addChild(_prePlayLayer, 15);
    
    _eventDispatcher->removeEventListener(_owlListener);
    
    _layerListener = EventListenerTouchOneByOne::create();
    _layerListener->onTouchBegan = CC_CALLBACK_2(MapScene::onTouchBegan, this);
    _layerListener->setSwallowTouches(true);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_layerListener, _prePlayLayer);
    
    mapScrollView->setTouchEnabled(false);
    
}

void MapScene::showFinishLayer(bool isWin,FinishPlayData fD)
{
    _userData=DataCenter::getInstance()->getUserData();
    
    if (fD.LevelId>_userData->getUserLevel()) {
        _userData->setUserLevel(fD.LevelId);
    }
    
    DataCenter::getInstance()->saveUserData(_userData);
    
    log("fD.LevelId-----%d",fD.LevelId);
    DataCenter::getInstance()->saveLevel(fD.LevelId, fD.Star, !fD.isWin, turnLongToString(fD.Score));
    
    _finishLayer = GameFinishLayer::newGameFinishLayer(this);
    
    this->addChild(_finishLayer, 16);
    
    _eventDispatcher->removeEventListener(_owlListener);
    
    _layerListener = EventListenerTouchOneByOne::create();
    _layerListener->onTouchBegan = CC_CALLBACK_2(MapScene::onTouchBegan, this);
    _layerListener->setSwallowTouches(true);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_layerListener, _finishLayer);
    
    mapScrollView->setTouchEnabled(false);

}

//void MapScene::gameFinish(bool s){
//    log("gameFinish-----");
//    if (s) {
//        showFinishLayer(s);
//    }else{
//    }
//}

void MapScene::showUserBagLayer(){
    auto bagLayer = UserBagLayer::newUserBagLayer(this);
    
    this->addChild(bagLayer, 16);
    
    _eventDispatcher->removeEventListener(_owlListener);
    
    _layerListener = EventListenerTouchOneByOne::create();
    _layerListener->onTouchBegan = CC_CALLBACK_2(MapScene::onTouchBegan, this);
    _layerListener->setSwallowTouches(true);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_layerListener, bagLayer);
    
    mapScrollView->setTouchEnabled(false);
}

void MapScene::showShoppingLayer(){
    auto shopLayer = ShoppingLayer::newShoppingLayer(this);
    
    this->addChild(shopLayer, 17);
    
    _eventDispatcher->removeEventListener(_owlListener);
    
    _layerListener = EventListenerTouchOneByOne::create();
    _layerListener->onTouchBegan = CC_CALLBACK_2(MapScene::onTouchBegan, this);
    _layerListener->setSwallowTouches(true);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_layerListener, shopLayer);
    
    mapScrollView->setTouchEnabled(false);
}

void MapScene::showOwlLayer(){
    auto owlLayer = OwlLayer::newOwlLayer(this);
    
    this->addChild(owlLayer, 18);
    
    this->removeChild(owlSleep);
    this->removeChild(owlCloud);
    this->removeChild(owlCloudS);
    this->removeChild(owlCloudS1);
    
    
    _eventDispatcher->removeEventListener(_owlListener);
    
//    _layerListener = EventListenerTouchOneByOne::create();
//    _layerListener->onTouchBegan = CC_CALLBACK_2(MapScene::onTouchBegan, this);
//    _layerListener->setSwallowTouches(true);
//    _eventDispatcher->addEventListenerWithSceneGraphPriority(_layerListener, owlLayer);
    
    mapScrollView->setTouchEnabled(false);
    
}

void MapScene::showFriendListLayer(){
    NetWorkData *netData=NetWorkData::newNetWorkData();
//    netData->sendNewUser(1, "abcd", "iqweuuw", "111222", "123554");
//    netData->setDelegate(this);
    
    
    _frLayer=FriendsListLayer::newFriendsListLayer();
    
//    _frLayer->setName("friendLayer");
//    _frLayer->setTag(999);
    
    this->addChild(_frLayer,19);
    
//    netData->requestFriendsList("user1000012", "0", "0", 2);
    netData->addFriend("user1000012", "user1000010", 1);
    
    _eventDispatcher->removeEventListener(_owlListener);
    
    _layerListener = EventListenerTouchOneByOne::create();
    _layerListener->onTouchBegan = CC_CALLBACK_2(MapScene::onTouchBegan, this);
    _layerListener->setSwallowTouches(true);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_layerListener, _frLayer);
    
    mapScrollView->setTouchEnabled(false);
    
}

//MapGuanLayerDelegate---------
void MapScene::onMenuItemClick(MenuItemImage *pSender)
{
    log("D Tag : %d-----_currenLevel:%d",pSender->getTag(),_currenLevel);
    _currenLevel=pSender->getTag();
    
    if (_currenLevel!=_levelData->getLevel()) {
        
        _levelData=DataCenter::getInstance()->getLevelData(_currenLevel);
    }
    
    showGamePreLayer(_currenLevel);
    
}


//MapToolsLayerDelegate----------
void MapScene::onToolBarMenuItemClick(int pSender)
{
    log("pSender = %d",pSender);
    
//    settingMenuItem->setTag(1);
//    missionMenuItem->setTag(2);
//    bagMenuItem->setTag(3);
//    signMenuItem->setTag(4);
//    shopMenuItem->setTag(5);
//    setMenuItem->setTag(6);
    
    switch (pSender) {
        case 1:
            //设置
            break;
        case 2:
            //任务(好友)
            
            showFriendListLayer();
            break;
        case 3:
            //背包
            showUserBagLayer();
            break;
        case 4:
            //签到
            break;
        case 5:
            //商店
            showShoppingLayer();
            break;
        case 6:
            //特殊关
            break;
        default:
            break;
    }
}

//GamePrePlayLayerDelegate--------
LevelData* MapScene::getPerLevel(){
    return _levelData;
}

UserData* MapScene::getPerUserData(){
    return _userData;
}

void MapScene::onStartGameClick()
{
    LevelData* levelD = DataCenter::getInstance()->getLevelData(_currenLevel);
    log("onStartGameClick-----levelD----%d",levelD->getLevel());
    auto gamePS=GamePlayScene::newGamePlayScene(_currenLevel);
    this->removeChild(_prePlayLayer);
    Director::getInstance()->pushScene(gamePS);
    
    ((GamePlayScene*)gamePS->getChildByTag(33333))->adjustQiuLayer();
}

void MapScene::onClosePreLayerClick()
{
    log("onClosePreLayerClick");
    
    _owlListener = EventListenerTouchOneByOne::create();
    _owlListener->onTouchBegan = CC_CALLBACK_2(MapScene::onTouchOwl, this);
    _owlListener->setSwallowTouches(false);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_owlListener, owlMapLayer);
    
    mapScrollView->setTouchEnabled(true);
    
}

//GameFinishLayerDelegate----------
FinishPlayData MapScene::getFinishPlayData(){
    FinishPlayData fpd;
    fpd.isWin=true;
    fpd.Score=10000;
    fpd.Star=3;
    fpd.LevelId=1;
    return fpd;
}

void MapScene::finishLayerClose(){
    _owlListener = EventListenerTouchOneByOne::create();
    _owlListener->onTouchBegan = CC_CALLBACK_2(MapScene::onTouchOwl, this);
    _owlListener->setSwallowTouches(false);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_owlListener, owlMapLayer);
    mapScrollView->setTouchEnabled(true);
}

void MapScene::finishLayerNext(int levelId){
    log("finishLayerNext");
    
    showGamePreLayer(levelId+1);
    
}

void MapScene::finishLayerShare(int levelId){
    log("finishLayerShare");
}

void MapScene::finishLayerRetry(int levelId){
    log("finishLayerRetry");
}



//UserBagLayerDelegate-------
void MapScene::onBagCloseClick()
{
    _owlListener = EventListenerTouchOneByOne::create();
    _owlListener->onTouchBegan = CC_CALLBACK_2(MapScene::onTouchOwl, this);
    _owlListener->setSwallowTouches(false);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_owlListener, owlMapLayer);
    mapScrollView->setTouchEnabled(true);
}

//ShoppingLayerDelegate-------
void MapScene::onShopCloseClick()
{
    _owlListener = EventListenerTouchOneByOne::create();
    _owlListener->onTouchBegan = CC_CALLBACK_2(MapScene::onTouchOwl, this);
    _owlListener->setSwallowTouches(false);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_owlListener, owlMapLayer);
    mapScrollView->setTouchEnabled(true);
}

//OwlLayerDelegate------
void MapScene::onOwlCloseClick()
{
    _owlListener = EventListenerTouchOneByOne::create();
    _owlListener->onTouchBegan = CC_CALLBACK_2(MapScene::onTouchOwl, this);
    _owlListener->setSwallowTouches(false);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_owlListener, owlMapLayer);
    mapScrollView->setTouchEnabled(true);
}


//FriendsListLayerDelegate------
void MapScene::onFDListCloseClick()
{
    _owlListener = EventListenerTouchOneByOne::create();
    _owlListener->onTouchBegan = CC_CALLBACK_2(MapScene::onTouchOwl, this);
    _owlListener->setSwallowTouches(false);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_owlListener, owlMapLayer);
    mapScrollView->setTouchEnabled(true);
}
