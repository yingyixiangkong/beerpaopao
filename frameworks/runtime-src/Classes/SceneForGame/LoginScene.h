//
//  LoginScene.h
//  beerpaopao
//
//  Created by Frank on 14-9-4.
//
//

#ifndef __beerpaopao__LoginScene__
#define __beerpaopao__LoginScene__

#include <iostream>

#include "../cocos/audio/include/SimpleAudioEngine.h"
#include "../DealForGame/DataCenter.h"
#include "../DealForGame/NetWorkData.h"
#include "MapScene.h"

//class UserData;
//class UserLevel;

USING_NS_CC;
USING_NS_CC_EXT;

class LoginScene : public Layer
{
public:
    static Scene* newLoginScene();
    
    virtual bool init();
    
    CREATE_FUNC(LoginScene);
    
private:
    void preLoad();
    
    void preLoadCallback(Ref *pSender);
    
    int _fileCount;
    
    void checkIfLogin();
    
    void addOtherUI(bool isLogin);
    
    void onMenuItemCallback(Ref *pSender);
};

#endif /* defined(__beerpaopao__LoginScene__) */
