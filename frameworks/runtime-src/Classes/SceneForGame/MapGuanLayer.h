//
//  MapGuanLayer.h
//  beerpaopao
//
//  Created by Frank on 14-9-12.
//
//

#ifndef __beerpaopao__MapGuanLayer__
#define __beerpaopao__MapGuanLayer__

#include <iostream>

#include "../DealForGame/DataCenter.h"

USING_NS_CC;
USING_NS_CC_EXT;


class MapGuanLayer;
class MapGuanLayerDelegate
{
public:
    virtual void onMenuItemClick(MenuItemImage *pSender);
};

class MapGuanLayer : public Layer
{
public:
    
    
    static MapGuanLayer* newMapGuanLayer();
    
    virtual bool init();
    
    void addBaseMapGuan(int baseNum);
    
    void addMapGuan();
    
    CC_PROPERTY(float, _currentH, CurrentH);
    
    void setDelegate(MapGuanLayerDelegate *delegate);
    
    CREATE_FUNC(MapGuanLayer);
    
private:
    int _mapGuanNum;
    
    bool _addBase;
    
    float _baseH;
    
    float _displayerFix;
    
    std::vector<Sprite*> _mapGuanVector;
    
    MapGuanLayerDelegate *_delegate;
    
    void onMenuClick(Ref *pSender);
    
};





#endif /* defined(__beerpaopao__MapGuanLayer__) */
