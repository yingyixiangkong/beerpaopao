//
//  PaoPao.cpp
//  beerpaopao
//
//  Created by Frank on 14-10-16.
//
//

#include "PaoPao.h"

USING_NS_CC;
USING_NS_CC_EXT;


PaoPao* PaoPao::createWithNumber(long paoNum)
{
//    PaoPaoData pD = DataCenter::getInstance()->getPaoPaoDataWithNum(paoNum);
    
    PaoPao *targetPao = new (std::nothrow) PaoPao(); //内存不足时，不会抛出异常，而是会返回空指针
    
    targetPao->setPaoNumber(paoNum);
    targetPao->setPaoSpFrameName(turnLongToString(paoNum)+".png");
    targetPao->setLifeValue(1);
    targetPao->setDeathRound(-1);
    targetPao->setDismissRound(-1);
    targetPao->setDestruction(1);
    
    long paoS = paoNum/100;
    long paoC = paoNum%10;
    long paoN = paoNum/10;
    
    if (paoS == 11 || paoS == 13 || paoS == 14 || paoS == 15 || paoS == 16)
    {
        targetPao->setIsPaoPao(true);
    }
    else
    {
        targetPao->setIsPaoPao(false);
    }
    
    if (paoN == 173 || paoN == 174 || paoN == 176 || paoN == 177 || paoN == 179)
    {
        targetPao->setIsTrigger(true);
    }
    else
    {
        targetPao->setIsTrigger(false);
    }
    
    
    if (paoC == 7)
    {
        targetPao->setPaoColor(PaoColorColorful);
    }
    else if (paoC == 6)
    {
        targetPao->setPaoColor(PaoColorYellow);
    }
    else if (paoC == 5)
    {
        targetPao->setPaoColor(PaoColorRed);
    }
    else if (paoC == 4)
    {
        targetPao->setPaoColor(PaoColorPink);
    }
    else if (paoC == 3)
    {
        targetPao->setPaoColor(PaoColorGreen);
    }
    else if (paoC == 2)
    {
        targetPao->setPaoColor(PaoColorBlue);
    }
    else
    {
        targetPao->setPaoColor(PaoColorNone);
    }
    
    
    if (paoN == 110)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNamePao);
        targetPao->setPaoStatu(PaoStatuNormal);
        targetPao->setPaoSpFrameName(turnLongToString(110)+turnLongToString(paoC)+".png");
    }
    else if (paoN == 111)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNamePao);
        targetPao->setPaoStatu(PaoStatuIce);
        targetPao->setPaoSpFrameName(turnLongToString(110)+turnLongToString(paoC)+".png");
    }
    else if (paoN == 112)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNamePao);
        targetPao->setPaoStatu(PaoStatuMud);
        targetPao->setPaoSpFrameName(turnLongToString(110)+turnLongToString(paoC)+".png");
    }
    else if (paoN == 113)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNamePao);
        targetPao->setPaoStatu(PaoStatuGiddy);
    }
    else if (paoN == 114)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNamePao);
        targetPao->setPaoStatu(PaoStatuRattanCover);
        targetPao->setPaoSpFrameName(turnLongToString(110)+turnLongToString(paoC)+".png");
    }
    else if (paoN == 115)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNamePao);
        targetPao->setPaoStatu(PaoStatuRattanSource);
        targetPao->setPaoSpFrameName(turnLongToString(110)+turnLongToString(paoC)+".png");
    }
    else if (paoN == 130)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameSpecial);
        targetPao->setPaoStatu(PaoStatuNormal);
        targetPao->setPaoSpFrameName(turnLongToString(130)+turnLongToString(paoC)+".png");
    }
    else if (paoN == 131)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameSpecial);
        targetPao->setPaoStatu(PaoStatuIce);
        targetPao->setPaoSpFrameName(turnLongToString(130)+turnLongToString(paoC)+".png");
    }
    else if (paoN == 132)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameSpecial);
        targetPao->setPaoStatu(PaoStatuMud);
        targetPao->setPaoSpFrameName(turnLongToString(130)+turnLongToString(paoC)+".png");
    }
    else if (paoN == 133)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameSpecial);
        targetPao->setPaoStatu(PaoStatuGiddy);
    }
    else if (paoN == 134)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameSpecial);
        targetPao->setPaoStatu(PaoStatuRattanCover);
        targetPao->setPaoSpFrameName(turnLongToString(130)+turnLongToString(paoC)+".png");
    }
    else if (paoN == 135)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameSpecial);
        targetPao->setPaoStatu(PaoStatuRattanSource);
        targetPao->setPaoSpFrameName(turnLongToString(130)+turnLongToString(paoC)+".png");
    }
    else if (paoN == 140)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameTimeBomb);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        targetPao->setDeathRound(5);
    }
    else if (paoN == 141)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameTimeBomb);
        targetPao->setPaoStatu(PaoStatuIce);
        //targetPao->setLifeValue(1);
        targetPao->setDeathRound(5);
    }
    else if (paoN == 142)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameTimeBomb);
        targetPao->setPaoStatu(PaoStatuMud);
        //targetPao->setLifeValue(1);
        targetPao->setDeathRound(5);
    }
    else if (paoN == 143)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameTimeBomb);
        targetPao->setPaoStatu(PaoStatuGiddy);
        //targetPao->setLifeValue(1);
        targetPao->setDeathRound(5);
    }
    else if (paoN == 144)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameTimeBomb);
        targetPao->setPaoStatu(PaoStatuRattanCover);
        //targetPao->setLifeValue(1);
        targetPao->setDeathRound(5);
    }
    else if (paoN == 145)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameTimeBomb);
        targetPao->setPaoStatu(PaoStatuRattanSource);
        //targetPao->setLifeValue(1);
        targetPao->setDeathRound(5);
    }
    else if (paoN == 150)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameSecondsIncrease);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 151)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameSecondsIncrease);
        targetPao->setPaoStatu(PaoStatuIce);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 152)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameSecondsIncrease);
        targetPao->setPaoStatu(PaoStatuMud);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 153)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameSecondsIncrease);
        targetPao->setPaoStatu(PaoStatuGiddy);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 154)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameSecondsIncrease);
        targetPao->setPaoStatu(PaoStatuRattanCover);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 155)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameSecondsIncrease);
        targetPao->setPaoStatu(PaoStatuRattanSource);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 160)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNamePaoIncrease);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 161)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNamePaoIncrease);
        targetPao->setPaoStatu(PaoStatuIce);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 162)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNamePaoIncrease);
        targetPao->setPaoStatu(PaoStatuMud);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 163)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNamePaoIncrease);
        targetPao->setPaoStatu(PaoStatuGiddy);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 164)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNamePaoIncrease);
        targetPao->setPaoStatu(PaoStatuRattanCover);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 165)
    {
        //targetPao->setIsPaoPao(true);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNamePaoIncrease);
        targetPao->setPaoStatu(PaoStatuRattanSource);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 173)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(true);
        targetPao->setPaoName(PaoNameSpiderWeb);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 174)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(true);
        targetPao->setPaoName(PaoNameDemonPao);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 175)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameStone);
        targetPao->setPaoStatu(PaoStatuNormal);
        targetPao->setLifeValue(-1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 176)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(true);
        targetPao->setPaoName(PaoNameFlashing);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 177)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(true);
        targetPao->setPaoName(PaoNameClarityPao);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        targetPao->setDismissRound(1);
    }
    else if (paoN == 178)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameBee);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 179)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(true);
        targetPao->setPaoName(PaoNameBombPao);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 200)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameColor);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 201)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameBomb);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 202)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameMeteorite);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 203)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameFan);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 211)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameCustomBomb);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 212)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameCustomBomb);
        targetPao->setPaoStatu(PaoStatuNormal);
        targetPao->setPaoSpFrameName("2110.png");
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 213)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameCustomBomb);
        targetPao->setPaoStatu(PaoStatuNormal);
        targetPao->setPaoSpFrameName("2110.png");
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 221)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameCustomMeteorite);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 222)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameCustomMeteorite);
        targetPao->setPaoStatu(PaoStatuNormal);
        targetPao->setPaoSpFrameName("2210.png");
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 223)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameCustomMeteorite);
        targetPao->setPaoStatu(PaoStatuNormal);
        targetPao->setPaoSpFrameName("2210.png");
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 231)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameCustomFan);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 232)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameCustomFan);
        targetPao->setPaoStatu(PaoStatuNormal);
        targetPao->setPaoSpFrameName("2310.png");
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 233)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameCustomFan);
        targetPao->setPaoStatu(PaoStatuNormal);
        targetPao->setPaoSpFrameName("2310.png");
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 901)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameWaterGun);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 902)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameHammer);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 903)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameMagicWand);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 904)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNamePaoExtra);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else if (paoN == 905)
    {
        //targetPao->setIsPaoPao(false);
        //targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNameSecondsExtra);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    else
    {
        targetPao->setIsPaoPao(true);
        targetPao->setIsTrigger(false);
        targetPao->setPaoName(PaoNamePao);
        targetPao->setPaoStatu(PaoStatuNormal);
        //targetPao->setLifeValue(1);
        //targetPao->setDeathRound(-1);
    }
    
//    log("targetPao->getPaoSpFrameName() : %s",targetPao->getPaoSpFrameName().c_str());
    
    if (targetPao && targetPao->initWithSpriteFrameName(targetPao->getPaoSpFrameName()))
    {
        targetPao->addStatu();
        
        return targetPao;
    }
    
    CC_SAFE_DELETE(targetPao);
    
    return nullptr;
}

/*
PaoPao* PaoPao::createCustomWithPaoName(PaoName paoName)
{
    PaoPaoData pD;
    pD.TargetPaoName = paoName;
    pD.TargetPaoStatu = PaoStatuNormal;
    pD.TargetPaoStyle = PaoStyleDaoJuDown;
    
    if (paoName == PaoNameFan)
    {
        pD.TargetPaoFrameName = DataCenter::pngNames().FanCustom;
    }
    else if (paoName == PaoNameMeteorite)
    {
        pD.TargetPaoFrameName = DataCenter::pngNames().MeteoriteCustom;
    }
    else
    {
        pD.TargetPaoFrameName = DataCenter::pngNames().BombCustom;
    }
    
    
    
    
    
    
    if (targetPao && targetPao->initWithSpriteFrameName(pD.TargetPaoFrameName))
    {
        return targetPao;
    }
    
    CC_SAFE_DELETE(targetPao);
    
    return nullptr;
}
 */

bool PaoPao::initWithSpriteFrameName(const std::string& spriteFrameName)
{
//    log("spriteFrameName = %s",spriteFrameName.c_str());
    if (spriteFrameName.length() <=0 || !SpriteFrameCache::getInstance()->getSpriteFrameByName(spriteFrameName) || !Sprite::initWithSpriteFrameName(spriteFrameName))
    {
        return false;
    }
    return true;
}

void PaoPao::removeStatu()
{
    if (_paoStatu == PaoStatuGiddy)
    {
        setSpriteFrame(turnLongToString(_paoName)+turnIntToString(_m_Color)+".png");
        setPaoStatu(PaoStatuNormal);
            
    }
    else if (_paoStatu == PaoStatuIce)
    {
        if (getChildByTag(100) != NULL)
        {
            removeChildByTag(100);
            setPaoStatu(PaoStatuNormal);
        }
    }
    else if (_paoStatu == PaoStatuMud)
    {
        if (getChildByTag(100) != NULL)
        {
            removeChildByTag(100);
            setPaoStatu(PaoStatuNormal);
        }
    }
    else if (_paoStatu == PaoStatuRattanCover)
    {
        if (getChildByTag(100) != NULL)
        {
            removeChildByTag(100);
            setPaoStatu(PaoStatuNormal);
        }
    }
    else if (_paoStatu == PaoStatuRattanSource)
    {
        if (getChildByTag(100) != NULL)
        {
            removeChildByTag(100);
            setPaoStatu(PaoStatuNormal);
        }
    }
    else
    {
        return;
    }
}

void PaoPao::test()
{
    Sprite *testSp = Sprite::create("res/GamePlay/bei.png");
    testSp->setPosition(getContentSize().width/2, getContentSize().height/2);
    testSp->setOpacity(100);
    testSp->setScale(0.6);
    addChild(testSp,3);
}

void PaoPao::addStatu()
{
    std::string statuSpfStr;
    if (_paoStatu == PaoStatuGiddy)
    {
        setSpriteFrame(turnIntToString(_paoName+_paoStatu)+turnIntToString(_m_Color)+".png");
        setDismissRound(2);
        return;
    }
    else if (_paoStatu == PaoStatuIce)
    {
        statuSpfStr = "Ice.png";
    }
    else if (_paoStatu == PaoStatuMud)
    {
        statuSpfStr = "Mud.png";
    }
    else if (_paoStatu == PaoStatuRattanCover)
    {
        statuSpfStr = "RattanCover.png";
    }
    else if (_paoStatu == PaoStatuRattanSource)
    {
        statuSpfStr = "RattanSource.png";
    }
    else
    {
        return;
    }
    
    auto statuSp = Sprite::createWithSpriteFrameName(statuSpfStr);
    statuSp->setPosition(getContentSize().width/2,getContentSize().height/2);
    statuSp->setTag(100);
    addChild(statuSp);
}

void PaoPao::finishedARound()
{
    if (_deathRound > 0)
    {
        if (_deathRound > TIMEBOMBROUND)
        {
            _deathRound = 5;
        }
        else
        {
            _deathRound -= 1;
        }
    }
    if (_dismissRound > 0)
    {
        _dismissRound -= 1;
    }
}


void PaoPao::paoDeathAnimation()
{
    if (_paoName == PaoNameBombPao)
    {
        removeFromParent();
        return;
    }
    
    float animationD = 0.8f;
    float dt = 4.0f;
    log("testDeathAnimation-----------------");
//    AnimationCache::getInstance()->addAnimationsWithFile("res/GamePlay/destroy_effect.plist");
    
//    auto deathSp = Sprite::createWithSpriteFrameName("destroy_effect_0.png");
    auto deathSp = Sprite::createWithSpriteFrameName("poblue0001.png");
    deathSp->setPosition(getPosition());
    deathSp->setTag(200);
    this->getParent()->addChild(deathSp,3);
//    addChild(deathSp,3);
    auto deathA = Animation::create();
//    for (int index = 1; index <= 25; index++)
//    {
//        deathA->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("drop"+turnIntToString(index)+".png"));
//    }
    for (int index = 1; index <= 9; index++)
    {
        deathA->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("poblue000"+turnIntToString(index)+".png"));
    }
    for (int index = 10; index <= 30; index++)
    {
        deathA->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("poblue00"+turnIntToString(index)+".png"));
    }
    deathA->setDelayPerUnit(0.02f);
    deathA->setRestoreOriginalFrame(false);
    auto action = Animate::create(deathA);
    
    auto tAct1 = TargetedAction::create(deathSp,action);
    
    Point mp=Point(VisibleSize.width * 0.5, 200);
    
    auto actMove=MoveTo::create(0.5, mp);
    auto tAct2=TargetedAction::create(deathSp, actMove);
    
    runAction(Spawn::create(ScaleTo::create(0.2f, 0.1f),tAct1,DelayTime::create(0.5f), tAct2, NULL));
    
//    runAction(action);
//    runAction(Sequence::create(DelayTime(<#const cocos2d::DelayTime &#>)))
    
//    runAction(Sequence::create(ScaleTo::create(0.2f, 0.1f),targetAct, NULL));
    
    
//    deathSp->runAction(ScaleTo::create(0.5f, 0.1f));
    
//    ParticleSystem *ps=ParticleMeteor::create();
//    ps->setTexture();
//    ps->setPosition(Point());
//    this->getParent()->addChild(ps);
    
    scheduleOnce(schedule_selector(PaoPao::paoDeath), animationD);
}


void PaoPao::paoDropAnimation(int col)
{
    if (_paoName == PaoNameBombPao)
    {
        removeFromParent();
        return;
    }
    
    float animationD = 0.8f;
    float dt = 4.0f;
    log("testDropAnimation-----------------");
    //    AnimationCache::getInstance()->addAnimationsWithFile("res/GamePlay/destroy_effect.plist");
    
    //    auto deathSp = Sprite::createWithSpriteFrameName("destroy_effect_0.png");
    auto deathSp = Sprite::createWithSpriteFrameName("drop0001.png");
    deathSp->setPosition(getContentSize().width/2, getContentSize().height/2);
    deathSp->setTag(200);
    addChild(deathSp,3);
    auto deathA = Animation::create();
    //    for (int index = 1; index <= 25; index++)
    //    {
    //        deathA->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("drop"+turnIntToString(index)+".png"));
    //    }
    for (int index = 1; index <= 9; index++)
    {
        deathA->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("drop000"+turnIntToString(index)+".png"));
    }
    for (int index = 10; index <= 25; index++)
    {
        deathA->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("drop00"+turnIntToString(index)+".png"));
    }
    deathA->setDelayPerUnit(0.02f);
    deathA->setRestoreOriginalFrame(false);
    //    auto action = Animate::create(deathA);
    //    runAction(action);
    
//    deathSp->runAction(Animate::create(deathA));
    ccBezierConfig bezi;
    float px=getPosition().x;
    float py=getPosition().y;
    
    float dA;
    float dB;
    float dx;
    
    struct timeval tpTime;
    gettimeofday(&tpTime,NULL);
    srand(tpTime.tv_usec);
    
    int ds;
    
    if (col>0 && col<10) {
        ds=rand()%4;
        dA=px-CIRCLED;
        dB=px+CIRCLED;
        dx=dA+ds*(dB-dA)/4;
    }else{
        ds=rand()%2;
        if (col==0) {
            dA=px;
            dB=px+CIRCLED;
            dx=dA+ds*(dB-dA)/2;
        }else if (col==10){
            dA=px-CIRCLED;
            dB=px;
            dx=dA-ds*(dB-dA)/2;
        }
    }
    
    if (px>=VisibleSize.width/2) {
        bezi.controlPoint_1=Point(dx,py-CIRCLED);
        bezi.controlPoint_2=Point(px+CIRCLED,py-CIRCLER*3);
        bezi.endPosition=Point(dx+CIRCLED,py-CIRCLED*3);
    }else{
        bezi.controlPoint_1=Point(dx,py-CIRCLED);
        bezi.controlPoint_2=Point(px-CIRCLED,py-CIRCLER*3);
        bezi.endPosition=Point(dx-CIRCLED,py-CIRCLED*3);
    }
    
//    auto jump = JumpBy::create(1.0f,Point(30,0),100,3);//先写一个跳跃的动作
//    auto targetAct = TargetedAction::create(girl,jump);//写一个目标动作，将动作执行人girl和要执行的动作jump放入其中
    
//    auto action=BezierTo::create(0.5f, <#const ccBezierConfig &c#>)
//    runAction(Sequence::create(CCBezierTo::create(0.3f, bezi),targetAct,Animate::create(deathA), NULL));
//    runAction(Sequence::create(Spawn::create(BezierTo::create(0.5, bezi),ScaleTo::create(0.5f, 0.1), NULL),Animate::create(deathA), NULL));
//    runAction(Sequence::create(MoveTo::create(0.2f, Point(px,py-100)),Animate::create(deathA), NULL));
    
    
    runAction(Sequence::create(CCBezierTo::create(0.3f, bezi),Animate::create(deathA), NULL));
    scheduleOnce(schedule_selector(PaoPao::paoDeath), animationD);
}


void PaoPao::paoDeath(float dt)
{
    getParent()->removeChildByTag(200);
    removeFromParent();
}

//void PaoPao::setPaoStyle(PaoStyle var)
//{
//    _paoStyle = var;
//}
//
//PaoStyle PaoPao::getPaoStyle()
//{
//    return _paoStyle;
//}

void PaoPao::setPaoStatu(PaoStatu var)
{
    _paoStatu = var;
}

PaoStatu PaoPao::getPaoStatu()
{
    return _paoStatu;
}

void PaoPao::setPaoName(PaoName var)
{
    _paoName = var;
}

PaoName PaoPao::getPaoName()
{
    return _paoName;
}

void PaoPao::setLifeValue(int var)
{
    _lifeValue = var;
}

int PaoPao::getLifeValue()
{
    return _lifeValue;
}

void PaoPao::setDeathRound(int var)
{
    _deathRound = var;
}

int PaoPao::getDeathRound()
{
    return _deathRound;
}

void PaoPao::setDismissRound(int var)
{
    _dismissRound = var;
}

int PaoPao::getDismissRound()
{
    return _dismissRound;
}

void PaoPao::setMoreSeconds(int var)
{
    _moreSeconds = var;
}
void PaoPao::setMorePao(int var)
{
    _morePao = var;
}


CustomPaoDestructive PaoPao::getDestructive()
{
    return _rDestructive;
}

void PaoPao::setDestructive(int rMax, int rMin)
{
    _rDestructive.m_maxDestructive = rMax;
    _rDestructive.m_minDestructive = rMin;
}


bool PaoPao::isPaoPao()
{
    return _isPaoPao;
}
void PaoPao::setIsPaoPao(bool var)
{
    _isPaoPao = var;
}
bool PaoPao::isTrigger()
{
    return _isTrigger;
}
void PaoPao::setIsTrigger(bool var)
{
    _isTrigger = var;
}
void PaoPao::setPaoColor(PaoColor var)
{
    _m_Color = var;
}

PaoColor PaoPao::getPaoColor()
{
    return _m_Color;
}

void PaoPao::setPaoSpFrameName(std::string var)
{
    _paoSpFrameName = var;
}
std::string PaoPao::getPaoSpFrameName()
{
    return _paoSpFrameName;
}







