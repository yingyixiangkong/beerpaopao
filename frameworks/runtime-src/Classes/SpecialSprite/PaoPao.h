//
//  PaoPao.h
//  beerpaopao
//
//  Created by Frank on 14-10-16.
//
//

#ifndef __beerpaopao__PaoPao__
#define __beerpaopao__PaoPao__

#include <iostream>
#include "DataCenter.h"

USING_NS_CC;
USING_NS_CC_EXT;




class PaoPao : public Sprite
{
public:
    static PaoPao* createWithNumber(long paoNum);
    
    virtual bool initWithSpriteFrameName(const std::string& spriteFrameName);
    
    CC_SYNTHESIZE(long, _paoNumber, PaoNumber);
    CC_PROPERTY(PaoStatu, _paoStatu, PaoStatu);
    CC_PROPERTY(PaoName, _paoName, PaoName);
    CC_PROPERTY(int, _lifeValue, LifeValue);
    CC_PROPERTY(int, _deathRound, DeathRound);
    CC_PROPERTY(int, _dismissRound, DismissRound);
    CC_SYNTHESIZE(int, _destruction, Destruction);
    
    CustomPaoDestructive getDestructive();
    bool isPaoPao();
    bool isTrigger();
    PaoColor getPaoColor();
    void addStatu();
    void removeStatu();
    
    void finishedARound();
    
    void paoDeathAnimation();
    void paoDropAnimation(int col);
    
    void test();
    
private:
    
    bool _isPaoPao;
    bool _isTrigger;
    PaoColor _m_Color;
    
    std::string _paoSpFrameName;
    
    
    int _moreSeconds;
    int _morePao;
    CustomPaoDestructive _rDestructive;
    
    void setMoreSeconds(int var);
    void setMorePao(int var);
    
    void setDestructive(int rMax, int rMin);
    
    void setIsPaoPao(bool var);
    void setIsTrigger(bool var);
    void setPaoColor(PaoColor var);
    void setPaoSpFrameName(std::string var);
    std::string getPaoSpFrameName();
    
    void paoDeath(float dt);
};



#endif /* defined(__beerpaopao__PaoPao__) */
