

local strModuleName = "mainLua";
--CCLuaLog("Module ".. strModuleName.. " loaded.");
strModuleName = nil;

------------------------------------------------------------------
-- ↓↓ 初始化环境变量 开始 ↓↓
------------------------------------------------------------------
ScutDataLogic.CNetWriter:setUrl("http://192.168.10.19:9002/service.aspx")

local strRootDir = ScutDataLogic.CFileHelper:getPath("src");
local strTmpPkgPath = package.path;

local strSubDirs =
{

	"lib",

	
	-- 在此添加新的目录
};

--package.path = string.format("%s/?.lua;%s/lib/?.lua;%s/action/?.lua;%s/common/?.lua;%s/datapool/?.lua;%s/Global/?.lua;%s/layers/?.lua;%s/LuaClass/?.lua;%s/scenes/?.lua;%s/titleMap/?.lua;%s",strRootDir,strRootDir,strRootDir,strRootDir,strRootDir,strRootDir,strRootDir,strRootDir,strRootDir,strRootDir, strTmpPkgPath);

-- 逐个添加子文件夹
for key, value in ipairs(strSubDirs) do
	local strOld = strTmpPkgPath;
	if(1 == key) then
		strTmpPkgPath = string.format("%s/%s/?.lua%s", strRootDir, value, strOld);
	else
		strTmpPkgPath = string.format("%s/%s/?.lua;%s", strRootDir, value, strOld);
	end
--	CCLuaLog(value.. " added.");
	strOld = nil;
end

package.path = string.format("%s/?.lua;%s", strRootDir, strTmpPkgPath);
strTmpPkgPath = nil;

------------------------------------------------------------------
-- ↑↑ 初始化环境变量 结束 ↑↑
------------------------------------------------------------------

-- require必须在环境变量初始化之后，避免文件找不到的情况发生
require("lib.lib")

require("GameNetWork")



 g_frame_mgr = FrameManager:new()
    g_frame_mgr:init()

    function OnHandleData(pScene, nTag, nNetRet, pData)
        local reader = ScutDataLogic.CDataRequest:Instance()
        local bValue = reader:LuaHandlePushDataWithInt(pData)

    end

math.randomseed(os.time());
__NETWORK__=true
------------------------------------------------------------------
-- ↓↓ 协议解析函数注册 开始 ↓↓
------------------------------------------------------------------

function processCommonData(lpScene)
   return true;
end

function netDecodeEnd(pScutScene, nTag)

end




------------------------------------------------------------------
-- ↑↑ 协议解析函数注册 结束 ↑↑
------------------------------------------------------------------


GameNetWork.init()
